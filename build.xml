<!-- 
# TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
# Youngjoon Jo (yjo@purdue.edu)
# https://sites.google.com/site/treesplicer/
#
# Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
-->

<project name="treesplicer" basedir="." default="help">

  <!-- Source directory -->
  <property name="srcdir" value="./src"/>

  <!-- Class directory -->
  <property name="classdir" value="./class"/>

  <!-- Jar directory -->
  <property name="jardir" value="./lib"/>

  <!-- apps directory -->
  <property name="appsdir" value="./apps"/>
  <property name="appsbasedir" value="./apps/base"/>
  <property name="appsblockdir" value="./apps/block"/>
  <property name="appssplicedir" value="./apps/splice"/>
  <property name="appsblocksplicedir" value="./apps/blocksplice"/>
  
  <property name="confdir" value="./conf"/>
      
  <property name="launcher" value="util.Launcher"/>
  
  <!-- default JVM heap size -->
  <property name="mem" value="1G"/>
  
  <target name="help">
    <loadfile property="message" srcFile="help.txt"/>
    <echo message="${message}"/>
  </target>
  
  
  <!-- Compiles TreeSplicer framework -->
  <target name="compile">
      <mkdir dir="${classdir}"/>
      <javac
      includeantruntime="false"
      srcdir    = "${srcdir}"
      destdir   = "${classdir}"
      />
  </target>
  
  <target name="help-transform" depends="compile">
    <java classname="transform.Launcher" fork="true" classpath="${classdir}">
	  <arg line="--help"/>
	  <jvmarg line="-ea"/>
	</java>
  </target>
    
  <!-- Archives the compiled class files -->
  <target name="jar" depends="compile">
    <mkdir dir = "${jardir}"/>
    <jar
      destfile = "${jardir}/treesplicer.jar"
      basedir  = "${classdir}"
      includes = "**/*.class"
    />
  </target>

  <!-- Make apps dir -->
  <condition property="appsdir.exists">
    <and>
      <available file="${appsbasedir}/classes"/>
      <available file="${appsblockdir}/classes"/>
      <available file="${appssplicedir}/classes"/>
    </and>
  </condition>
  
  <target name="mkappsdir" unless="appsdir.exists">
    <mkdir dir = "${appsbasedir}/classes"/>
    <mkdir dir = "${appsblockdir}"/>
    <mkdir dir = "${appssplicedir}"/>
    <mkdir dir = "${appsblocksplicedir}"/>
    <mkdir dir = "${appsblockdir}/classes"/>
    <mkdir dir = "${appssplicedir}/classes"/>
    <mkdir dir = "${appsblocksplicedir}/classes"/>
  </target>
  
  <!-- First transform the baseline with/without splicing -->
  <condition property="app.exists">
    <and>
      <available file="${appsbasedir}/${app}"/>
      <available file="${appsblockdir}/${app}"/>
      <available file="${appssplicedir}/${app}"/>
      <available file="${appsblocksplicedir}/${app}"/>
    </and>
  </condition>
  
  <target name="check-app">
    <fail unless="app" message="Please specify an app (-Dapp=pointcorrelation)"/>
  </target>  

  <target name="check-sort">
    <fail unless="sort" message="Please specify whether to use point sorting (-Dapp=true or -Dapp=false)"/>
  </target>  
    
  <target name="analyze-app" depends="compile,mkappsdir,check-app">
    <java classname="transform.Launcher" fork="true" classpath="${classdir}" dir="${appsdir}">
	  <arg line="--indir base --outdir block --nosplice --utilstats ${app} util"/>
	  <jvmarg line="-ea"/>
	</java>
  </target>
  
  <target name="clean-app" depends="check-app">
    <delete dir="${appsblockdir}/${app}"/>
    <delete dir="${appssplicedir}/${app}"/>
  </target>
  
  <target name="clean-app-custom" depends="check-app,check-base">
    <delete dir="${appsdir}/${base]/${app}"/>
  </target>
    
  <target name="clean">
    <delete dir="${appsbasedir}/classes"/>
    <delete dir="${appsblockdir}"/>
    <delete dir="${appssplicedir}"/>
    <delete dir="${appsblocksplicedir}"/>
    <delete dir="${classdir}"/>
    <delete dir="${jardir}"/>
    <delete file="stats.csv"/>
  </target>
  
  <condition property="appsdir-custom.exists">
    <and>
      <available file="${appsdir}/${base}/classes"/>
    </and>
  </condition>
  
  <target name="mkappsdir-custom" unless="appsdir-custom.exists" depends="check-base">
    <mkdir dir = "${appsdir}/${base}"/>
    <mkdir dir = "${appsdir}/${base}/classes"/>
  </target>
  
  <target name="check-base">
    <fail unless="base" message="Please specify base (-Dbase=base)"/>
  </target>   

  <target name="compile-app-custom" depends="check-app,check-base">
    <mkdir dir = "${appsdir}/${base}/classes"/>
    <javac
      includeantruntime="false"
      srcdir    = "${appsdir}/${base}/${app}:${appsdir}/${base}/util"
      destdir   = "${appsdir}/${base}/classes"
      debug="true"
    />
  </target>
  
</project>
