package skewheap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;

import util.Launcher;

public final class Main {
    private static void callInsert(Node root, int[] body, int totalbodies) {
        for (int i = 1; i < totalbodies; i++)
            traversal_transform(root, body[i]);
    }
    
	private static void traversal_transform(Node root, int b) {
	    if (b < root.data) {
            //System.out.printf("Val: " + b + " Node Val: " + root.data + " swapping\n");
            
            int temp = root.data;
            root.data = b;
            b = temp;
        }
        root.swapped = !root.swapped;
        if (root.swapped == false) {
            //System.out.printf("Inserting " + b + " left\n");
            if (root.left == null) {
                Node newnode = new Node();
                newnode.data = b;
                newnode.swapped = true;
                root.left = newnode;
            }
            else
                traversal_transform(root.left, b);
        }
        else {
            //System.out.printf("Inserting " + b + " right\n");
            if (root.right == null) {
                Node newnode = new Node();
                newnode.data = b;
                newnode.swapped = true;
                root.right = newnode;
            }
            else
                traversal_transform(root.right, b);
        }
    }
    
	
	public static void main(String[] args) throws ExecutionException {
        if(args.length != 1) {
            System.err.println("arguments: numBodies");
            System.err.println("ex) 100000");
            System.exit(-1);
        }
        
        int listsize = Integer.parseInt(args[0]);
        Random rand = new Random(0);
        int[] bodylist = new int[listsize];
        for (int i = 0; i < listsize; i++) {
            bodylist[i] = rand.nextInt(Integer.MAX_VALUE);
        }
        
	    final Node root = new Node(bodylist[0]);
        
        Launcher.startTiming();
        callInsert(root, bodylist, listsize);
        Launcher.stopTiming();
	}
}