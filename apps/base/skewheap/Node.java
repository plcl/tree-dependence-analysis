package skewheap;

public class Node {
    public int data;
    public Node left;
    public Node right;
    public boolean swapped;
    
    public Node(int x) {
        this.data = x;
        this.swapped = true;
	}
}