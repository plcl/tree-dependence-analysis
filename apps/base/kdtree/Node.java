package kdtree;

public class Node {
    public float xmin;
    public float xmax;
    public float ymin;
    public float ymax;
    public float zmin;
    public float zmax;
    public float x;
    public float y;
    public float z;
    public boolean isleaf;
    public Node left;
    public Node right;
    
    public Node() {
        this.xmin = this.ymin = this.zmin = 0.0f;
        this.xmax = this.ymax = this.zmax = 1.0f;
        this.isleaf = true;
	}
}