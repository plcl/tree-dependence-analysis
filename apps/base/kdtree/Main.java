package kdtree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;

import util.Launcher;

public final class Main {
    private static int nbodies; // number of bodies in system
    private static int max_depth; // depth of tree
    private static Node bodylist[];
    
    private static void ReadInput(String filename) {
        Scanner in = null;
        try {
            if (filename.endsWith(".gz")) {
                in = new Scanner(new GZIPInputStream(new FileInputStream(filename)));
            } else {
                in = new Scanner(new BufferedReader(new FileReader(filename)));
            }
            in.useLocale(Locale.US);
        } catch (FileNotFoundException e) {
            System.err.println(e);
            System.exit(-1);
        } catch (IOException e) {
            System.err.println(e);
            System.exit(-1);
        }
        
        bodylist = new Node[nbodies];
        for (int i = 0; i < nbodies; i++) {
            bodylist[i] = new Node();
            
            int temp = in.nextInt();
            bodylist[i].x = (float) temp / Integer.MAX_VALUE;
            bodylist[i].y = (float) temp / Integer.MAX_VALUE;
            bodylist[i].z = (float) temp / Integer.MAX_VALUE;
        }
    }

    
    private static void callInsert(Node root, Node[] body, int totalbodies) {
        root.x = body[0].x;
        root.y = body[0].y;
        root.z = body[0].z;
        root.isleaf = true;
        
        for(int i = 1; i < totalbodies; i++)
        {
            Insert(root, body[i].x, body[i].y, body[i].z, 0);
        }
    }
    
	private static void traversal_transform(Node root, float x, float y, float z, int _split) {
        if (root.isleaf == true)
        {
            // set left and right
            root.left = new Node();
            root.right = new Node();
            root.left.isleaf = true;
            root.right.isleaf = false;
            
            if (_split == 0) {
                root.left.xmax = root.x;
                root.left.xmin = root.xmin;
                root.right.xmax = root.xmax;
                root.right.xmin = root.x;
                
                root.left.ymax = root.ymax;
                root.left.ymin = root.ymin;
                root.right.ymax = root.ymax;
                root.right.ymin = root.ymin;
                
                root.left.zmax = root.zmax;
                root.left.zmin = root.zmin;
                root.right.zmax = root.zmax;
                root.right.zmin = root.zmin;
                
                if (root.x < x)
                {
                    root.left.x = root.x;
                    root.left.y = root.y;
                    root.left.z = root.z;
                    root.right.x = x;
                    root.right.y = y;
                    root.right.z = z;
                }
                else
                {
                    root.left.x = x;
                    root.left.y = y;
                    root.left.z = z;
                    root.right.x = root.x;
                    root.right.y = root.y;
                    root.right.z = root.z;
                }
            }
            else if (_split == 1) {
                root.left.xmax = root.xmax;
                root.left.xmin = root.xmin;
                root.right.xmax = root.xmax;
                root.right.xmin = root.xmin;
                
                root.left.ymax = root.y;
                root.left.ymin = root.ymin;
                root.right.ymax = root.ymax;
                root.right.ymin = root.y;
                
                root.left.zmax = root.zmax;
                root.left.zmin = root.zmin;
                root.right.zmax = root.zmax;
                root.right.zmin = root.zmin;
                
                if (root.y < y)
                {
                    root.left.x = root.x;
                    root.left.y = root.y;
                    root.left.z = root.z;
                    root.right.x = x;
                    root.right.y = y;
                    root.right.z = z;
                }
                else
                {
                    root.left.x = x;
                    root.left.y = y;
                    root.left.z = z;
                    root.right.x = root.x;
                    root.right.y = root.y;
                    root.right.z = root.z;
                }
            }
            else if (_split == 2) {
                root.left.xmax = root.xmax;
                root.left.xmin = root.xmin;
                root.right.xmax = root.xmax;
                root.right.xmin = root.xmin;
                
                root.left.ymax = root.ymax;
                root.left.ymin = root.ymin;
                root.right.ymax = root.ymax;
                root.right.ymin = root.ymin;
                
                root.left.zmax = root.z;
                root.left.zmin = root.zmin;
                root.right.zmax = root.zmax;
                root.right.zmin = root.z;
                
                if (root.z < z)
                {
                    root.left.x = root.x;
                    root.left.y = root.y;
                    root.left.z = root.z;
                    root.right.x = x;
                    root.right.y = y;
                    root.right.z = z;
                }
                else
                {
                    root.left.x = x;
                    root.left.y = y;
                    root.left.z = z;
                    root.right.x = root.x;
                    root.right.y = root.y;
                    root.right.z = root.z;
                }
            }
            
            root.isleaf = false;
        }
        else
        {
            if (_split == 0) {
                if (x < root.left.xmax) {
                    traversal_transform(root.left, x, y, z, (_split + 1) % 3);
                }
                else {
                    traversal_transform(root.right, x, y, z, (_split + 1) % 3);
                }
            }
            else if (_split == 1) {
                if (y < root.left.ymax) {
                    traversal_transform(root.left, x, y, z, (_split + 1) % 3);
                }
                else {
                    traversal_transform(root.right, x, y, z, (_split + 1) % 3);
                }
            }
            else if (_split == 2) {
                if (z < root.left.zmax) {
                    traversal_transform(root.left, x, y, z, (_split + 1) % 3);
                }
                else {
                    traversal_transform(root.right, x, y, z, (_split + 1) % 3);
                }
            }
        }
    }
    
	
	public static void main(String[] args) throws ExecutionException {
        if(args.length != 2) {
            System.err.println("inputFile numPoints");
            System.err.println("ex) input.in 10000");
            System.exit(-1);
        }
        
        nbodies = Integer.parseInt(args[1]);
        if (nbodies <= 0) {
            System.out.printf("Not enough input points!\n");
            System.exit(-1);
        }
        ReadInput(args[0]);
        
	    final Node root = new Node();
        
        Launcher.startTiming();
        callInsert(root, bodylist, nbodies);
        Launcher.stopTiming();
	}
}