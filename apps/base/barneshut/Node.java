/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package barneshut;

public class Node {
	OctTreeNodeData data;
    public Node child0;
    public Node child1;
    public Node child2;
    public Node child3;
    public Node child4;
    public Node child5;
    public Node child6;
    public Node child7;
    
    public double mass;
    public double posx;
    public double posy;
    public double posz;
    public boolean isLeaf;
	
	public Node(OctTreeNodeData data) {
		this.data = data;
	}
}
