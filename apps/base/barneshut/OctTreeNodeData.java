/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package barneshut;


/**
 * This class defines objects to hold the node data in the internal tree nodes
 * of the Barnes Hut application {@link barneshut.main.Main}.
 */
public class OctTreeNodeData { // the internal nodes are cells that summarize their children's properties
  public double mass;
  public double posx;
  public double posy;
  public double posz;

  /**
   * Constructor that initializes the mass with zero and the position with the
   * passed in values.
   * 
   * @param px
   *          double value used to initialize the x coordinate
   * @param py
   *          double value used to initialize the y coordinate
   * @param pz
   *          double value used to initialize the z coordinate
   */
  public OctTreeNodeData(double px, double py, double pz) {
    mass = 0.0;
    posx = px;
    posy = py;
    posz = pz;
//    id = nextId++;
  }

  /**
   * This method determines whether a tree node is a leaf.
   * 
   * @return a boolean indicating whether the current object is a leaf node
   */
    public boolean isLeaf = false;

  /**
   * This method converts the mass and position information into a string.
   * 
   * @return a string that summarizes the values in this node
   */
  @Override
  public String toString() {
    String result = "mass = " + mass;
    result += "pos = (" + posx + "," + posy + "," + posz + ")";
    return result;
  }

  /**
   * This method reads the x coordinate of this node.
   * 
   * @return a double value that represents the x coordinate
   */
  public double posx() {
    return posx;
  }

  /**
   * This method reads the y coordinate of this node.
   * 
   * @return a double value that represents the y coordinate
   */
  public double posy() {
    return posy;
  }

  /**
   * This method reads the z coordinate of this node.
   * 
   * @return a double value that represents the z coordinate
   */
  public double posz() {
    return posz;
  }
}
