/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package barneshut;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;

import util.Launcher;

/**
 * Main class of the Barnes Hut application.
 */
public final class Main {
	private static int nbodies; // number of bodies in system
	private static int ntimesteps; // number of time steps to run
	private static double dtime; // length of one time step
	private static double eps; // potential softening parameter
	private static double tol; // tolerance for stopping recursion, should be less than 0.57 for 3D case to bound error

	private static double dthf, epssq, itolsq;
	private static OctTreeLeafNodeData body[]; // the n bodies
	private static double diameter, centerx, centery, centerz;
	private static int curr;

	static boolean sort;

	/*
	 * Reads the input from a gzipped or clear text file.
	 */
	private static void ReadInput(String filename, boolean print) {
		double vx, vy, vz;
		Scanner in = null;
		try {
			if (filename.endsWith(".gz")) {
				in = new Scanner(new GZIPInputStream(new FileInputStream(filename)));
			} else {
				in = new Scanner(new BufferedReader(new FileReader(filename)));
			}
			in.useLocale(Locale.US);
		} catch (FileNotFoundException e) {
			System.err.println(e);
			System.exit(-1);
		} catch (IOException e) {
			System.err.println(e);
			System.exit(-1);
		}
		nbodies = in.nextInt();
		ntimesteps = in.nextInt();
		dtime = in.nextDouble();
		eps = in.nextDouble();
		tol = in.nextDouble();
		dthf = 0.5 * dtime;
		epssq = eps * eps;
		itolsq = 1.0 / (tol * tol);
		if (print) {
			System.err.println("configuration: " + nbodies + " bodies, " + ntimesteps + " time steps");
			System.err.println();
		}
		body = new OctTreeLeafNodeData[nbodies];
		for (int i = 0; i < nbodies; i++) {
			body[i] = new OctTreeLeafNodeData();
		}
		for (int i = 0; i < nbodies; i++) {
				body[i].mass = in.nextDouble();
				body[i].posx = in.nextDouble();
				body[i].posy = in.nextDouble();
				body[i].posz = in.nextDouble();
				vx = in.nextDouble();
				vy = in.nextDouble();
				vz = in.nextDouble();
				body[i].setVelocity(vx, vy, vz);
		}
	}
	
	private static void RandomInput(int num, boolean print) {
		double vx, vy, vz;
		Random rand = new Random(0);
		nbodies = num;
		ntimesteps = 1;
		dtime = 0.025;
		eps = 0.05;
		tol = 0.5;
		dthf = 0.5 * dtime;
		epssq = eps * eps;
		itolsq = 1.0 / (tol * tol);
		if (print) {
			System.err.println("configuration: " + nbodies + " bodies, " + ntimesteps + " time steps");
			System.err.println();
		}
		body = new OctTreeLeafNodeData[nbodies];
		for (int i = 0; i < nbodies; i++) {
			body[i] = new OctTreeLeafNodeData();
		}
		for (int i = 0; i < nbodies; i++) {
				body[i].mass = 1.0E-04;
				body[i].posx = rand.nextDouble();
				body[i].posy = rand.nextDouble();
				body[i].posz = rand.nextDouble();
				vx = rand.nextDouble();
				vy = rand.nextDouble();
				vz = rand.nextDouble();
				body[i].setVelocity(vx, vy, vz);
		}
	}

	/*
	 * Computes a bounding box around all the bodies.
	 */
	private static void ComputeCenterAndDiameter() {
		double minx, miny, minz;
		double maxx, maxy, maxz;
		double posx, posy, posz;
		minx = miny = minz = Double.MAX_VALUE;
		maxx = maxy = maxz = Double.MIN_VALUE;
		for (int i = 0; i < nbodies; i++) {
			posx = body[i].posx;
			posy = body[i].posy;
			posz = body[i].posz;
			if (minx > posx) {
				minx = posx;
			}
			if (miny > posy) {
				miny = posy;
			}
			if (minz > posz) {
				minz = posz;
			}
			if (maxx < posx) {
				maxx = posx;
			}
			if (maxy < posy) {
				maxy = posy;
			}
			if (maxz < posz) {
				maxz = posz;
			}
		}
		diameter = maxx - minx;
		if (diameter < (maxy - miny)) {
			diameter = (maxy - miny);
		}
		if (diameter < (maxz - minz)) {
			diameter = (maxz - minz);
		}
		centerx = (maxx + minx) * 0.5;
		centery = (maxy + miny) * 0.5;
		centerz = (maxz + minz) * 0.5;
	}

	/*
	 * Recursively inserts a body into the octree.
	 */
	private static void traversal_transform(Node root, double bmass, double bposx, double bposy, double bposz, boolean bisLeaf,
			double r) {
		double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        
        int i = 0;
        if (root.posx < bposx) {
            i = 1;
            x = r;
        }
        if (root.posy < bposy) {
            i += 2;
            y = r;
        }
        if (root.posz < bposz) {
            i += 4;
            z = r;
        }
        
        if (i == 0) {
            if (root.child0 == null) {
                root.child0 = new Node();
                root.child0.mass = bmass;
                root.child0.posx = bposx;
                root.child0.posx = bposy;
                root.child0.posx = bposz;
                root.child0.isLeaf = bisLeaf;
            }
            else {
                if (root.child0.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child0.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child0.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child0.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child0.child0 = new Node();
                        root.child0.child0.mass = root.child0.mass;
                        root.child0.child0.isLeaf = true;
                        root.child0.child0.posx = root.child0.posx;
                        root.child0.child0.posy = root.child0.posy;
                        root.child0.child0.posz = root.child0.posz;
                    }
                    else if (i2 == 1) {
                        root.child0.child1 = new Node();
                        root.child0.child1.mass = root.child0.mass;
                        root.child0.child1.isLeaf = true;
                        root.child0.child1.posx = root.child0.posx;
                        root.child0.child1.posy = root.child0.posy;
                        root.child0.child1.posz = root.child0.posz;
                    }
                    else if (i2 == 2) {
                        root.child0.child2 = new Node();
                        root.child0.child2.mass = root.child0.mass;
                        root.child0.child2.isLeaf = true;
                        root.child0.child2.posx = root.child0.posx;
                        root.child0.child2.posy = root.child0.posy;
                        root.child0.child2.posz = root.child0.posz;
                    }
                    else if (i2 == 3) {
                        root.child0.child3 = new Node();
                        root.child0.child3.mass = root.child0.mass;
                        root.child0.child3.isLeaf = true;
                        root.child0.child3.posx = root.child0.posx;
                        root.child0.child3.posy = root.child0.posy;
                        root.child0.child3.posz = root.child0.posz;
                    }
                    else if (i2 == 4) {
                        root.child0.child4 = new Node();
                        root.child0.child4.mass = root.child0.mass;
                        root.child0.child4.isLeaf = true;
                        root.child0.child4.posx = root.child0.posx;
                        root.child0.child4.posy = root.child0.posy;
                        root.child0.child4.posz = root.child0.posz;
                    }
                    else if (i2 == 5) {
                        root.child0.child5 = new Node();
                        root.child0.child5.mass = root.child0.mass;
                        root.child0.child5.isLeaf = true;
                        root.child0.child5.posx = root.child0.posx;
                        root.child0.child5.posy = root.child0.posy;
                        root.child0.child5.posz = root.child0.posz;
                    }
                    else if (i2 == 6) {
                        root.child0.child6 = new Node();
                        root.child0.child6.mass = root.child0.mass;
                        root.child0.child6.isLeaf = true;
                        root.child0.child6.posx = root.child0.posx;
                        root.child0.child6.posy = root.child0.posy;
                        root.child0.child6.posz = root.child0.posz;
                    }
                    else if (i2 == 7) {
                        root.child0.child7 = new Node();
                        root.child0.child7.mass = root.child0.mass;
                        root.child0.child7.isLeaf = true;
                        root.child0.child7.posx = root.child0.posx;
                        root.child0.child7.posy = root.child0.posy;
                        root.child0.child7.posz = root.child0.posz;
                    }
                    
                    root.child0.mass = 0.0;
                    root.child0.isLeaf = false;
                    root.child0.posx = tposx;
                    root.child0.posy = tposy;
                    root.child0.posz = tposz;
                }
                traversal_transform(root.child0, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 1) {
            if (root.child1 == null) {
                root.child1 = new Node();
                root.child1.mass = bmass;
                root.child1.posx = bposx;
                root.child1.posx = bposy;
                root.child1.posx = bposz;
                root.child1.isLeaf = bisLeaf;
            }
            else {
                if (root.child1.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child1.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child1.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child1.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child1.child0 = new Node();
                        root.child1.child0.mass = root.child1.mass;
                        root.child1.child0.isLeaf = true;
                        root.child1.child0.posx = root.child1.posx;
                        root.child1.child0.posy = root.child1.posy;
                        root.child1.child0.posz = root.child1.posz;
                    }
                    else if (i2 == 1) {
                        root.child1.child1 = new Node();
                        root.child1.child1.mass = root.child1.mass;
                        root.child1.child1.isLeaf = true;
                        root.child1.child1.posx = root.child1.posx;
                        root.child1.child1.posy = root.child1.posy;
                        root.child1.child1.posz = root.child1.posz;
                    }
                    else if (i2 == 2) {
                        root.child1.child2 = new Node();
                        root.child1.child2.mass = root.child1.mass;
                        root.child1.child2.isLeaf = true;
                        root.child1.child2.posx = root.child1.posx;
                        root.child1.child2.posy = root.child1.posy;
                        root.child1.child2.posz = root.child1.posz;
                    }
                    else if (i2 == 3) {
                        root.child1.child3 = new Node();
                        root.child1.child3.mass = root.child1.mass;
                        root.child1.child3.isLeaf = true;
                        root.child1.child3.posx = root.child1.posx;
                        root.child1.child3.posy = root.child1.posy;
                        root.child1.child3.posz = root.child1.posz;
                    }
                    else if (i2 == 4) {
                        root.child1.child4 = new Node();
                        root.child1.child4.mass = root.child1.mass;
                        root.child1.child4.isLeaf = true;
                        root.child1.child4.posx = root.child1.posx;
                        root.child1.child4.posy = root.child1.posy;
                        root.child1.child4.posz = root.child1.posz;
                    }
                    else if (i2 == 5) {
                        root.child1.child5 = new Node();
                        root.child1.child5.mass = root.child1.mass;
                        root.child1.child5.isLeaf = true;
                        root.child1.child5.posx = root.child1.posx;
                        root.child1.child5.posy = root.child1.posy;
                        root.child1.child5.posz = root.child1.posz;
                    }
                    else if (i2 == 6) {
                        root.child1.child6 = new Node();
                        root.child1.child6.mass = root.child1.mass;
                        root.child1.child6.isLeaf = true;
                        root.child1.child6.posx = root.child1.posx;
                        root.child1.child6.posy = root.child1.posy;
                        root.child1.child6.posz = root.child1.posz;
                    }
                    else if (i2 == 7) {
                        root.child1.child7 = new Node();
                        root.child1.child7.mass = root.child1.mass;
                        root.child1.child7.isLeaf = true;
                        root.child1.child7.posx = root.child1.posx;
                        root.child1.child7.posy = root.child1.posy;
                        root.child1.child7.posz = root.child1.posz;
                    }
                    
                    root.child1.mass = 0.0;
                    root.child1.isLeaf = false;
                    root.child1.posx = tposx;
                    root.child1.posy = tposy;
                    root.child1.posz = tposz;
                }
                traversal_transform(root.child1, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 2) {
            if (root.child2 == null) {
                root.child2 = new Node();
                root.child2.mass = bmass;
                root.child2.posx = bposx;
                root.child2.posx = bposy;
                root.child2.posx = bposz;
                root.child2.isLeaf = bisLeaf;
            }
            else {
                if (root.child2.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child2.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child2.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child2.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child2.child0 = new Node();
                        root.child2.child0.mass = root.child2.mass;
                        root.child2.child0.isLeaf = true;
                        root.child2.child0.posx = root.child2.posx;
                        root.child2.child0.posy = root.child2.posy;
                        root.child2.child0.posz = root.child2.posz;
                    }
                    else if (i2 == 1) {
                        root.child2.child1 = new Node();
                        root.child2.child1.mass = root.child2.mass;
                        root.child2.child1.isLeaf = true;
                        root.child2.child1.posx = root.child2.posx;
                        root.child2.child1.posy = root.child2.posy;
                        root.child2.child1.posz = root.child2.posz;
                    }
                    else if (i2 == 2) {
                        root.child2.child2 = new Node();
                        root.child2.child2.mass = root.child2.mass;
                        root.child2.child2.isLeaf = true;
                        root.child2.child2.posx = root.child2.posx;
                        root.child2.child2.posy = root.child2.posy;
                        root.child2.child2.posz = root.child2.posz;
                    }
                    else if (i2 == 3) {
                        root.child2.child3 = new Node();
                        root.child2.child3.mass = root.child2.mass;
                        root.child2.child3.isLeaf = true;
                        root.child2.child3.posx = root.child2.posx;
                        root.child2.child3.posy = root.child2.posy;
                        root.child2.child3.posz = root.child2.posz;
                    }
                    else if (i2 == 4) {
                        root.child2.child4 = new Node();
                        root.child2.child4.mass = root.child2.mass;
                        root.child2.child4.isLeaf = true;
                        root.child2.child4.posx = root.child2.posx;
                        root.child2.child4.posy = root.child2.posy;
                        root.child2.child4.posz = root.child2.posz;
                    }
                    else if (i2 == 5) {
                        root.child2.child5 = new Node();
                        root.child2.child5.mass = root.child2.mass;
                        root.child2.child5.isLeaf = true;
                        root.child2.child5.posx = root.child2.posx;
                        root.child2.child5.posy = root.child2.posy;
                        root.child2.child5.posz = root.child2.posz;
                    }
                    else if (i2 == 6) {
                        root.child2.child6 = new Node();
                        root.child2.child6.mass = root.child2.mass;
                        root.child2.child6.isLeaf = true;
                        root.child2.child6.posx = root.child2.posx;
                        root.child2.child6.posy = root.child2.posy;
                        root.child2.child6.posz = root.child2.posz;
                    }
                    else if (i2 == 7) {
                        root.child2.child7 = new Node();
                        root.child2.child7.mass = root.child2.mass;
                        root.child2.child7.isLeaf = true;
                        root.child2.child7.posx = root.child2.posx;
                        root.child2.child7.posy = root.child2.posy;
                        root.child2.child7.posz = root.child2.posz;
                    }
                    
                    root.child2.mass = 0.0;
                    root.child2.isLeaf = false;
                    root.child2.posx = tposx;
                    root.child2.posy = tposy;
                    root.child2.posz = tposz;
                }
                traversal_transform(root.child2, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 3) {
            if (root.child3 == null) {
                root.child3 = new Node();
                root.child3.mass = bmass;
                root.child3.posx = bposx;
                root.child3.posx = bposy;
                root.child3.posx = bposz;
                root.child3.isLeaf = bisLeaf;
            }
            else {
                if (root.child3.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child3.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child3.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child3.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child3.child0 = new Node();
                        root.child3.child0.mass = root.child3.mass;
                        root.child3.child0.isLeaf = true;
                        root.child3.child0.posx = root.child3.posx;
                        root.child3.child0.posy = root.child3.posy;
                        root.child3.child0.posz = root.child3.posz;
                    }
                    else if (i2 == 1) {
                        root.child3.child1 = new Node();
                        root.child3.child1.mass = root.child3.mass;
                        root.child3.child1.isLeaf = true;
                        root.child3.child1.posx = root.child3.posx;
                        root.child3.child1.posy = root.child3.posy;
                        root.child3.child1.posz = root.child3.posz;
                    }
                    else if (i2 == 2) {
                        root.child3.child2 = new Node();
                        root.child3.child2.mass = root.child3.mass;
                        root.child3.child2.isLeaf = true;
                        root.child3.child2.posx = root.child3.posx;
                        root.child3.child2.posy = root.child3.posy;
                        root.child3.child2.posz = root.child3.posz;
                    }
                    else if (i2 == 3) {
                        root.child3.child3 = new Node();
                        root.child3.child3.mass = root.child3.mass;
                        root.child3.child3.isLeaf = true;
                        root.child3.child3.posx = root.child3.posx;
                        root.child3.child3.posy = root.child3.posy;
                        root.child3.child3.posz = root.child3.posz;
                    }
                    else if (i2 == 4) {
                        root.child3.child4 = new Node();
                        root.child3.child4.mass = root.child3.mass;
                        root.child3.child4.isLeaf = true;
                        root.child3.child4.posx = root.child3.posx;
                        root.child3.child4.posy = root.child3.posy;
                        root.child3.child4.posz = root.child3.posz;
                    }
                    else if (i2 == 5) {
                        root.child3.child5 = new Node();
                        root.child3.child5.mass = root.child3.mass;
                        root.child3.child5.isLeaf = true;
                        root.child3.child5.posx = root.child3.posx;
                        root.child3.child5.posy = root.child3.posy;
                        root.child3.child5.posz = root.child3.posz;
                    }
                    else if (i2 == 6) {
                        root.child3.child6 = new Node();
                        root.child3.child6.mass = root.child3.mass;
                        root.child3.child6.isLeaf = true;
                        root.child3.child6.posx = root.child3.posx;
                        root.child3.child6.posy = root.child3.posy;
                        root.child3.child6.posz = root.child3.posz;
                    }
                    else if (i2 == 7) {
                        root.child3.child7 = new Node();
                        root.child3.child7.mass = root.child3.mass;
                        root.child3.child7.isLeaf = true;
                        root.child3.child7.posx = root.child3.posx;
                        root.child3.child7.posy = root.child3.posy;
                        root.child3.child7.posz = root.child3.posz;
                    }
                    
                    root.child3.mass = 0.0;
                    root.child3.isLeaf = false;
                    root.child3.posx = tposx;
                    root.child3.posy = tposy;
                    root.child3.posz = tposz;
                }
                traversal_transform(root.child3, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 4) {
            if (root.child4 == null) {
                root.child4 = new Node();
                root.child4.mass = bmass;
                root.child4.posx = bposx;
                root.child4.posx = bposy;
                root.child4.posx = bposz;
                root.child4.isLeaf = bisLeaf;
            }
            else {
                if (root.child4.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child4.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child4.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child4.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child4.child0 = new Node();
                        root.child4.child0.mass = root.child4.mass;
                        root.child4.child0.isLeaf = true;
                        root.child4.child0.posx = root.child4.posx;
                        root.child4.child0.posy = root.child4.posy;
                        root.child4.child0.posz = root.child4.posz;
                    }
                    else if (i2 == 1) {
                        root.child4.child1 = new Node();
                        root.child4.child1.mass = root.child4.mass;
                        root.child4.child1.isLeaf = true;
                        root.child4.child1.posx = root.child4.posx;
                        root.child4.child1.posy = root.child4.posy;
                        root.child4.child1.posz = root.child4.posz;
                    }
                    else if (i2 == 2) {
                        root.child4.child2 = new Node();
                        root.child4.child2.mass = root.child4.mass;
                        root.child4.child2.isLeaf = true;
                        root.child4.child2.posx = root.child4.posx;
                        root.child4.child2.posy = root.child4.posy;
                        root.child4.child2.posz = root.child4.posz;
                    }
                    else if (i2 == 3) {
                        root.child4.child3 = new Node();
                        root.child4.child3.mass = root.child4.mass;
                        root.child4.child3.isLeaf = true;
                        root.child4.child3.posx = root.child4.posx;
                        root.child4.child3.posy = root.child4.posy;
                        root.child4.child3.posz = root.child4.posz;
                    }
                    else if (i2 == 4) {
                        root.child4.child4 = new Node();
                        root.child4.child4.mass = root.child4.mass;
                        root.child4.child4.isLeaf = true;
                        root.child4.child4.posx = root.child4.posx;
                        root.child4.child4.posy = root.child4.posy;
                        root.child4.child4.posz = root.child4.posz;
                    }
                    else if (i2 == 5) {
                        root.child4.child5 = new Node();
                        root.child4.child5.mass = root.child4.mass;
                        root.child4.child5.isLeaf = true;
                        root.child4.child5.posx = root.child4.posx;
                        root.child4.child5.posy = root.child4.posy;
                        root.child4.child5.posz = root.child4.posz;
                    }
                    else if (i2 == 6) {
                        root.child4.child6 = new Node();
                        root.child4.child6.mass = root.child4.mass;
                        root.child4.child6.isLeaf = true;
                        root.child4.child6.posx = root.child4.posx;
                        root.child4.child6.posy = root.child4.posy;
                        root.child4.child6.posz = root.child4.posz;
                    }
                    else if (i2 == 7) {
                        root.child4.child7 = new Node();
                        root.child4.child7.mass = root.child4.mass;
                        root.child4.child7.isLeaf = true;
                        root.child4.child7.posx = root.child4.posx;
                        root.child4.child7.posy = root.child4.posy;
                        root.child4.child7.posz = root.child4.posz;
                    }
                    
                    root.child4.mass = 0.0;
                    root.child4.isLeaf = false;
                    root.child4.posx = tposx;
                    root.child4.posy = tposy;
                    root.child4.posz = tposz;
                }
                traversal_transform(root.child4, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 5) {
            if (root.child5 == null) {
                root.child5 = new Node();
                root.child5.mass = bmass;
                root.child5.posx = bposx;
                root.child5.posx = bposy;
                root.child5.posx = bposz;
                root.child5.isLeaf = bisLeaf;
            }
            else {
                if (root.child5.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child5.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child5.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child5.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child5.child0 = new Node();
                        root.child5.child0.mass = root.child5.mass;
                        root.child5.child0.isLeaf = true;
                        root.child5.child0.posx = root.child5.posx;
                        root.child5.child0.posy = root.child5.posy;
                        root.child5.child0.posz = root.child5.posz;
                    }
                    else if (i2 == 1) {
                        root.child5.child1 = new Node();
                        root.child5.child1.mass = root.child5.mass;
                        root.child5.child1.isLeaf = true;
                        root.child5.child1.posx = root.child5.posx;
                        root.child5.child1.posy = root.child5.posy;
                        root.child5.child1.posz = root.child5.posz;
                    }
                    else if (i2 == 2) {
                        root.child5.child2 = new Node();
                        root.child5.child2.mass = root.child5.mass;
                        root.child5.child2.isLeaf = true;
                        root.child5.child2.posx = root.child5.posx;
                        root.child5.child2.posy = root.child5.posy;
                        root.child5.child2.posz = root.child5.posz;
                    }
                    else if (i2 == 3) {
                        root.child5.child3 = new Node();
                        root.child5.child3.mass = root.child5.mass;
                        root.child5.child3.isLeaf = true;
                        root.child5.child3.posx = root.child5.posx;
                        root.child5.child3.posy = root.child5.posy;
                        root.child5.child3.posz = root.child5.posz;
                    }
                    else if (i2 == 4) {
                        root.child5.child4 = new Node();
                        root.child5.child4.mass = root.child5.mass;
                        root.child5.child4.isLeaf = true;
                        root.child5.child4.posx = root.child5.posx;
                        root.child5.child4.posy = root.child5.posy;
                        root.child5.child4.posz = root.child5.posz;
                    }
                    else if (i2 == 5) {
                        root.child5.child5 = new Node();
                        root.child5.child5.mass = root.child5.mass;
                        root.child5.child5.isLeaf = true;
                        root.child5.child5.posx = root.child5.posx;
                        root.child5.child5.posy = root.child5.posy;
                        root.child5.child5.posz = root.child5.posz;
                    }
                    else if (i2 == 6) {
                        root.child5.child6 = new Node();
                        root.child5.child6.mass = root.child5.mass;
                        root.child5.child6.isLeaf = true;
                        root.child5.child6.posx = root.child5.posx;
                        root.child5.child6.posy = root.child5.posy;
                        root.child5.child6.posz = root.child5.posz;
                    }
                    else if (i2 == 7) {
                        root.child5.child7 = new Node();
                        root.child5.child7.mass = root.child5.mass;
                        root.child5.child7.isLeaf = true;
                        root.child5.child7.posx = root.child5.posx;
                        root.child5.child7.posy = root.child5.posy;
                        root.child5.child7.posz = root.child5.posz;
                    }
                    
                    root.child5.mass = 0.0;
                    root.child5.isLeaf = false;
                    root.child5.posx = tposx;
                    root.child5.posy = tposy;
                    root.child5.posz = tposz;
                }
                traversal_transform(root.child5, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 6) {
            if (root.child6 == null) {
                root.child6 = new Node();
                root.child6.mass = bmass;
                root.child6.posx = bposx;
                root.child6.posx = bposy;
                root.child6.posx = bposz;
                root.child6.isLeaf = bisLeaf;
            }
            else {
                if (root.child6.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child6.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child6.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child6.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child6.child0 = new Node();
                        root.child6.child0.mass = root.child6.mass;
                        root.child6.child0.isLeaf = true;
                        root.child6.child0.posx = root.child6.posx;
                        root.child6.child0.posy = root.child6.posy;
                        root.child6.child0.posz = root.child6.posz;
                    }
                    else if (i2 == 1) {
                        root.child6.child1 = new Node();
                        root.child6.child1.mass = root.child6.mass;
                        root.child6.child1.isLeaf = true;
                        root.child6.child1.posx = root.child6.posx;
                        root.child6.child1.posy = root.child6.posy;
                        root.child6.child1.posz = root.child6.posz;
                    }
                    else if (i2 == 2) {
                        root.child6.child2 = new Node();
                        root.child6.child2.mass = root.child6.mass;
                        root.child6.child2.isLeaf = true;
                        root.child6.child2.posx = root.child6.posx;
                        root.child6.child2.posy = root.child6.posy;
                        root.child6.child2.posz = root.child6.posz;
                    }
                    else if (i2 == 3) {
                        root.child6.child3 = new Node();
                        root.child6.child3.mass = root.child6.mass;
                        root.child6.child3.isLeaf = true;
                        root.child6.child3.posx = root.child6.posx;
                        root.child6.child3.posy = root.child6.posy;
                        root.child6.child3.posz = root.child6.posz;
                    }
                    else if (i2 == 4) {
                        root.child6.child4 = new Node();
                        root.child6.child4.mass = root.child6.mass;
                        root.child6.child4.isLeaf = true;
                        root.child6.child4.posx = root.child6.posx;
                        root.child6.child4.posy = root.child6.posy;
                        root.child6.child4.posz = root.child6.posz;
                    }
                    else if (i2 == 5) {
                        root.child6.child5 = new Node();
                        root.child6.child5.mass = root.child6.mass;
                        root.child6.child5.isLeaf = true;
                        root.child6.child5.posx = root.child6.posx;
                        root.child6.child5.posy = root.child6.posy;
                        root.child6.child5.posz = root.child6.posz;
                    }
                    else if (i2 == 6) {
                        root.child6.child6 = new Node();
                        root.child6.child6.mass = root.child6.mass;
                        root.child6.child6.isLeaf = true;
                        root.child6.child6.posx = root.child6.posx;
                        root.child6.child6.posy = root.child6.posy;
                        root.child6.child6.posz = root.child6.posz;
                    }
                    else if (i2 == 7) {
                        root.child6.child7 = new Node();
                        root.child6.child7.mass = root.child6.mass;
                        root.child6.child7.isLeaf = true;
                        root.child6.child7.posx = root.child6.posx;
                        root.child6.child7.posy = root.child6.posy;
                        root.child6.child7.posz = root.child6.posz;
                    }
                    
                    root.child6.mass = 0.0;
                    root.child6.isLeaf = false;
                    root.child6.posx = tposx;
                    root.child6.posy = tposy;
                    root.child6.posz = tposz;
                }
                traversal_transform(root.child6, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
        else if (i == 7) {
            if (root.child7 == null) {
                root.child7 = new Node();
                root.child7.mass = bmass;
                root.child7.posx = bposx;
                root.child7.posx = bposy;
                root.child7.posx = bposz;
                root.child7.isLeaf = bisLeaf;
            }
            else {
                if (root.child7.isLeaf == true) {
                    double tposx = root.posx - (0.5 * r) + x;
                    double tposy = root.posy - (0.5 * r) + y;
                    double tposz = root.posz - (0.5 * r) + z;
                    
                    int i2 = 0;
                    if (tposx < root.child7.posx) {
                        i2 = 1;
                    }
                    if (tposy < root.child7.posy) {
                        i2 += 2;
                    }
                    if (tposz < root.child7.posz) {
                        i2 += 4;
                    }
                    
                    if (i2 == 0) {
                        root.child7.child0 = new Node();
                        root.child7.child0.mass = root.child7.mass;
                        root.child7.child0.isLeaf = true;
                        root.child7.child0.posx = root.child7.posx;
                        root.child7.child0.posy = root.child7.posy;
                        root.child7.child0.posz = root.child7.posz;
                    }
                    else if (i2 == 1) {
                        root.child7.child1 = new Node();
                        root.child7.child1.mass = root.child7.mass;
                        root.child7.child1.isLeaf = true;
                        root.child7.child1.posx = root.child7.posx;
                        root.child7.child1.posy = root.child7.posy;
                        root.child7.child1.posz = root.child7.posz;
                    }
                    else if (i2 == 2) {
                        root.child7.child2 = new Node();
                        root.child7.child2.mass = root.child7.mass;
                        root.child7.child2.isLeaf = true;
                        root.child7.child2.posx = root.child7.posx;
                        root.child7.child2.posy = root.child7.posy;
                        root.child7.child2.posz = root.child7.posz;
                    }
                    else if (i2 == 3) {
                        root.child7.child3 = new Node();
                        root.child7.child3.mass = root.child7.mass;
                        root.child7.child3.isLeaf = true;
                        root.child7.child3.posx = root.child7.posx;
                        root.child7.child3.posy = root.child7.posy;
                        root.child7.child3.posz = root.child7.posz;
                    }
                    else if (i2 == 4) {
                        root.child7.child4 = new Node();
                        root.child7.child4.mass = root.child7.mass;
                        root.child7.child4.isLeaf = true;
                        root.child7.child4.posx = root.child7.posx;
                        root.child7.child4.posy = root.child7.posy;
                        root.child7.child4.posz = root.child7.posz;
                    }
                    else if (i2 == 5) {
                        root.child7.child5 = new Node();
                        root.child7.child5.mass = root.child7.mass;
                        root.child7.child5.isLeaf = true;
                        root.child7.child5.posx = root.child7.posx;
                        root.child7.child5.posy = root.child7.posy;
                        root.child7.child5.posz = root.child7.posz;
                    }
                    else if (i2 == 6) {
                        root.child7.child6 = new Node();
                        root.child7.child6.mass = root.child7.mass;
                        root.child7.child6.isLeaf = true;
                        root.child7.child6.posx = root.child7.posx;
                        root.child7.child6.posy = root.child7.posy;
                        root.child7.child6.posz = root.child7.posz;
                    }
                    else if (i2 == 7) {
                        root.child7.child7 = new Node();
                        root.child7.child7.mass = root.child7.mass;
                        root.child7.child7.isLeaf = true;
                        root.child7.child7.posx = root.child7.posx;
                        root.child7.child7.posy = root.child7.posy;
                        root.child7.child7.posz = root.child7.posz;
                    }
                    
                    root.child7.mass = 0.0;
                    root.child7.isLeaf = false;
                    root.child7.posx = tposx;
                    root.child7.posy = tposy;
                    root.child7.posz = tposz;
                }
                traversal_transform(root.child7, bmass, bposx, bposy, bposz, bisleaf, (0.5 * r));
            }
        }
	}

    /**
	 * Main method to launch the binary search tree code.
	 */
	public static void main(String args[]) throws ExecutionException {
		
		if (args.length != 3) {
			System.err.println("arguments: sort randomize [inputFile / numBodies]");
			System.err.println("ex) true false BarnesHutA.in.gz");
			System.err.println("ex) true true 100000");
			System.exit(-1);
		}
		sort = Boolean.parseBoolean(args[0]);
		boolean randomize = Boolean.parseBoolean(args[1]);
		if (randomize) {
			RandomInput(Integer.parseInt(args[2]), true);
		} else {
			ReadInput(args[2], true);	
		}
		
		OctTreeNodeData res = null;

		for (int step = 0; step < ntimesteps; step++) { // time-step the system
			final int s = step;
			ComputeCenterAndDiameter();
			final Node root = new Node(new OctTreeNodeData(centerx, centery, centerz)); // create the
			double radius = diameter * 0.5;
			
			Launcher.startTiming();
			for (int i = 0; i < nbodies; i++) {
				traversal_transform(root, body[i].mass, body[i].posx, body[i].posy, body[i].posz, true, radius); // grow the tree by inserting each body
			}
			Launcher.stopTiming();
		}
	}
}
