/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package barneshut;

/**
 * This class defines objects to hold the node data in the leaf nodes (the
 * bodies) of the tree built by the Barnes Hut application
 * {@link barneshut.main.Main}.
 */
public class OctTreeLeafNodeData extends OctTreeNodeData { // the tree leaves are the bodies
  public double velx;
  public double vely;
  public double velz;
  public double accx;
  public double accy;
  public double accz;
  
  public int nodesTraversed = 0;
  
  /**
   * Constructor that initializes the mass, position, velocity, and acceleration
   * with zeros.
   */
  public OctTreeLeafNodeData() {
    super(0.0, 0.0, 0.0);
    velx = 0.0;
    vely = 0.0;
    velz = 0.0;
    accx = 0.0;
    accy = 0.0;
    accz = 0.0;
  }

  /**
   * This method determines whether a tree node is a leaf.
   * 
   * @return a boolean indicating whether the current object is a leaf node
   */
  @Override
    public boolean isLeaf = true;

  /**
   * This method sets the velocity to the passed in values.
   * 
   * @param x
   *          double value used to initialize the x component of the velocity
   * @param y
   *          double value used to initialize the y component of the velocity
   * @param z
   *          double value used to initialize the z component of the velocity
   */
  public void setVelocity(double x, double y, double z) {
    velx = x;
    vely = y;
    velz = z;
  }

  /**
   * This method determines whether the current node has the same position as
   * the passed node.
   * 
   * @param o
   *          the object (a node) whose position is compared to the current
   *          node's position
   * @return a boolean indicating whether the position is identical
   */
  @Override
  public boolean equals(Object o) {
    OctTreeLeafNodeData n = (OctTreeLeafNodeData) o;
    return (this.posx == n.posx && this.posy == n.posy && this.posz == n.posz);
  }

  /**
   * This method converts the mass, position, velocity, and acceleration
   * information into a string.
   * 
   * @return a string that summarizes the values in this node
   */
  @Override
  public String toString() {
    String result = super.toString();
    result += "vel = (" + velx + "," + vely + "," + velz + ")";
    result += "acc = (" + accx + "," + accy + "," + accz + ")";
    return result;
  }
}
