/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Papi {
	private static Logger logger = Logger.getLogger("util.Papi");
	private static boolean loaded = false;

	private static final String lib = "util_Papi";

	static {
		try {
			System.loadLibrary(lib);
			loaded = true;
		} catch (Error e) {
			logger.warning(String.format("Could not load library %s: %s", lib, e.toString()));
		}
	}

	private static native int _startSystem(String[] counters);
	private static native void _finishSystem();
	private static native void _readThread(int tid, long[] values);
	private static native void _startThread(int tid);
	private static native void _finishThread(int tid);

	private static String [] counters;
	private static final int numCounters = 3;
	private static List<Long> [] results;
	
	public static void init() {
		results = new List[numCounters];
		for (int i = 0; i < numCounters; i++) {
			results[i] = new ArrayList<Long>();
		}
	}

	public static int startSystem(int eventIndex) {
		String [] counters = null;
		if (eventIndex == 1) {
			counters = new String[] { "PAPI_TOT_INS", "PAPI_TOT_CYC", "PAPI_L1_DCM" };
		} else if (eventIndex == 2) {
			counters = new String[] { "PAPI_L1_DCA", "PAPI_L2_DCM", "PAPI_L2_DCA" };
		} else {
			assert false;
		}
		System.out.println("Profiling with PAPI: ");
		for (String s : counters) {
			System.out.println(s);
		}
		return startSystem(counters);
	}

	public static int startSystem(String[] c) {
		counters = c;
		if (loaded)
			return _startSystem(counters);
		return 0;
	}

	public static void finishSystem() {
		if (loaded)
			_finishSystem();
	}

	public static void readThread(int tid) {
		long [] values = new long[numCounters];
		if (loaded)
			_readThread(tid, values);
		for (int i = 0; i < numCounters; i++) {
			System.out.printf("counter%d:%d\n", i, values[i]);
			results[i].add(values[i]);
		}
	}

	public static void readThread(int tid, long[] values) {
		if (loaded)
			_readThread(tid, values);
	}

	public static void startThread(int tid) {
		if (loaded)
			_startThread(tid);
	}

	public static void finishThread(int tid) {
		if (loaded)
			_finishThread(tid);
	}

	public static boolean isLoaded() {
		return loaded;
	}

	public static void papiDone(int papiCounters, int drop, int total, String desc, String main, String argstr) throws FileNotFoundException {
		int count = total - drop;
		PrintStream csv = new PrintStream(new FileOutputStream("papi.csv", true));
		if (papiCounters == 1) csv.printf("%s, %s, %s, ", desc, main, argstr);
		for (int i = 0; i < numCounters; i++) {
			long sum = 0;
			for (int j = drop; j < total; j++) {
				sum += results[i].get(j);
			}
			double avg = (double) sum / count;
			csv.printf("%.2f, ", avg);
		}
		if (papiCounters == 2) csv.print("\n");
		csv.close();
	}

	private static class Test implements Runnable {
		private final int id;
		private final int iterations;
		private final long[] counters;
		public Test(int id, int events, int iterations) {
			this.id = id;
			this.iterations = iterations;
			counters = new long[events];
		}

		@Override
		public void run() {
			startThread(id);
			double c = 0.11;
			double a = 0.5;
			double b = 6.2;
			for (int i = 0; i < iterations; i++)
				c += a * b;
			readThread(id, counters);
			finishThread(id);
			long ins = counters[0];
			long cyc = counters[1];
			//double ipc = counters[1] != 0 ? (double) counters[0] / counters[1] : 0.0;
			long l1dcm = counters[2];
			System.out.printf("id: %d ins:%d cyc:%d l1dcm:%d\n", id, ins, cyc, l1dcm);
		}
	}

	public static void main(String[] args) throws Exception {
		int numThreads = 4;
		int iterations = 1000;

		switch (args.length) {
		default:
		case 2: iterations = Integer.parseInt(args[1]);
		case 1: numThreads = Integer.parseInt(args[0]);
		case 0:
		}

		System.out.println("test PAPI");
		String [] events = new String[] { "PAPI_TOT_INS", "PAPI_TOT_CYC", "PAPI_L1_DCM" };
		int num = startSystem(events);
		System.out.println(num + "events initialized");
		if (num < 2) {
			throw new Error("Could not initialize events");
		}
		try {
			Thread[] threads = new Thread[numThreads];
			for (int i = 0; i < numThreads; i++) {
				threads[i] = new Thread(new Test(i, num, iterations));
			}
			for (int i = 0; i < numThreads; i++) {
				threads[i].start();
			}
			for (int i = 0; i < numThreads; i++) {
				threads[i].join();
			}
		} finally {
			finishSystem();
		}
	}
}
