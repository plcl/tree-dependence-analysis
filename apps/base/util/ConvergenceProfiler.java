/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */
package util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class ConvergenceProfiler extends FootprintSizeProfiler {
	HashMap<Object, Wrapper> map;
	Wrapper [] wrappers;
	HashSet<Object> [] sets;
	int nextWrapperIdx;
	int nextSetIdx;
	double avgConvergenceSum;

	int setSize;
	boolean useSampling;
	final int maxProfileSize = 32;
	Random rand;
	int [] randIdxs;
	Wrapper nullWrapper = new Wrapper(null);

	class Wrapper {
		HashSet<Object> sets;

		Wrapper(HashSet<Object> sets) {
			this.sets = sets;
		}
	}

	public ConvergenceProfiler(int b) {
		super(b);
		reset();
	}

	protected void reset() {
		super.reset();
		nextWrapperIdx = 0;
		nextSetIdx = 0;
		avgConvergenceSum = 0;
		map = new HashMap<Object, Wrapper>();
		if (blockSize > maxProfileSize) {
			setSize = maxProfileSize;
			useSampling = true;
			rand = new Random();
			randIdxs = new int[setSize];
		} else {
			useSampling = false;
			setSize = blockSize;
		}
		sets = new HashSet[setSize];
		for (int i = 0; i < setSize; i++) {
			sets[i] = new HashSet<Object>();
		}		
		wrappers = new Wrapper[blockSize];
		if (!useSampling) {
			// do all-pairs comparison; set wrappers once and don't change them
			for (int i = 0; i < blockSize; i++) {
				wrappers[i] = new Wrapper(sets[nextSetIdx++]);
			}			
		} else {
			// take [setSize] samples; init all to nullWrapper here, then set random sample points in setWrappers()
			for (int i = 0; i < blockSize; i++) {
				wrappers[i] = nullWrapper;
			}
			setSampleWrappers();
		}
	}

	public void touch(Object key, Object o) {
		super.touch(key, o);
		Wrapper wrapper = map.get(key);
		if (wrapper == null) {
			wrapper = wrappers[nextWrapperIdx++];
			map.put(key, wrapper);
			// to treat shadow rays as part of initial ray
			if (nextWrapperIdx == blockSize) {
				nextWrapperIdx = 0;
			}
		}
		if (wrapper.sets != null) {
			wrapper.sets.add(o);
		}
	}

	public void oneBlockDone() {
		double convergenceSum = 0;
		int cnt = 0;
		double avgConvergence;
		if (blockSize == 1) {
			avgConvergence = 1.0;
			cnt = 1;
		} else {
			for (int i = 0; i < nextSetIdx; i++) {
				for (int j = i + 1; j < nextSetIdx; j++) {
					if (!sets[i].isEmpty() && !sets[j].isEmpty()) {
						double conv = calcConvergence(sets[i], sets[j]);
						//System.out.println("\t" + i + " " + j + "\t" + conv);				
						convergenceSum += conv;
						cnt++;
					}
				}
			}
			avgConvergence = convergenceSum / cnt;
		}

		if (cnt > 0) {
			// if last block had no valid samples, ignore it
			avgConvergenceSum += avgConvergence;
			int footprint = footprintSet.size();
			footprintSum += footprint;
			if (footprint < footprintMin) {
				footprintMin = footprint;
			}
			if (footprint > footprintMax) {
				footprintMax = footprint;
			}
			//System.out.println(numBlocks + "\t" + avgConvergence + "\t" + avgConvergenceSum);
			numBlocks++;
		}
		resetOneBlockDone();
	}

	public void allBlocksDone(String desc, String main, String argstr, int blockSize, int spliceDepth) {

		double avgFootprint = (double) footprintSum / numBlocks;
		System.out.println("footprint");
		System.out.format("\t avg: %.4f\n", avgFootprint);
		System.out.format("\t min: %d\n", footprintMin);
		System.out.format("\t max: %d\n", footprintMax);

		double avgTotalConvergence = 0;
		avgTotalConvergence = avgConvergenceSum / numBlocks;
		System.out.format("convergence: %.4f\n", avgTotalConvergence);

		long sumEffectiveBlockSizes = 0, sumEffectiveBlockSizes2 = 0;
		long numTraversals = 0, numTraversals2 = 0;
		for (int i = 0; i < blockSize + 1; i++) {
			//System.out.format("effectiveBlockSizes[%d]: %d\n", i + 1, effectiveBlockSizes[i]);
			sumEffectiveBlockSizes += effectiveBlockSizes[i] * (i);
			numTraversals += effectiveBlockSizes[i];
			sumEffectiveBlockSizes2 += effectiveBlockSizes2[i] * (i);
			numTraversals2 += effectiveBlockSizes2[i];
		}
		long sumWork = sumEffectiveBlockSizes + sumEffectiveBlockSizes2;
		double avgEffectiveBlockSize = (double) sumEffectiveBlockSizes / numTraversals;
		double avgEffectiveBlockSize2 = (double) sumEffectiveBlockSizes2 / numTraversals2;
		double avgEffectiveBlockSizeTotal = (double) (sumWork) / (numTraversals + numTraversals2);
		double avgNormalizedBlockSize = avgEffectiveBlockSizeTotal / blockSize;
		double workRatio1 = (double) sumEffectiveBlockSizes / sumWork;
		double workRatio2 = (double) sumEffectiveBlockSizes2 / sumWork;
		System.out.format("avgEffectiveBlockSizeTotal: %.4f\n", avgEffectiveBlockSizeTotal);
		System.out.format("avgNormalizedBlockSize: %.4f\n", avgNormalizedBlockSize);
		System.out.format("avgEffectiveBlockSize: %.4f\n", avgEffectiveBlockSize);
		System.out.format("avgEffectiveBlockSize2: %.4f\n", avgEffectiveBlockSize2);
		System.out.format("sumEffectiveBlockSizes: %d\n", sumEffectiveBlockSizes);
		System.out.format("sumEffectiveBlockSizes2: %d\n", sumEffectiveBlockSizes2);
		System.out.format("workRatio1: %.4f\n", workRatio1);
		System.out.format("workRatio2: %.4f\n", workRatio2);
		System.out.format("numTraversals: %d\n", numTraversals);
		System.out.format("numTraversals2: %d\n", numTraversals2);
		
		try {			
			PrintStream csv = new PrintStream(new FileOutputStream("profile.csv", true));
			csv.printf("%s,%s,%s,%d,%d, %.2f,%d,%d, %.4f, %.4f,%.4f,%.4f,%.4f, %d,%d,%.4f,%.4f,%d,%d\n",
					desc, main, argstr, blockSize, spliceDepth,
					avgFootprint, footprintMin, footprintMax,
					avgTotalConvergence,
					avgEffectiveBlockSizeTotal, avgNormalizedBlockSize, avgEffectiveBlockSize, avgEffectiveBlockSize2,
					sumEffectiveBlockSizes, sumEffectiveBlockSizes2, workRatio1, workRatio2, numTraversals, numTraversals2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reset();
	}

	void clearSampleWrappers() {
		for (int i = 0; i < setSize; i++) {
			wrappers[randIdxs[i]] = nullWrapper;
		}			
	}

	void setSampleWrappers() {
		nextSetIdx = 0;	
		for (int i = 0; i < setSize; i++) {
			int gen = rand.nextInt(blockSize);
			boolean ok = false;
			do {
				ok = true;
				for (int j = 0; j < i; j++) {
					if (randIdxs[j] == gen) {
						ok = false;
						gen++;
						if (gen == blockSize) {
							gen = 0;
						}
					}
				}
			} while (!ok);
			randIdxs[i] = gen;
		}
		for (int i = 0; i < setSize; i++) {
			//System.out.print(randIdxs[i] + ", ");
			wrappers[randIdxs[i]] = new Wrapper(sets[nextSetIdx++]);
		}	
		//System.out.println();
	}

	void resetOneBlockDone() {
		footprintSet.clear();
		map.clear();
		for (int i = 0; i < setSize; i++) {
			sets[i].clear();
		}	

		nextWrapperIdx = 0;
		if (useSampling) {
			clearSampleWrappers();
			setSampleWrappers();
		}
	}

	double calcConvergence(HashSet<Object> set1, HashSet<Object> set2) {
		int same = 0;
		for (Object o : set1) {
			if (set2.contains(o)) {
				same++;
			}
		}
		double avgSize = (double) (set1.size() + set2.size()) / 2;
		assert set1.size() != 0 && set2.size() != 0;
		return (double) same / avgSize;
	}
}
