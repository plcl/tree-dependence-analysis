/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;

public class FootprintSizeProfiler extends EffectiveBlockSizeProfiler {
	
	HashSet<Object> footprintSet;
	long footprintSum;
	int footprintMax;
	int footprintMin;
	int numBlocks;

	public FootprintSizeProfiler(int b) {
		super(b);
		reset();
	}
	
	protected void reset() {
		super.reset();
		footprintSet = new HashSet<Object>();
		footprintSum = 0;
		footprintMax = 0;
		footprintMin = Integer.MAX_VALUE;
		numBlocks = 0;
	}
	
	public void touch(Object key, Object o) {
		//super.touch(key, o); // does nothing
		footprintSet.add(o);
	}
	
	public void oneBlockDone() {
		//super.oneBlockDone(); // does nothing
		int footprint = footprintSet.size();
		footprintSum += footprint;
		if (footprint < footprintMin) {
			footprintMin = footprint;
		}
		if (footprint > footprintMax) {
			footprintMax = footprint;
		}
		//System.out.println(numBlocks + "\t" + avgConvergence + "\t" + avgConvergenceSum);
		numBlocks++;
		resetOneBlockDone();
	}
	
	public void allBlocksDone(String desc, String main, String argstr, int blockSize, int spliceDepth) {
		double avgFootprint = (double) footprintSum / numBlocks;
		System.out.println("footprint");
		System.out.format("\t avg: %.4f\n", avgFootprint);
		System.out.format("\t min: %d\n", footprintMin);
		System.out.format("\t max: %d\n", footprintMax);
		
		long sumEffectiveBlockSizes = 0, sumEffectiveBlockSizes2 = 0;
		long numTraversals = 0, numTraversals2 = 0;
		for (int i = 0; i < blockSize + 1; i++) {
			//System.out.format("effectiveBlockSizes[%d]: %d\n", i + 1, effectiveBlockSizes[i]);
			sumEffectiveBlockSizes += effectiveBlockSizes[i] * (i);
			numTraversals += effectiveBlockSizes[i];
			sumEffectiveBlockSizes2 += effectiveBlockSizes2[i] * (i);
			numTraversals2 += effectiveBlockSizes2[i];
		}
		long sumWork = sumEffectiveBlockSizes + sumEffectiveBlockSizes2;
		double avgEffectiveBlockSize = (double) sumEffectiveBlockSizes / numTraversals;
		double avgEffectiveBlockSize2 = (double) sumEffectiveBlockSizes2 / numTraversals2;
		double avgEffectiveBlockSizeTotal = (double) (sumWork) / (numTraversals + numTraversals2);
		double avgNormalizedBlockSize = avgEffectiveBlockSizeTotal / blockSize;
		double workRatio1 = (double) sumEffectiveBlockSizes / sumWork;
		double workRatio2 = (double) sumEffectiveBlockSizes2 / sumWork;
		System.out.format("avgEffectiveBlockSizeTotal: %.4f\n", avgEffectiveBlockSizeTotal);
		System.out.format("avgNormalizedBlockSize: %.4f\n", avgNormalizedBlockSize);
		System.out.format("avgEffectiveBlockSize: %.4f\n", avgEffectiveBlockSize);
		System.out.format("avgEffectiveBlockSize2: %.4f\n", avgEffectiveBlockSize2);
		System.out.format("sumEffectiveBlockSizes: %d\n", sumEffectiveBlockSizes);
		System.out.format("sumEffectiveBlockSizes2: %d\n", sumEffectiveBlockSizes2);
		System.out.format("workRatio1: %.4f\n", workRatio1);
		System.out.format("workRatio2: %.4f\n", workRatio2);
		System.out.format("numTraversals: %d\n", numTraversals);
		System.out.format("numTraversals2: %d\n", numTraversals2);
		
		try {			
			PrintStream csv = new PrintStream(new FileOutputStream("profile.csv", true));
			csv.printf("%s,%s,%s,%d,%d, %.2f,%d,%d, %.4f, %.4f,%.4f,%.4f,%.4f, %d,%d,%.4f,%.4f,%d,%d\n",
					desc, main, argstr, blockSize, spliceDepth,
					avgFootprint, footprintMin, footprintMax,
					0.0,
					avgEffectiveBlockSizeTotal, avgNormalizedBlockSize, avgEffectiveBlockSize, avgEffectiveBlockSize2,
					sumEffectiveBlockSizes, sumEffectiveBlockSizes2, workRatio1, workRatio2, numTraversals, numTraversals2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reset();
	}
	
	void resetOneBlockDone() {
		footprintSet.clear();
	}
}
