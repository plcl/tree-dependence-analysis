/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.

These benchmarks are adapted from the Lonestar benchmark suite:
http://iss.ices.utexas.edu/?p=projects/galois/lonestar
 */

package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.ThreadTimer.Tick;

public class Launcher {

	private static Tick start;
	private static Tick stop;
	private static int blockSize, spliceDepth;
	private static float spliceConvergence;
	private static BlockProfiler profiler;
	private static int papiCounters;
	
	public static int customParam1, customParam2, customParam3;

	public static BlockProfiler getProfiler() {
		return profiler;
	}

	public static Object invokeStaticMethod(String className, String methodName, Object[] params) {
		Class<?> c = null;
		try {
			c = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		final Method methods[] = c.getDeclaredMethods();
		for (Method method : methods) {
			if (methodName.equals(method.getName())) {
				try {
					method.setAccessible(true);
					return method.invoke(null, params);
				} catch (IllegalAccessException ex) {
					throw new RuntimeException(ex);
				} catch (InvocationTargetException ite) {
					throw new RuntimeException(ite);
				}
			}
		}
		throw new RuntimeException("Method '" + methodName + "' not found");
	}

	/**
	 * Marks beginning of timed section. If there are multiple calls to this
	 * method within the same run, honor the latest call. This method also
	 * performs a full garbage collection.
	 */
	public static void startTiming() {
		System.gc();
		if (stop == null)
			start = ThreadTimer.tick();

		if (papiCounters > 0) {
			int events = Papi.startSystem(papiCounters);
			System.out.printf("Papi events started: %d\n", events);
			Papi.startThread(0);
		}
	}

	/**
	 * Marks end of timed section. If there are multiple calls to this method
	 * within the same run, honor the earliest call.
	 */
	public static void stopTiming() {
		if (papiCounters > 0) {
			Papi.readThread(0);
			Papi.finishThread(0);
			Papi.finishSystem();
		}

		if (stop == null)
			stop = ThreadTimer.tick();
	}

	/**
	 * Returns the wall-time of the most recently completed run with or without
	 * garbage collection time included
	 * 
	 * @param withoutGc true if garbage collection time should be excluded from result
	 * @return   wall-time in milliseconds of the run
	 */
	public static long elapsedTime(boolean withoutGc) {
		return start.elapsedTime(withoutGc, stop);
	}

	public static void recordBlockSize(int size) {
		blockSize = size;
	}

	public static int getBlockSize() {
		return blockSize;
	}

	public static void recordSpliceInfo(int depth, float conv) {
		spliceDepth = depth;
		spliceConvergence = conv;
	}

	public static int getSpliceDepth() {
		return spliceDepth;
	}

	public static void usage() {
		System.err.println("java -cp util.Launcher [options] <app.Main> [args]");
		System.err.println(" --runs               : number of runs");
		System.err.println(" --dropruns           : number of runs to drop in stats (to account for JIT compilation)");
		System.err.println(" --blocksize <size>   : block size used (need --utilstats or --utilprofiler in TreeSplicer");
		System.err.println(" --splicedepth <depth>: splice depth used (need --utilstats or --utilprofiler in TreeSplicer");
		System.err.println(" --profiler [0-3]     : profiler used [default: 0]");
		System.err.println(" --help               : print help");
		System.exit(1);
	}

	public static void main(String[] args) throws Exception {
		String main = null;
		String[] mainArgs = new String[0];
		int numRuns = 8;
		int numDroppedRuns = 1;
		int numRepeatRuns = 20;
		int profilerLevel = 0;
		String desc = "";
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.equals("--runs")) {
				numRuns = Integer.parseInt(args[++i]);
			} else if (arg.equals("--dropruns")) {
				numDroppedRuns = Integer.parseInt(args[++i]);
			} else if (arg.equals("--repeatruns")) {
				numRepeatRuns = Integer.parseInt(args[++i]);
			} else if (arg.equals("--desc")) {
				desc = args[++i];
			} else if (arg.equals("--blocksize")) {
				blockSize = Integer.parseInt(args[++i]);
			} else if (arg.equals("--splicedepth")) {
				spliceDepth = Integer.parseInt(args[++i]);
			} else if (arg.equals("--profiler")) {
				profilerLevel = Integer.parseInt(args[++i]);
			} else if (arg.equals("--papi")) {
				papiCounters = Integer.parseInt(args[++i]);
			} else if (arg.equals("--custom1")) {
				customParam1 = Integer.parseInt(args[++i]);
			} else if (arg.equals("--custom2")) {
				customParam2 = Integer.parseInt(args[++i]);
			} else if (arg.equals("--custom3")) {
				customParam3 = Integer.parseInt(args[++i]);
			} else if (arg.equals("--help")) {
				usage();
			} else {
				main = arg;
				mainArgs = Arrays.asList(args).subList(i + 1, args.length).toArray(new String[args.length - i - 1]);
				break;
			}
		}

		if (main == null) {
			usage();
		}
		
		if (profilerLevel == 1) {
			profiler = new EffectiveBlockSizeProfiler(blockSize);
		} else if (profilerLevel == 2) {
			profiler = new FootprintSizeProfiler(blockSize);
		} else if (profilerLevel == 3) {
			profiler = new ConvergenceProfiler(blockSize);
		}
		
		if (papiCounters > 0) {
			Papi.init();
		}

		List<Long> times = new ArrayList<Long>();
		List<Integer> blockSizes = new ArrayList<Integer>();
		List<Integer> spliceDepths = new ArrayList<Integer>();
		List<Float> convergences = new ArrayList<Float>();

		String argstr = "";
		for (int i = 0; i < mainArgs.length; i++) {
			argstr += mainArgs[i] + " ";
		} 
		System.out.printf("[runs:%d][desc:%s][app:%s][args:%s]\n\n", numRuns, desc, main, argstr);

		for (int i = 0; i < numRuns; i++) {
			//startTiming();	// should be called only once in app code for correct PAPI
			invokeStaticMethod(main, "main", new Object[] { mainArgs });
			//stopTiming();
			long time = Launcher.elapsedTime(true);
			stop = null;
			System.out.printf("run %d of %d, runtime(ms): %d\n\n", i, numRuns, time);
			if (profiler != null) {
				profiler.allBlocksDone(desc, main, argstr, blockSize, spliceDepth);
			}
			times.add(time);
			blockSizes.add(blockSize);
			spliceDepths.add(spliceDepth);
			convergences.add(spliceConvergence);

			if (i == numRuns - 1) {
				if (!resultsStable(numDroppedRuns, numRuns, times) && numRuns < numRepeatRuns) {
					numRuns++;
					numDroppedRuns++;
				}
			}
		}

		summarizeStats(numDroppedRuns, numRuns, desc, main, argstr, times, blockSizes, spliceDepths, convergences);

		if (papiCounters > 0) {
			Papi.papiDone(papiCounters, numDroppedRuns, numRuns, desc, main, argstr);
		}
	}

	private static boolean resultsStable(int drop, int total, List<Long> times) {
		float[] stats = summarizeTimes(drop, total, times);	
		if (stats[4] > 10.0f) {
			return false;
		} else {
			return true;
		}
	}

	private static float [] summarizeTimes(int drop, int total, List<Long> times) {
		float [] stats = new float[5];
		int count = total - drop;
		long min = Long.MAX_VALUE;
		long max = 0;
		long timeSum = 0;
		for (int i = drop; i < total; i++) {
			long time = times.get(i);
			timeSum += time;
			if (time < min) min = time;
			if (time > max) max = time;
		}
		float timeAvg = (float) timeSum / count;

		float varSum = 0;
		for (int i = drop; i < total; i++) {
			long time = times.get(i);
			varSum += (time - timeAvg) * (time - timeAvg);
		}
		float stdev = (float) Math.sqrt(varSum / count);
		float stability = stdev * 100.0f / timeAvg;
		stats[0] = timeAvg;
		stats[1] = min;
		stats[2] = max;
		stats[3] = stdev;
		stats[4] = stability;
		return stats;
	}

	private static void summarizeStats(int drop, int total, String desc, String main, String argstr, 
			List<Long> times, List<Integer> blockSizes, List<Integer> spliceDepths, List<Float> convergences) throws FileNotFoundException {
		int count = total - drop;
		int blockSizeSum = 0;
		int spliceDepthSum = 0;
		float convergenceSum = 0.0f;
		for (int i = drop; i < total; i++) {
			blockSizeSum += blockSizes.get(i);
			spliceDepthSum += spliceDepths.get(i);
			convergenceSum += convergences.get(i);
		}
		float blockSizeAvg = (float) blockSizeSum / count;
		float spliceDepthAvg = (float) spliceDepthSum / count;
		float spliceConvergenceAvg = convergenceSum / count;
		float [] stats = summarizeTimes(drop, total, times);

		System.out.printf("runtime(ms): avg:%.2f min:%.0f max:%.0f stdev:%.2f stability:%.1f\n\n\n", stats[0], stats[1], stats[2], stats[3], stats[4]);
		boolean printHeader = false;
		File file = new File("stats.csv");
		if (!file.exists()) {
			printHeader = true;
		}
		PrintStream csv = new PrintStream(new FileOutputStream("stats.csv", true));
		if (printHeader) {
			csv.printf("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s\n", 
					"desc", "benchmark", "args", "avg", "min", "max", "stdev", "stability", "blockSize", "spliceDepth", "convergence");
		}
		csv.printf("%s, %s, %s, %.2f, %.0f, %.0f, %.2f, %.1f, %.2f, %.2f, %.4f\n", desc, main, argstr, stats[0], stats[1], stats[2], stats[3], stats[4],
				blockSizeAvg, spliceDepthAvg, spliceConvergenceAvg);
		csv.close();

		PrintStream fullcsv = new PrintStream(new FileOutputStream("fullstats.csv", true));
		for (int i = 0; i < total; i++) {
			fullcsv.printf("%s, %s, %s, %d, %d, %d, %d, %.4f\n", desc, main, argstr,
					i, times.get(i), blockSizes.get(i), spliceDepths.get(i), convergences.get(i));
		}
		fullcsv.println();
		fullcsv.close();
	}
}
