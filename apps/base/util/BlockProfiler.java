/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)
https://sites.google.com/site/treesplicer/

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package util;

public interface BlockProfiler {
	
	public void touch(Object key, Object o);
	
	public void effectiveBlock(int size);
	
	public void effectiveBlock(int size, int phase);
	
	public void oneBlockDone();
		
	public void allBlocksDone(String desc, String main, String argstr, int blockSize, int spliceDepth);	
}
