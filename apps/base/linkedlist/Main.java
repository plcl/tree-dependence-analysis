package linkedlist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;

import util.Launcher;

public final class Main {
    private static void callInsert(Node root, int i) {
        traversal_transform(root, i);
    }
    
	private static void traversal_transform(Node root, int b) {
	    if (root.next == null) {
	    	root.next = new Node();
            root.next.data = b;
	    }
        else {
	        traversal_transform(root.next, b);
        }
	}
	
	public static void main(String[] args) throws ExecutionException {
	    final Node root = new Node(0);
	    Global.counter = 0;
        
        Launcher.startTiming();
        ParallelFor(root, step);
        Launcher.stopTiming();
	    
	    Node traverse = root;
	    System.err.println(traverse.getData());
	    while (traverse.getNext() != null) {
	    	traverse = traverse.getNext();
	    	System.err.println(traverse.getData());
	    }
	    
	    System.err.println("Final counter is: " + Global.counter);
	}
    
    // a substitute for parallel for annotations on for statements, which are not supported in Java 6
	public @interface ParallelFor {}
	@ParallelFor
	public static void ParallelFor(Node root) {
		for(int i = 1; i <= 100; i++) {
	        callInsert(root, i);
	    }
	}
}