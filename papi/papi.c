#include <sys/types.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <papi.h>
#include <jni.h>

#define MAX_EVENTS 16
#define MAX_THREADS 256

static int Events[MAX_EVENTS];
static int EventSets[MAX_THREADS];
static int NumEvents = 0;
static long_long StartingCycle;
static long_long StartingUsec;

static void
throw_error(JNIEnv *env, char *file, int line, char *fmt, ...)
{
	va_list ap;
  char buf[128];
  int written;

  written = snprintf(buf, 128, "%s:%d ", file, line);

  va_start(ap, fmt);
  written += vsnprintf(buf + written, 128 - written, fmt, ap);
  va_end(ap);

  fprintf(stderr, "3: %s\n", buf);

  if (written == 128)
    buf[127] = '\0';

  if (env) {
    jclass Exception = (*env)->FindClass(env, "java/lang/Error");
	  (*env)->ThrowNew(env, Exception, buf);
  } else {
    fprintf(stderr, "%s", buf);
    fprintf(stderr, "\n");
    exit(1);
  }
}


static int min(int a, int b) {
  if (a < b)
    return a;
  else
    return b;
}


static int create_eventset(JNIEnv *env, int *eventset) {
  int i;

  *eventset = PAPI_NULL;
  if (PAPI_create_eventset(eventset) != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot create event set");
    return 1;
  }

  for (i = 0; i < NumEvents; i++) {
    if (PAPI_add_event(*eventset, Events[i]) != PAPI_OK) {
      char name[PAPI_MAX_STR_LEN];
      if (PAPI_event_code_to_name(Events[i], name) != PAPI_OK) {
        throw_error(env, __FILE__, __LINE__, "Cannot add hardware event: UNKNOWN");
        return 1;
      } else {
        throw_error(env, __FILE__, __LINE__, "Cannot add hardware event: %s", name);
        return 1;
      }
    }
  }

  return 0;
}

static int get_event_id(JNIEnv *env, jstring jstr, int* event_id) {
  const jbyte *str = (*env)->GetStringUTFChars(env, jstr, NULL);
  if (str == NULL) {
    /* OutOfMemoryError already thrown */
    return 1;
  }
  int retval = PAPI_event_name_to_code((char*) str, event_id);
  (*env)->ReleaseStringUTFChars(env, jstr, str);
  if (retval != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot convert %s to event id", str);
    return 1;
  }
  return 0;
}

/*
 * Class:     util_Papi
 * Method:    _startSystem
 * Signature: ([Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL 
Java_util_Papi__1startSystem(JNIEnv *env, jclass clz, jobjectArray events) {

  if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT) {
    throw_error(env, __FILE__, __LINE__, "Cannot init papi");
    return 0;
  }

  if (PAPI_thread_init(pthread_self) != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot not init thread papi");
    return 0;
  }

  NumEvents = min(MAX_EVENTS, min((*env)->GetArrayLength(env, events), PAPI_num_counters()));

  int i;
  for (i = 0; i < NumEvents; i++) {
    int event_id;

    jstring event = (*env)->GetObjectArrayElement(env, events, i);
    int retval = get_event_id(env, event, &event_id);
    (*env)->DeleteLocalRef(env, event);

    if (retval) {
      return 0;
    }
    
    Events[i] = event_id;
  }

  /* Establish starting point */
  StartingUsec = PAPI_get_real_usec();
  StartingCycle = PAPI_get_real_cyc();

  return NumEvents;
}

/*
 * Class:     util_Papi
 * Method:    _finishSystem
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_util_Papi__1finishSystem(JNIEnv *env, jclass clz) {
  StartingCycle = PAPI_get_real_cyc() - StartingCycle;
  StartingUsec = PAPI_get_real_usec() - StartingUsec;
  PAPI_shutdown();
}

/*
 * Class:     util_Papi
 * Method:    _readThread
 * Signature: (I[J)V
 */
JNIEXPORT void JNICALL
Java_util_Papi__1readThread(JNIEnv *env, jclass clz, jint id, jlongArray array) {
  long_long values[MAX_EVENTS];

  if (PAPI_read(EventSets[id], values) != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Could not read values");
  }

  if (PAPI_reset(EventSets[id]) != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Could not reset values");
  }
  
  if (sizeof(jlong) != sizeof(long_long)) {
    jlong jvalues[MAX_EVENTS];
    int i;
    for (i = 0; i < NumEvents; i++)
      jvalues[i] = values[i];

    (*env)->SetLongArrayRegion(env, array, 0, NumEvents, jvalues);
  } else {
    (*env)->SetLongArrayRegion(env, array, 0, NumEvents, (jlong*) values);
  }
}

/*
 * Class:     util_Papi
 * Method:    _startThread
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_util_Papi__1startThread(JNIEnv *env, jclass clz, jint id) {
  if (id >= MAX_THREADS) {
    throw_error(env, __FILE__, __LINE__, "Thread id greater than max thread id : %d", id);
  }

  EventSets[id] = PAPI_NULL;

  if (PAPI_register_thread() != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot register thread");
    return;
  }

  int eventset;
  if (create_eventset(env, &eventset)) {
    throw_error(env, __FILE__, __LINE__, "Cannot create eventset");
    return;
  }

  EventSets[id] = eventset;

  int code;
  if ((code = PAPI_start(eventset)) != PAPI_OK) {
    if (code == PAPI_ESYS) {
      throw_error(env, __FILE__, __LINE__, "Cannot start papi monitoring: %s\n", strerror(errno));
      return;
    } else {
      throw_error(env, __FILE__, __LINE__, "Cannot start papi monitoring");
      return;
    }
  }
}

/*
 * Class:     util_Papi
 * Method:    _finishThread
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_util_Papi__1finishThread(JNIEnv *env, jclass clz, jint id) {
  if (id >= MAX_THREADS) {
    throw_error(env, __FILE__, __LINE__, "Thread id greater than max thread id : %d", id);
  }

  long_long values[MAX_EVENTS];
  int eventset = EventSets[id];

  if (eventset == PAPI_NULL) {
    return;
  }

  if (PAPI_stop(eventset, values) != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot stop papi monitoring");
    return;
  }

  if (PAPI_unregister_thread() != PAPI_OK) {
    throw_error(env, __FILE__, __LINE__, "Cannot unregister thread");
    return;
  }
}
