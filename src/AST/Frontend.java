
package AST;
import java.util.HashSet;import java.util.LinkedHashSet;import java.io.File;import java.util.*;import beaver.*;import java.util.ArrayList;import java.util.zip.*;import java.io.*;import java.io.FileNotFoundException;import java.util.Collection;import transform.*;

public class Frontend extends java.lang.Object {
    // Declared in FrontendMain.jrag at line 12

    protected Program program;

    // Declared in FrontendMain.jrag at line 14


    protected Frontend() {
      ASTNode.reset(); // reset global state and possible debug info
      program = new Program();
    }

    // Declared in FrontendMain.jrag at line 19

    
    protected void processPre(CompilationUnit unit) {
    	throw new Error("Launcher should override this");
    }

    // Declared in FrontendMain.jrag at line 23

    
    protected void processMain(CompilationUnit unit) {
    	throw new Error("Launcher should override this");
    }

    // Declared in FrontendMain.jrag at line 27

    
    protected void process() {
    	throw new Error("Launcher should override this");
    }

    // Declared in FrontendMain.jrag at line 31

    
    protected void processPost(CompilationUnit unit) {
    	throw new Error("Launcher should override this");
    }

    // Declared in FrontendMain.jrag at line 35
    

    public boolean process(String[] args, BytecodeReader reader, JavaParser parser) {
      program.initBytecodeReader(reader);
      program.initJavaParser(parser);

      initOptions();
      processArgs(args);

      Collection files = program.files();

      if(program.hasOption("-version")) {
        printVersion();
        return false;
      }
      if(program.hasOption("-help") || files.isEmpty()) {
        printUsage();
        return false;
      }

      try {
        for(Iterator iter = files.iterator(); iter.hasNext(); ) {
          String name = (String)iter.next();
          if(!new File(name).exists())
            System.out.println("WARNING: file \"" + name + "\" does not exist");
          program.addSourceFile(name);
        }

        for(Iterator iter = program.compilationUnitIterator(); iter.hasNext(); ) {
          CompilationUnit unit = (CompilationUnit)iter.next();
          if(unit.fromSource()) {
        	  processPre(unit);
          }
        }
        for(Iterator iter = program.compilationUnitIterator(); iter.hasNext(); ) {
            CompilationUnit unit = (CompilationUnit)iter.next();
            if(unit.fromSource()) {
          	  processMain(unit);
            }
          }
        process();
        for(Iterator iter = program.compilationUnitIterator(); iter.hasNext(); ) {
            CompilationUnit unit = (CompilationUnit)iter.next();
            if(unit.fromSource()) {
          	  processPost(unit);
            }
          }        
      } catch (Exception e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
      return true;
    }

    // Declared in FrontendMain.jrag at line 87


    protected void initOptions() {
      program.initOptions();
      program.addKeyOption("-version");
      program.addKeyOption("-print");
      program.addKeyOption("-g");
      program.addKeyOption("-g:none");
      program.addKeyOption("-g:lines,vars,source");
      program.addKeyOption("-nowarn");
      program.addKeyOption("-verbose");
      program.addKeyOption("-deprecation");
      program.addKeyValueOption("-classpath");
      program.addKeyValueOption("-sourcepath");
      program.addKeyValueOption("-bootclasspath");
      program.addKeyValueOption("-extdirs");
      program.addKeyValueOption("-d");
      program.addKeyValueOption("-encoding");
      program.addKeyValueOption("-source");
      program.addKeyValueOption("-target");
      program.addKeyOption("-help");
      program.addKeyOption("-O");
      program.addKeyOption("-J-Xmx128M");
      program.addKeyOption("-recover");
    }

    // Declared in FrontendMain.jrag at line 110

    protected void processArgs(String[] args) {
      program.addOptions(args);
    }

    // Declared in FrontendMain.jrag at line 114


    protected void processErrors(Collection errors, CompilationUnit unit) {
      System.out.println("Errors:");
      for(Iterator iter2 = errors.iterator(); iter2.hasNext(); ) {
        System.out.println(iter2.next());
      }
    }

    // Declared in FrontendMain.jrag at line 120

    protected void processWarnings(Collection warnings, CompilationUnit unit) {
      for(Iterator iter2 = warnings.iterator(); iter2.hasNext(); ) {
        System.out.println(iter2.next());
      }
    }

    // Declared in FrontendMain.jrag at line 125

    protected void processNoErrors(CompilationUnit unit) {
    	//System.out.println(unit);
    }

    // Declared in FrontendMain.jrag at line 129


    protected void printUsage() {
      printVersion();
      System.out.println(
          "\n" + name() + "\n\n" +
          "Usage: java " + name() + " <options> <source files>\n" +
          "  -verbose                  Output messages about what the compiler is doing\n" +
          "  -classpath <path>         Specify where to find user class files\n" +
          "  -sourcepath <path>        Specify where to find input source files\n" + 
          "  -bootclasspath <path>     Override location of bootstrap class files\n" + 
          "  -extdirs <dirs>           Override location of installed extensions\n" +
          "  -d <directory>            Specify where to place generated class files\n" +
          "  -help                     Print a synopsis of standard options\n" +
          "  -version                  Print version information\n"
          );
    }

    // Declared in FrontendMain.jrag at line 145


    protected void printVersion() {
      System.out.println(name() + " " + url() + " Version " + version());
    }

    // Declared in FrontendMain.jrag at line 149


    protected String name() {
      return "Java1.4Frontend";
    }

    // Declared in FrontendMain.jrag at line 152

    protected String url() {
      return "(http://jastadd.cs.lth.se)";
    }

    // Declared in FrontendMain.jrag at line 156


    protected String version() {
      return "R20070504";
    }


}
