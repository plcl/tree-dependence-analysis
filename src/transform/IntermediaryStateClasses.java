/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import AST.VariableDeclaration;

public class IntermediaryStateClasses {
	private static class InterStateField {
		VariableDeclaration vdec;
		String methodName;

		InterStateField(VariableDeclaration vdec, String methodName) {
			this.vdec = vdec;
			this.methodName = methodName;
		}
	}

	private static ArrayList<InterStateField> fields = new ArrayList<InterStateField>();

	private static String getFieldName(InterStateField field) {
		return field.methodName + "_" + field.vdec.getID();
	}

	private static String getSaveInterStateMethodName(InterStateField field) {
		return "save_" + getFieldName(field);
	}

	private static String getLoadInterStateMethodName(InterStateField field) {
		return "load_" + getFieldName(field);
	}

	public static String getLoadInterStateMethodName(VariableDeclaration vdec, String methodName) {
		return "load_" + methodName + "_" + vdec.getID();
	}

	public static String saveInterState(VariableDeclaration vdec, String methodName) {
		InterStateField field = new InterStateField(vdec, methodName); 
		boolean isDuplicate = false;
		for (InterStateField field2 : fields) {
			if (field.vdec.toString().equals(field2.vdec.toString()) &&
					field.methodName.equals(field2.methodName)) {
				isDuplicate = true;
				break;
			}
		}
		if (!isDuplicate) fields.add(field);
		return getSaveInterStateMethodName(field);
	}

	public static void generate() {
		try {
			generateIntermediaryStateClass();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void generateIntermediaryStateClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_IntermediaryState + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		out.print("public class _IntermediaryState {\n");
		for (InterStateField field : fields) {
			out.format("	private final %s [] %s;\n", field.vdec.getTypeAccess(), getFieldName(field));
		}
		out.format("	private int index;\n");
		if (Globals.useBlock) {
			out.format("	public _Block block;\n");
			if (!Globals.useSplice) {
			out.format("	public _IntermediaryState() {\n" +
					"		this(_Block.maxBlockSize);\n" +
			"	}\n");
			}
		}
		if (Globals.useSplice) {
			out.format("	public _SpliceSet spliceSet;\n");
			out.format("	public _IntermediaryState() {\n" +
					"		this(_SpliceSet.getWorkSize());\n" +
			"	}\n");
		}
		out.print("	public void incIndex() {\n" +
				"		index++;\n" +
				"	}\n" +
				"	public void resetIndex() {\n" +
				"		index = 0;\n" +
				"	}\n" +
				"	public int getIndex() {\n" +
				"		return index;\n" +
				"	}\n" +
		"	\n");
			out.print("	static ThreadLocal<_IntermediaryState> instance0 = new ThreadLocal<_IntermediaryState>();\n" +
					"	public static _IntermediaryState getInstance0() {\n" +
					"		_IntermediaryState ret = instance0.get();\n" +
					"		if (ret == null) {\n" +
					"			ret = new _IntermediaryState();\n" +
					"			instance0.set(ret);\n" +
					"		}\n" +
					"		return ret;\n" +
					"	}\n" +
					"	public static void resetInstance() {\n" +
					"		instance0 = new ThreadLocal<_IntermediaryState>();\n" +
			"	}\n");
		
		out.format("	public _IntermediaryState(int initSize) {\n");
		for (InterStateField field : fields) {
			out.format("		%s = new %s[initSize];\n", getFieldName(field), field.vdec.getTypeAccess());
		}
		out.format("	}\n");
		for (InterStateField field : fields) {
			out.format("	public void %s(%s item) {\n", getSaveInterStateMethodName(field), field.vdec.getTypeAccess());
			out.format("		%s[index] = item;\n", getFieldName(field));
			out.format("	}\n");
			out.format("	public %s %s() {\n", field.vdec.getTypeAccess(), getLoadInterStateMethodName(field));
			out.format("		return %s[index];\n", getFieldName(field));
			out.format("	}\n");
		}
		out.format("}\n");
	}
}
