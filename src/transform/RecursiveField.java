/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashSet;

import AST.Access;
import AST.ArrayCreationExpr;
import AST.ArrayInit;
import AST.ArrayTypeAccess;
import AST.ArrayTypeWithSizeAccess;
import AST.ClassDecl;
import AST.Expr;
import AST.FieldDeclaration;
import AST.IntegerLiteral;
import AST.MethodAccess;
import AST.Stmt;

public class RecursiveField {
	public FieldDeclaration fdec;
	public Stmt recursiveCallStmt = null;
	public Expr argExpr = null;
	public int id;
	public int size;
	
	public static int totalSize = 0;
	public static int nextId = 0;
	public static ClassDecl recursiveClass = null;
	
	private static HashSet<String> fieldNames = new HashSet<String>();
	private static ArrayList<RecursiveField> recursiveFields = new ArrayList<RecursiveField>();

	public static void addRecursiveField(FieldDeclaration f, ClassDecl c) {
		if (!fieldNames.add(f.getID())) return;
		recursiveFields.add(new RecursiveField(f));
		if (recursiveClass == null) {
			recursiveClass = c;
		} else {
			assert recursiveClass == c;
		}
	}
	
	public static ArrayList<RecursiveField> getFields() {
		return recursiveFields;
	}
	
	public static RecursiveField findField(String name) {
		for (RecursiveField rf : recursiveFields) {
			if (rf.fdec.getID().equals(name)) {
				return rf;
			}
		}
		return null;
	}
	
	public static void clearFields() {
		recursiveFields.clear();
		recursiveClass = null;
	}
	
	public static void finalizeFields() {
		totalSize = 0;
		for (RecursiveField rf : recursiveFields) {
			totalSize += rf.size;
		}
	}
	
	public RecursiveField(FieldDeclaration f) {
		fdec = f;
		Access acc = fdec.getTypeAccess();
		if (acc instanceof ArrayTypeAccess) {
			ArrayCreationExpr initExpr = (ArrayCreationExpr) fdec.getInit();
			ArrayTypeWithSizeAccess acc2 = (ArrayTypeWithSizeAccess) initExpr.getTypeAccess();
			IntegerLiteral sizeLit = (IntegerLiteral) acc2.getExpr();
			size = Integer.parseInt(sizeLit.toString());
		} else {
			size = 1;
		}
	}
	
	public void setID() {
		id = nextId;
		nextId += size;
	}
}
