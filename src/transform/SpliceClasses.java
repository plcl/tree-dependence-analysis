/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import AST.ParameterDeclaration;
import AST.PrimitiveTypeAccess;

public class SpliceClasses extends ClassHelpers {

	private static boolean useSplicePoint;
	private static int maxPhases;
	private static String nodeClass = RecursiveField.recursiveClass.getID();

	// used when blockClassPointSets.size() == 1
	// also used to generate autotune code
	private static String pointClass;

	private static boolean needTreeClass = false;

	public static void generateSpliceClasses() {
		needTreeClass = !Globals.isImplicitRecursiveStructure && !Globals.isStaticRecursiveMethod;

		for (RecursiveCallSet rcs : RecursiveCallSet.getSpliceSets()) {
			maxPhases = maxPhases > rcs.maccs.size() ? 	maxPhases : rcs.maccs.size();
		}

		if (MethodInfo.getRecursiveMethodLoopVariantParams().size() > 1 || Globals.usePointIndex) {
			useSplicePoint = true;	
		} else {
			useSplicePoint = false;
			pointClass = MethodInfo.getRecursiveMethodLoopVariantParams().get(0).getTypeAccess().toString();
		}

		try {
			generateSpliceNodeClass();
			generateSpliceSetClass();
			if (useSplicePoint) generateSplicePointClass();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	private static void generateSplicePointClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_SplicePoint + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		out.print("public class _SplicePoint {\n");
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodLoopVariantParams()) {
			out.format("	public %s;\n", pdec);
		}
		ArrayList<String> params = new ArrayList<String>();
		params.addAll(getLoopVariantParams());
		if (Globals.usePointIndex) {
			params.add("int pointIndex");
			out.format("	public int _pointIndex;\n");
		} 
		out.printf("	public _SplicePoint(%s) {\n", concat(params));

		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodLoopVariantParams()) {
			out.format("		this.%s = %s;\n", pdec.getID(), pdec.getID());
		}
		if (Globals.usePointIndex) {
			out.format("		_pointIndex = pointIndex;\n");
		}
		out.print("	}\n");
		out.print("}\n");
	}

	private static void generateSpliceNodeClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_SpliceNode + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		out.print("import java.util.ArrayList;\n" +
		"public class _SpliceNode {\n");

		if (useSplicePoint) {
			out.format("	public ArrayList<_SplicePoint> points = new ArrayList<_SplicePoint>();\n");
		} else {
			out.format("	public ArrayList<%s> points = new ArrayList<%s>();\n", pointClass, pointClass);	
		}

		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopInvariantParams()) {
			out.print("	public " + pdec + ";\n");
		}

		out.format("	public _SpliceNode(%s){\n", concat(getAllLoopInvariantParams()));
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopInvariantParams()) {
			out.format("		this.%s = %s;\n", pdec.getID(), pdec.getID());
		}
		out.print(
				"	}\n" +
		"\n");
		ArrayList<String> args = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		params.addAll(getLoopVariantParams());
		args.addAll(getLoopVariantArgs());
		if (Globals.usePointIndex) {
			params.add("int _pointIndex");
			args.add("_pointIndex");
		}
		out.printf("	public void add(%s) {\n", concat(params));
		if (useSplicePoint) {
			out.printf("		points.add(new _SplicePoint(%s));\n", concat(args));
		} else {
			out.printf("		points.add(%s);\n", concat(args));
		}
		out.print("	}\n" +
		"\n");

		if (useSplicePoint) {
			out.print("	public void add(_SplicePoint p) {\n" +
					"		points.add(p);\n" +
			"	}\n");
		}
		out.print("	public int size() {\n" +
				"		return points.size();\n" +
				"	}	  \n" +
				"	\n" +
				"	public void clear() {\n" +
				"		points.clear();\n" +
				"	}\n" +
		"}\n");
	}

	private static void generateSpliceSetClass() throws FileNotFoundException {

		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_SpliceSet + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		if (Globals.useUtilStats) {
			out.print("import util.Launcher;\n");
		}
		out.print(
				"import java.util.HashSet;" +
				"public class _SpliceSet {\n" +
				"	\n" +
				"	private _SpliceNode [] spliceNodesSrc;\n" +
				"	private _SpliceNode [] spliceNodesDest;\n" +
				"	private " + nodeClass + " [] treeMap;\n");
		if (needTreeClass) out.print("	private " + MethodInfo.getRecursiveCdec().getID() + " treeClass;\n");
		out.print("	\n" +
				"	private static int depthParam;\n" +
				"	private static int depthParamHalf;\n" +
				"	private static int numLeafNodes;\n" +
				"	private static int numNodes;\n" +
				"	private static int workSize;\n" +
		"		\n");
		out.format("	private final static int maxChildren = %d;\n", RecursiveField.totalSize);
		out.format("	private final static int maxPhases = %d;\n", maxPhases);
		out.print("	\n");

		out.print(
				"	\n" +
				"	public static boolean useSplice() {\n" +
				"		return depthParam > 0;\n" +
				"	}\n" +
				"	\n" +
				"	public static int getWorkSize() {\n" +
				"		return workSize;\n" +
				"	}\n" +
				"	\n" +
				"	public static int getDepthParam() {\n" +
				"		return depthParam;\n" +
				"	}\n" +
				"	\n" +
				"	private static int exp(int n) {\n" +
				"		int ret = 1;\n" +
				"		while (n-- > 0) {\n" +
				"			ret *= maxChildren;\n" +
				"		}\n" +
				"		return ret;\n" +
				"	}\n" +
		"	\n");

		if (Globals.useAutotune) {
			out.print("	public static void init(int numPoints, float avgConvergence, float avgReach) {\n");
			out.print("	    System.out.format(\"avg convergence: %.4f\\n\", avgConvergence);\n");
			if (Globals.skipSpliceIfSorted) {

				out.print("	    if (avgConvergence > 0.7) {\n");
				out.print("	    	System.out.println(\"points are sorted - not using splicing\\n\");\n");
				if (Globals.useUtilStats) {
					out.print("		    Launcher.recordSpliceInfo(depthParam, avgConvergence);\n");
				}
				out.print("	    	return;\n");
				out.print("	    }\n");
			}
			out.print("	    depthParam =  (int) avgReach / 2;\n");
			if (Globals.useUtilStats) {
				out.print("	    Launcher.recordSpliceInfo(depthParam, avgConvergence);\n");
			}
		} else {
			out.print("	public static void init(int numPoints) {\n");
			if (Globals.useUtilStats) {
				out.format("	    depthParam = Launcher.getSpliceDepth();\n");
			} else {
				out.format("	    depthParam = %d;\n", Globals.spliceDepthParam);
			}
		}

		out.print(
				"	    depthParamHalf = depthParam >> 1;\n" +
				"	    workSize = numPoints; // this could be tweaked to handle part of points at a time\n" +
				"		numLeafNodes = exp(depthParam + 1);\n" +
		"		numNodes = numLeafNodes * maxChildren;\n");
		if (Globals.useAutotune) out.print("	    System.out.format(\"using splicing at depth: %d reach: %.2f nodes: %d\\n\", depthParam, avgReach, numNodes);\n");
		else out.print("	    System.out.format(\"using splicing at depth: %d nodes: %d\\n\", depthParam, numNodes);\n");
		if (Globals.hasSplicePersistents) {
			out.print("		initPersistents(numPoints);\n");
		}
		out.print("		\n");

		if (Globals.optimizationLevel == Globals.OptimizationLevel.GENERAL_ORDER) {
			out.print("		spliceStateBitmap = new int[numPoints][];\n" +
					"		for (int i = 0; i < numPoints; i++) {\n" +
					"			spliceStateBitmap[i] = new int[depthParam + 2];\n" +
			"		}\n");
		}
		if (MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size() > 0) {
			for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
				Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
				if (callSetNum == null) {
					out.format("		spliceLocal_%s = new %s[numPoints][];\n", pdec.getID(), pdec.getTypeAccess());
					out.format(					"		for (int i = 0; i < numPoints; i++) {\n" +
							"			spliceLocal_%s[i] = new %s[depthParam + 2];\n" +
							"		}\n", pdec.getID(), pdec.getTypeAccess());					
				} else {
					for (int j = 0; j < callSetNum; j++) {
						out.format("		spliceLocal_%s%d = new %s[numPoints][];\n", pdec.getID(), j, pdec.getTypeAccess());
						out.format(					"		for (int i = 0; i < numPoints; i++) {\n" +
								"			spliceLocal_%s%d[i] = new %s[depthParam + 2];\n" +
								"		}\n", pdec.getID(), j, pdec.getTypeAccess());
					}
				}
			}
		}

		out.print("	}\n" +
		"	public _SpliceSet() {\n");
		if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
			out.print("		spliceNodesSrc = new _SpliceNode[depthParam + 2];\n" +
			"		spliceNodesDest = new _SpliceNode[depthParam + 2];\n");
		} else {
			out.print("		spliceNodesSrc = new _SpliceNode[numNodes];\n" +
			"		spliceNodesDest = new _SpliceNode[numNodes];\n");	
		}
		out.print("		treeMap = new " + nodeClass + "[numNodes];\n");
		out.print("	}\n" +
				"\n" +
				"	public _SpliceNode getSpliceNode(int index) {\n" +
				"		return spliceNodesDest[index];\n" +
				"	}\n" +
				"	\n" +
				"	private void swapSpliceSets(int i) {\n" +
				"		_SpliceNode swap = spliceNodesSrc[i];\n" +
				"		spliceNodesSrc[i] = spliceNodesDest[i];\n" +
				"		spliceNodesDest[i] = swap;\n" +
				"	}\n" +
				"\n" +
		"	\n");
		ArrayList<String> params = new ArrayList<String>();
		params.addAll(getAllLoopInvariantParams());
		params.add("int _depth");
		params.add("int _index");
		params.add(nodeClass + " _node");
		if (needTreeClass) params.add(MethodInfo.getRecursiveCdec().getID() + " _tree");

		out.format("	public void initSpliceSetAndTree(%s) {\n", concat(params));
		if (needTreeClass) out.print("		treeClass = _tree;	\n");
		out.print("		treeMap[_index] = _node;	\n");
		if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
			out.print("		// for splice set stacks, can be init multiple times\n" +
			"		if (spliceNodesDest[_depth] != null) return;\n");
			out.format("		spliceNodesDest[_depth] = new _SpliceNode(%s);\n", concat(getAllLoopInvariantArgs()));
			out.format("		spliceNodesSrc[_depth] = new _SpliceNode(%s);\n", concat(getAllLoopInvariantArgs()));
		} else {
			out.format("		spliceNodesDest[_index] = new _SpliceNode(%s);\n", concat(getAllLoopInvariantArgs()));
			out.format("		spliceNodesSrc[_index] = new _SpliceNode(%s);\n", concat(getAllLoopInvariantArgs()));
		}
		out.print(
				"	}\n" +
		"	\n");


		out.print("\n");
		out.printf("	public void doSplice() %s {\n", getThrows());
		if (Globals.useBlock) {
			out.printf("		if (_Block.useBlock()) {\n");
			if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) out.print("		phaseInfix_Block(0, 1, 0, 1);\n");
			else out.print("		phaseInfix_Block(0, 0, 0, 0);\n");
			out.printf("		} else {\n");
			if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) out.print("		phaseInfix(0, 1, 0, 1);\n");
			else out.print("		phaseInfix(0, 0, 0, 0);\n");
			out.printf("		}\n");
		} else {
			if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) out.print("		phaseInfix(0, 1, 0, 1);\n");
			else out.print("		phaseInfix(0, 0, 0, 0);\n");			
		}

		out.print("	}\n" +
		"\n");

		gen_PhaseInfix(out);

		if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
			gen_NoOrder(out);
		} else if (Globals.optimizationLevel == Globals.OptimizationLevel.NODE_BASED_ORDER) {
			gen_NodeBasedOrder(out);
		} else {
			assert Globals.usePointIndex; // general order requires _pointIndex
			assert useSplicePoint; // _pointIndex means at least two points, hence SplicePoint should be used 
			gen_GeneralOrder(out);
			if (Globals.useBlock) gen_GeneralOrder_Block(out);
		}

		gen_ProcessMethods(out);

		if (Globals.optimizationLevel == Globals.OptimizationLevel.GENERAL_ORDER) {
			generateSpliceState(out);
		}
		if (MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size() > 0) {
			generateSpliceLocals(out);
		}

		out.print("}\n");
	}

	private static void gen_ProcessMethods(PrintStream out) {
		/*
		 * make processTopPhase method 
		 */
		String nodeVarName = Globals.isImplicitRecursiveStructure ? "node" : Globals.nodeParam.getID();
		ArrayList<String> params = new ArrayList<String>();
		if (useSplicePoint) {
			params.add("_SplicePoint _sp");
			params.addAll(getAllLoopInvariantParams());
		} else {
			params.addAll(getAllLoopVariantParams());
			params.addAll(getAllLoopInvariantParams());			
		}
		if (!Globals.isImplicitRecursiveStructure) params.add(Globals.nodeParam.toString());
		else params.add(nodeClass + " node");
		params.add("int _depth");

		if (Globals.optimizationLevel != Globals.OptimizationLevel.NO_ORDER) {
			params.add("int _phase");
			params.add("int _index");		
		}
		out.printf("	private void processTopPhase(%s) %s {\n", concat(params), getThrows());
		out.format("		if (%s == null) return;\n", nodeVarName);
		if (useSplicePoint) {
			for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodLoopVariantParams()) {
				out.format("		%s = _sp.%s;\n", pdec, pdec.getID());
			}
			if (Globals.usePointIndex) {
				out.format("		int _pointIndex = _sp._pointIndex;\n");
			}
		}

		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			if (callSetNum == null) {
				out.format("		%s %s = getSpliceLocal_%s(_sp._pointIndex, _depth);\n",
						pdec.getTypeAccess(), pdec.getID(), pdec.getID());
			} else {
				out.format("		%s %s;\n", pdec.getTypeAccess(), pdec.getID());
				ArrayList<IfPart> ifStr = new ArrayList<IfPart>();
				for (int i = 1; i < callSetNum; i++) {	// top phase is never _phase == 0
					IfPart ifPart = new IfPart();
					ifPart.cond = String.format("_phase== %d", i);
					ifPart.lines.add(String.format("%s = getSpliceLocal_%s%d(_sp._pointIndex, _depth);\n", pdec.getID(), pdec.getID(), i));
					ifStr.add(ifPart);
				}
				out.print(getIfStr("		", ifStr));
			}
		}

		String recursiveCallName = MethodInfo.getRecursiveMdec().getID() + Utils.methName_spliceSuffix;
		String callPrefix;
		if (needTreeClass) {
			callPrefix = "treeClass";
		} else if (Globals.isImplicitRecursiveStructure) {
			callPrefix = "node";
		} else {
			callPrefix = MethodInfo.getRecursiveCdec().getID();
		}
		ArrayList<String> args = new ArrayList<String>();
		args.addAll(getAllArgs());
		//args.addAll(getAdditionalLoopVariantArgs())
		if (Globals.usePointIndex) args.add("_pointIndex");
		args.add("_depth");
		if (Globals.optimizationLevel != Globals.OptimizationLevel.NO_ORDER) args.add("_index");
		args.add("this");
		out.format("		%s.%s(%s);\n", callPrefix, recursiveCallName, concat(args));
		out.print("	}\n");

		/*
		 * make processBottomPhase method 
		 */
		params = new ArrayList<String>();
		if (useSplicePoint) {
			params.add("_SplicePoint _sp");
			params.addAll(getAdditionalLoopVariantParams());
			params.addAll(getAllLoopInvariantParams());
		} else {
			params.addAll(getAllLoopVariantParams());
			params.addAll(getAllLoopInvariantParams());			
		}
		if (!Globals.isImplicitRecursiveStructure) params.add(Globals.nodeParam.toString());
		else params.add(nodeClass + " node");
		out.printf("	private void processBottomPhase(%s) %s {\n", concat(params), getThrows());

		out.format("		if (%s == null) return;\n", nodeVarName);
		if (useSplicePoint) {
			for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodLoopVariantParams()) {
				out.format("		%s = _sp.%s;\n", pdec, pdec.getID());
			}
		}
		recursiveCallName = MethodInfo.getRecursiveMdec().getID();
		out.format("		%s.%s(%s);\n", callPrefix, recursiveCallName, concat(getAllArgs()));
		out.print("	}\n");

		if (Globals.useBlock) {
			/*
			 * make processBottomPhase_Block method 
			 */
			params = new ArrayList<String>();
			params.add("_Block _block");
			params.addAll(getAllLoopInvariantParams());			

			if (!Globals.isImplicitRecursiveStructure) params.add(Globals.nodeParam.toString());
			else params.add(nodeClass + " node");
			if (!MethodInfo.getPdecNameToCallSetNumMap().isEmpty()) {
				params.add("int _callIndex");
			}
			out.printf("	private void processBottomPhase_Block(%s) %s {\n", concat(params), getThrows());

			out.format("		if (%s == null) return;\n", nodeVarName);
			out.print("		_BlockStack _stack = _BlockStack.getInstance();\n" +
			"		_stack.get(0).block = _block;\n");
			recursiveCallName = MethodInfo.getRecursiveMdec().getID();
			args = new ArrayList<String>();
			args.addAll(getAllLoopInvariantArgsIncludingNode());
			args.add("_stack");
			args.add("0");	// _depth
			if (!MethodInfo.getPdecNameToCallSetNumMap().isEmpty()) {
				args.add("_callIndex");
			}
			out.format("		%s.%s(%s);\n", callPrefix, recursiveCallName, concat(args));

			if (Globals.useUtilProfiler) {
				out.format("		Launcher.getProfiler().oneBlockDone();\n");
			}
			out.print("	}\n");
		}
	}

	private static void generateSpliceState(PrintStream out) {
		out.print(
				"	private static int [][] spliceStateBitmap;\n" +
				"	public static void markSpliceState(int pointIndex, int depth, int value) {\n" +
				"		spliceStateBitmap[pointIndex][depth] = value;\n" +
				"	}\n" +
				"	\n" +
				"	private static int getSpliceState(int pointIndex, int depth) {\n" +
				"		return spliceStateBitmap[pointIndex][depth];\n" +
		"	}\n");
	}

	private static void generateSpliceLocals(PrintStream out) {
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			if (callSetNum == null) {
				out.format("		private static %s spliceLocal_%s [][];\n", pdec.getTypeAccess(), pdec.getID());
			} else {
				for (int j = 0; j < callSetNum; j++) {
					out.format("		private static %s spliceLocal_%s%d [][];\n", pdec.getTypeAccess(), pdec.getID(), j);
				}
			}
		}
		out.print("	public static void saveSpliceLocals(int pointIndex, int depth, ");
		String str = "";
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			if (callSetNum == null) {
				str += String.format("%s %s, ", pdec.getTypeAccess(), pdec.getID());
			} else {
				for (int j = 0; j < callSetNum; j++) {
					str += String.format("%s %s%d, ", pdec.getTypeAccess(), pdec.getID(), j);
				}
			}
		}
		out.print(str.substring(0, str.length() - 2));

		out.print(") {\n");
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			if (callSetNum == null) {
				out.format("		spliceLocal_%s[pointIndex][depth] = %s;\n", pdec.getID(), pdec.getID());
			} else {
				for (int j = 0; j < callSetNum; j++) {
					out.format("		spliceLocal_%s%d[pointIndex][depth] = %s%d;\n", pdec.getID(), j, pdec.getID(), j);
				}
			}
		}

		out.print("	}\n" +
		"	\n");
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			if (callSetNum == null) {
				out.format("	private static %s getSpliceLocal_%s(int pointIndex, int depth) {\n", pdec.getTypeAccess(), pdec.getID());
				out.format("		return spliceLocal_%s[pointIndex][depth];\n", pdec.getID());
				out.print("	}\n");
			} else {
				for (int j = 0; j < callSetNum; j++) {
					out.format("	private static %s getSpliceLocal_%s%d(int pointIndex, int depth) {\n", pdec.getTypeAccess(), pdec.getID(), j);
					out.format("		return spliceLocal_%s%d[pointIndex][depth];\n", pdec.getID(), j);
					out.print("	}\n");			}
			}
		}
	}

	private static void gen_PhaseInfix(PrintStream out) {
		if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
			out.printf("	private  void phaseInfix(int headDepth, int headNodeIndex, int depth, int nodeIndex) %s {\n", getThrows());
			out.print("		if (depth < depthParam) {\n" +
					"			for (int i = 0; i < maxChildren; i++) {\n" +	// for no order, iterate over maxChildren instead of maxPhases
					"				if (i == 0) {\n" +
					"					if (treeMap[nodeIndex * maxChildren] != null) {\n" +
					"						phaseInfix(headDepth, headNodeIndex, depth + 1, nodeIndex * maxChildren);\n" +
					"					} else {\n" +
					"						topPhase(headDepth, headNodeIndex);\n" +
					"					}\n" +
					"				} else {\n" +
					"					if (treeMap[nodeIndex * maxChildren + i] != null) {\n");
			if (Globals.useElision) out.print("						if (depth < depthParamHalf) {\n");
					out.print("							phaseInfix(depth + 1, nodeIndex * maxChildren + i, depth + 1, nodeIndex * maxChildren + i);\n");
					if (Globals.useElision)	out.print("						} else {\n" +
					"							bottomUpperPhase(nodeIndex * maxChildren + i, depth + 1);\n" +
					"							break;\n" +
					"						}\n");
					out.print("					}\n" +
					"				}\n" +
					"			}\n" +
					"		} else {\n" +
					"			topPhase(headDepth, headNodeIndex);\n" +
					"			if (treeMap[nodeIndex * maxChildren] != null) {\n" +
					"				bottomPhase(nodeIndex * maxChildren);\n" +
					"			}\n" +
					"		}\n" +
			"	}\n");
		} else {
			out.printf("	private void phaseInfix(int headDepth, int headPhase, int depth, int phase) %s {\n", getThrows());
			out.print("		if (depth < depthParam) {\n" +
					"			for (int i = 0; i < maxPhases; i++) {\n" +
					"				if (i == 0) {\n" +
					"					phaseInfix(headDepth, headPhase, depth + 1, i);\n" +
					"				} else {\n");
			if (Globals.useElision) out.print("					if (depth < depthParamHalf) {\n");
					out.print("						phaseInfix(depth + 1, i, depth + 1, i);\n");
					if (Globals.useElision) out.print("					} else {\n" +
					"						bottomUpperPhase(depth + 1, i);\n" +
					"						break;\n" +
					"					}\n");
					out.print("				}\n" +
					"			}\n" +
					"		} else {\n" +
					"			topPhase(headDepth, headPhase);\n" +
					"			bottomPhase();\n" +
					"		}\n" +
					"	}\n" +
			"\n");
		}
		if (Globals.useBlock) {
			if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
				out.printf("	private  void phaseInfix_Block(int headDepth, int headNodeIndex, int depth, int nodeIndex) %s {\n", getThrows());
				out.print("		if (depth < depthParam) {\n" +
						"			for (int i = 0; i < maxChildren; i++) {\n" +	// for no order, iterate over maxChildren instead of maxPhases
						"				if (i == 0) {\n" +
						"					if (treeMap[nodeIndex * maxChildren] != null) {\n" +
						"						phaseInfix_Block(headDepth, headNodeIndex, depth + 1, nodeIndex * maxChildren);\n" +
						"					} else {\n" +
						"						topPhase(headDepth, headNodeIndex);\n" +
						"					}\n" +
						"				} else {\n" +
						"					if (treeMap[nodeIndex * maxChildren + i] != null) {\n");
						if (Globals.useElision) out.print("						if (depth < depthParamHalf) {\n");
						out.print("							phaseInfix_Block(depth + 1, nodeIndex * maxChildren + i, depth + 1, nodeIndex * maxChildren + i);\n");
						if (Globals.useElision) out.print("						} else {\n" +
						"							bottomUpperPhase_Block(nodeIndex * maxChildren + i, depth + 1);\n" +
						"							break;\n" +
						"						}\n");
						out.print("					}\n" +
						"				}\n" +
						"			}\n" +
						"		} else {\n" +
						"			topPhase(headDepth, headNodeIndex);\n" +
						"			if (treeMap[nodeIndex * maxChildren] != null) {\n" +
						"				bottomPhase_Block(nodeIndex * maxChildren);\n" +
						"			}\n" +
						"		}\n" +
				"	}\n");
			} else {
				out.printf("	private void phaseInfix_Block(int headDepth, int headPhase, int depth, int phase) %s {\n", getThrows());
				out.print("		if (depth < depthParam) {\n" +
						"			for (int i = 0; i < maxPhases; i++) {\n" +
						"				if (i == 0) {\n" +
						"					phaseInfix_Block(headDepth, headPhase, depth + 1, i);\n" +
						"				} else {\n");
				if (Globals.useElision) out.print("					if (depth < depthParamHalf) {\n");
						out.print("						phaseInfix_Block(depth + 1, i, depth + 1, i);\n");
						if (Globals.useElision) out.print("					} else {\n" +
						"						bottomUpperPhase_Block(depth + 1, i);\n" +
						"						break;\n" +
						"					}\n");
						out.print("				}\n" +
						"			}\n" +
						"		} else {\n" +
						"			topPhase(headDepth, headPhase);\n" +
						"			bottomPhase_Block();\n" +
						"		}\n" +
						"	}\n" +
				"\n");
			}
		}
	}

	private static void gen_NoOrder(PrintStream out) {
		out.printf("	private  void topPhase(final int depth, final int nodeIndex) %s {\n", getThrows());
		out.print("		if (depth == 0) return;\n" +
				"\n" +
				"		for (int i = depth; i <= depthParam + 1; i++) {\n" +
				"			if (spliceNodesDest[i] == null) continue;\n" +
				"			swapSpliceSets(i);\n" +
				"			_SpliceNode dest = spliceNodesDest[i];\n" +
				"			dest.clear();\n" +
				"		}\n" +
				"\n" +
				"		final _SpliceNode targetsn = spliceNodesSrc[depth];\n" +
				"		for (int i = depth; i <= depthParam + 1; i++) {\n" +
		"			_SpliceNode sn = spliceNodesSrc[i];\n");
		if (useSplicePoint) {
			//			out.format("					for (_SplicePoint point : sn.points) {\n");
			//			out.format("						_block.%s(%s);\n", blockAddWithPointIndexMethodName, getAllLoopVariantArgsWithPrefix("point.", blockAddWithPointIndexMethodName));
		} else {
			ArrayList<String> args = new ArrayList<String>();
			args.add("point");
			args.addAll(getAllLoopInvariantArgsWithPrefix("targetsn."));
			args.add("treeMap[nodeIndex]");
			args.add("depth");
			out.format("			for (%s point : sn.points) {\n", pointClass);
			out.format("				processTopPhase(%s);\n", concat(args));
		}
		out.print("			}\n" +
				"		}\n" +
		"	}\n");
		out.printf("	private void bottomUpperPhase(final int startNodeIndex, int depth) %s {\n", getThrows());
		out.print("		final _SpliceNode targetsn = spliceNodesDest[depth];\n" +
				"		int target = startNodeIndex;\n" +
				"		int targetRem = target % maxChildren;\n" +
				"		int targetBase = target - targetRem;\n" +
				"		for (int i = depth; i <= depthParam + 1; i++) {\n" +
				"			_SpliceNode sn = spliceNodesDest[i];\n" +
		"			if (sn == null) continue;\n");
		if (useSplicePoint) {
			out.format("		for (_SplicePoint point : sn.points) {\n");
		} else {
			out.format("			for (%s point : sn.points) {\n", pointClass);
		}
		out.format("				doBottomUpperPhase(point, targetBase, targetsn);\n");
		out.print("			}\n" +
				"		}\n" +
		"	}\n");
		out.printf("	private void doBottomUpperPhase(%s point, int targetBase, _SpliceNode sn) %s {\n", pointClass, getThrows());
		gen_NodeBasedOrder_BottomBody(out, false, true);
		out.print("	}\n" +
		"\n");
		out.printf("	private  void bottomPhase(final int startNodeIndex) %s {\n", getThrows());
		out.print("		_SpliceNode sn = spliceNodesDest[depthParam + 1];\n" +
				"		if (sn == null) return;\n" +
				"		int target = startNodeIndex;\n" +
				"		int targetRem = target % maxChildren;\n" +
		"		int targetBase = target - targetRem;\n");
		if (useSplicePoint) {
			out.format("		for (_SplicePoint point : sn.points) {\n");
		} else {
			out.format("		for (%s point : sn.points) {\n", pointClass);
		}
		out.format("			doBottomPhase(point, targetBase, sn);\n");
		out.print("		}\n" +
		"	}\n");
		out.printf("	private void doBottomPhase(%s point, int targetBase, _SpliceNode sn) %s {\n", pointClass, getThrows());
		gen_NodeBasedOrder_BottomBody(out, false, false);
		out.print("	}\n");

		if (Globals.useBlock) {
			out.printf("	private void bottomUpperPhase_Block(final int startNodeIndex, int depth) %s {\n", getThrows());
			out.print("		_Block _block = _Block.getInstance0();\n");
			out.print("		final _SpliceNode targetsn = spliceNodesDest[depth];\n" +
					"		int target = startNodeIndex;\n" +
					"		int targetRem = target % maxChildren;\n" +
					"		int targetBase = target - targetRem;\n" +
					"		for (int i = depth; i <= depthParam + 1; i++) {\n" +
					"			_SpliceNode sn = spliceNodesDest[i];\n" +
			"			if (sn == null) continue;\n");
			if (useSplicePoint) {
				out.format("			for (_SplicePoint point : sn.points) {\n");
				assert false; //TODO: handle
			} else {
				out.format("			for (%s point : sn.points) {\n", pointClass);
				out.format("				_block.add(point);\n");
			}

			out.print("				if (_block.size == _Block.maxBlockSize) {\n" +
					"					doBottomUpperPhase_Block(_block, targetBase, targetsn);\n" +
					"					_block.recycle();\n" +
					"				}\n" +
					"			}\n" +
					"		}\n" +
					"		if (!_block.isEmpty()) {\n" +
					"			doBottomUpperPhase_Block(_block, targetBase, targetsn);\n" +
					"			_block.recycle();\n" +
					"		}\n" +
			"	}\n");
			out.printf("	private void doBottomUpperPhase_Block(_Block _block, int targetBase, _SpliceNode sn) %s {\n", getThrows());
			gen_NodeBasedOrder_BottomBody(out, true, true);
			out.print("	}\n" +
			"\n");
			out.printf("	private  void bottomPhase_Block(final int startNodeIndex) %s {\n", getThrows());
			out.print("		_Block _block = _Block.getInstance0();\n");
			out.print("		_SpliceNode sn = spliceNodesDest[depthParam + 1];\n" +
					"		if (sn == null) return;\n" +
					"		int target = startNodeIndex;\n" +
					"		int targetRem = target % maxChildren;\n" +
			"		int targetBase = target - targetRem;\n");
			if (useSplicePoint) {
				assert false; //TODO: handle
			} else {
				out.format("		for (%s point : sn.points) {\n", pointClass);
				out.format("			_block.add(point);\n");
			}
			out.print("			if (_block.size == _Block.maxBlockSize) {\n" +
					"				doBottomPhase_Block(_block, targetBase, sn);\n" +
					"				_block.recycle();\n" +
					"			}\n" +
					"		}\n" +
					"		if (!_block.isEmpty()) {\n" +
					"			doBottomPhase_Block(_block, targetBase, sn);\n" +
					"			_block.recycle();\n" +
					"		}\n" +
			"	}\n");
			out.printf("	private void doBottomPhase_Block(_Block _block, int targetBase, _SpliceNode sn) %s {\n", getThrows());
			gen_NodeBasedOrder_BottomBody(out, true, false);
			out.print("	}\n");
		}
	}

	private static void gen_NodeBasedOrder(PrintStream out) {

		gen_NodeBasedOrder_TopPhase(out);

		out.printf("	private void bottomUpperPhase(int depth, int phase) %s {\n", getThrows());
		out.print("		int start = exp(depth);\n" +
				"		final int end = start * maxChildren;\n" +
				"		for (int i = start; i < end; i++) {\n" +
				"			_SpliceNode targetsn = spliceNodesDest[i];\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			int subStart = i;\n" +
				"			int subEnd = i + 1;\n" +
				"			while (subStart < numNodes) {\n" +
				"				for (int k = subStart; k < subEnd; k++) {\n" +
				"					_SpliceNode sn = spliceNodesDest[k];\n" +
		"					if (sn == null) continue;\n");
		if (useSplicePoint) {
			out.format("					for (_SplicePoint _sp : sn.points) {\n");
			out.format("						doBottomUpperPhase(_sp, targetBase, targetRem, targetsn, depth);\n");
		} else {
			out.format("					for (%s point : sn.points) {\n", pointClass);
			out.format("						doBottomUpperPhase(point, targetBase, targetRem, targetsn, depth);\n");
		}
		out.print("					}\n" +
				"				}\n" +
				"				subStart *= maxChildren;\n" +
				"				subEnd *= maxChildren;\n" +
				"			}\n" +
				"		}\n" +
		"	}\n");
		if (useSplicePoint) {
			out.printf("	private void doBottomUpperPhase(_SplicePoint point, int targetBase, int targetRem, _SpliceNode sn, int depth) %s {\n", getThrows());
		} else {
			out.printf("	private void doBottomUpperPhase(%s point, int targetBase, int targetRem, _SpliceNode sn, int depth) %s {\n", pointClass, getThrows());			
		}
		gen_NodeBasedOrder_BottomBody(out, false, true);
		out.print("	}\n");
		out.printf("	private void bottomPhase() %s {\n", getThrows());
		out.print("		for (int i = numLeafNodes; i < numNodes; i++) {\n" +
				"			_SpliceNode sn = spliceNodesDest[i];\n" +
				"			if (sn == null) continue;\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
		"			int targetBase = target - targetRem;\n");
		if (useSplicePoint) {
			out.format("			for (_SplicePoint point : sn.points) {\n", pointClass);
			out.format("				doBottomPhase(point, targetBase, targetRem, sn);\n");
		} else {
			out.format("			for (%s point : sn.points) {\n", pointClass);
			out.format("				doBottomPhase(point, targetBase, targetRem, sn);\n");
		}
		out.print("			}\n" +
				"		}\n" +
				"	}\n" +
		"\n");
		out.printf("	private void doBottomPhase(%s point, int targetBase, int targetRem, _SpliceNode sn) %s {\n",
				useSplicePoint ? "_SplicePoint" : pointClass, getThrows());
		gen_NodeBasedOrder_BottomBody(out, false, false);
		out.print("	}\n");

		if (Globals.useBlock) {
			/*
			 * bottomUpperPhase_Block
			 */
			out.printf("	private void bottomUpperPhase_Block(int depth, int phase) %s {\n", getThrows());
			out.print("		int start = exp(depth);\n" +
					"		final int end = start * maxChildren;\n" +
					"		_Block _block = _Block.getInstance0();\n" +
					"		for (int i = start; i < end; i++) {\n" +
					"			_SpliceNode targetsn = spliceNodesDest[i];\n" +
					"			int target = i;\n" +
					"			int targetRem = target % maxChildren;\n" +
					"			int targetBase = target - targetRem;\n" +
					"			int subStart = i;\n" +
					"			int subEnd = i + 1;\n" +
					"			while (subStart < numNodes) {\n" +
					"				for (int k = subStart; k < subEnd; k++) {\n" +
					"					_SpliceNode sn = spliceNodesDest[k];\n" +
			"					if (sn == null) continue;\n");
			if (useSplicePoint) {
				out.format("					for (_SplicePoint _sp : sn.points) {\n");
				out.format("						addBottomUpperPhaseBlock(_sp, _block, targetBase, targetRem, targetsn, depth);\n");
			} else {
				out.format("					for (%s point : sn.points) {\n", pointClass);
				out.format("						addBottomUpperPhaseBlock(point, _block, targetBase, targetRem, targetsn, depth);\n");
			}
			out.print("					}\n" +
					"				}\n" +
					"				subStart *= maxChildren;\n" +
					"				subEnd *= maxChildren;\n" +
					"			}\n" +
					"			doBottomUpperPhaseIfNotEmptyBlock(_block, targetBase, targetRem, targetsn);\n" +
					"		}\n" +
			"	}\n");

			ArrayList<String> params = new ArrayList<String>();
			if (useSplicePoint) params.add("_SplicePoint _sp");
			else params.add(String.format("%s point", pointClass));
			params.add("_Block _block");
			params.add("int targetBase");
			params.add("int targetRem");
			params.add("_SpliceNode sn");
			params.add("int _depth");
			out.printf("	private void addBottomUpperPhaseBlock(%s) %s {\n", concat(params), getThrows());


			ArrayList<String> args = new ArrayList<String>();
			if (useSplicePoint) args.addAll(getLoopVariantArgsWithPrefix("_sp."));
			else args.addAll(getLoopVariantArgs());
			for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
				Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
				if (callSetNum == null) {
					args.add(String.format("getSpliceLocal_%s(_sp._pointIndex, _depth)", pdec.getID()));
				} else {
					for (int i = 0; i < callSetNum; i++) {
						args.add(String.format("getSpliceLocal_%s%d(_sp._pointIndex, _depth)", pdec.getID(), i));
					}
				}
			}

			out.printf("		_block.add(%s);\n", concat(args));
			out.print("		if (_block.size != _Block.maxBlockSize) return;\n" +
					"		doBottomUpperPhase_Block(_block, targetBase, targetRem, sn);\n" +
					"		_block.recycle();\n" +
					"	}\n" +
			"	\n");
			out.printf("	private void doBottomUpperPhaseIfNotEmptyBlock(_Block _block, int targetBase, int targetRem, _SpliceNode sn) %s {\n", getThrows());
			out.print("		if (_block.isEmpty()) return;\n" +
					"		doBottomUpperPhase_Block(_block, targetBase, targetRem, sn);\n" +
					"		_block.recycle();\n" +
			"	}\n");

			out.printf("	private void doBottomUpperPhase_Block(_Block _block, int targetBase, int targetRem, _SpliceNode sn) %s {\n", getThrows());
			gen_NodeBasedOrder_BottomBody(out, true, true);
			out.print("	}\n");

			/*
			 * bottomPhase_Block
			 */
			out.printf("	private void bottomPhase_Block() %s {\n", getThrows());
			out.print("		_Block _block = _Block.getInstance0();\n");
			out.print("		for (int i = numLeafNodes; i < numNodes; i++) {\n" +
					"			_SpliceNode sn = spliceNodesDest[i];\n" +
					"			if (sn == null) continue;\n" +
					"			int target = i;\n" +
					"			int targetRem = target % maxChildren;\n" +
			"			int targetBase = target - targetRem;\n");
			if (useSplicePoint) {
				out.format("			for (_SplicePoint _sp : sn.points) {\n", pointClass);
				out.format("				addBottomPhaseBlock(_sp, _block, targetBase, targetRem, sn);\n");
			} else {
				out.format("			for (%s point : sn.points) {\n", pointClass);
				out.format("				addBottomPhaseBlock(point, _block, targetBase, targetRem, sn);\n");
			}
			out.print("			}\n" +
					"			doBottomPhaseIfNotEmptyBlock(_block, targetBase, targetRem, sn);\n" +
					"		}\n" +
					"	}\n" +
			"\n");

			params = new ArrayList<String>();
			if (useSplicePoint) params.add("_SplicePoint _sp");
			else params.add(String.format("%s point", pointClass));
			params.add("_Block _block");
			params.add("int targetBase");
			params.add("int targetRem");
			params.add("_SpliceNode sn");
			out.printf("	private void addBottomPhaseBlock(%s) %s {\n", concat(params), getThrows());


			args = new ArrayList<String>();
			if (useSplicePoint) args.addAll(getLoopVariantArgsWithPrefix("_sp."));
			else args.addAll(getLoopVariantArgs());
			for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
				Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
				if (callSetNum == null) {
					args.add(String.format("getSpliceLocal_%s(_sp._pointIndex, depthParam + 1)", pdec.getID()));
				} else {
					for (int i = 0; i < callSetNum; i++) {
						args.add(String.format("getSpliceLocal_%s%d(_sp._pointIndex, depthParam + 1)", pdec.getID(), i));
					}
				}
			}

			out.printf("		_block.add(%s);\n", concat(args));
			out.print("		if (_block.size != _Block.maxBlockSize) return;\n" +
					"		doBottomPhase_Block(_block, targetBase, targetRem, sn);\n" +
					"		_block.recycle();\n" +
					"	}\n" +
			"	\n");
			out.printf("	private void doBottomPhaseIfNotEmptyBlock(_Block _block, int targetBase, int targetRem, _SpliceNode sn) %s {\n", getThrows());
			out.print("		if (_block.isEmpty()) return;\n" +
					"		doBottomPhase_Block(_block, targetBase, targetRem, sn);\n" +
					"		_block.recycle();\n" +
			"	}\n");

			out.printf("	private void doBottomPhase_Block(_Block _block, int targetBase, int targetRem, _SpliceNode sn) %s {\n", getThrows());
			gen_NodeBasedOrder_BottomBody(out, true, false);
			out.print("	}\n");
		}
	}

	private static void gen_NodeBasedOrder_TopPhase(PrintStream out) {
		out.printf("	private void topPhase(int depth, int phase) %s {\n", getThrows());
		out.print("		if (depth == 0) return;\n" +
				"\n" +
				"		int start = exp(depth);\n" +
				"		final int end = start * maxChildren;\n" +
				"\n" +
				"		for (int i = start; i < numNodes; i++) {\n" +
				"			if (spliceNodesDest[i] == null) continue;\n" +
				"			swapSpliceSets(i);\n" +
				"			_SpliceNode dest = spliceNodesDest[i];\n" +
				"			dest.clear();\n" +
				"		}\n" +
				"		for (int i = start; i < end; i++) {\n" +
				"			_SpliceNode targetsn = spliceNodesSrc[i];\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
		"			int nextNode;\n");
		gen_NodeBasedOrder_TopBody(out);
		out.print("			int subStart = i;\n" +
				"			int subEnd = i + 1;\n" +
				"			while (subStart < numNodes) {\n" +
				"				for (int k = subStart; k < subEnd; k++) {\n" +
				"					_SpliceNode src = spliceNodesSrc[k];\n" +
		"					if (src == null) continue;\n");

		ArrayList<String> args = new ArrayList<String>();
		if (useSplicePoint) {
			out.format("					for (_SplicePoint _sp : src.points) {\n");
			args.add("_sp");
		} else {
			out.format("					for (%s point : src.points) {\n", pointClass);
			args.add("point");
		}		
		args.addAll(getAllLoopInvariantArgsWithPrefix("targetsn."));
		args.add("treeMap[nextNode]");
		args.add("depth");
		args.add("phase");
		args.add("nextNode");
		out.format("						processTopPhase(%s);\n", concat(args));
		out.print("					}\n" +
				"				}\n" +
				"				subStart *= maxChildren;\n" +
				"				subEnd *= maxChildren;\n" +
				"			}\n" +
				"		}\n" +
		"	}\n");
	}

	private static void gen_NodeBasedOrder_TopBody(PrintStream out) {

		for (int j = 1; j < RecursiveField.totalSize; j++) {
			int i = 0;
			if (RecursiveField.totalSize > 2) {
				if (j == 1) {
					out.format("			if (phase == %d) {\n", j);
				} else if (j == RecursiveField.totalSize - 1) {
					out.format("			} else {\n");
				} else {
					out.format("			} else if (phase == %d) {\n", j);
				}
			}

			for (RecursiveCallSet rcs : RecursiveCallSet.getSpliceSets()) {
				RecursiveField prevRf = rcs.recursiveFields.get(j - 1);
				RecursiveField rf = rcs.recursiveFields.get(j);
				if (rf == null) {
					//TODO: handle as in RT, save to parent
					assert false;
				}
				if (RecursiveCallSet.getSpliceSets().size() == 1) {
					// single call set, no brackets needed
				} else if (i == 0) {
					out.format("				if (targetRem == %d) {\n", prevRf.id);
				} else if (i == RecursiveCallSet.getSpliceSets().size() - 1) {
					out.format("				} else {\n");
				} else {
					out.format("				} else if (targetRem == %d) {\n", prevRf.id);
				}
				out.format("					nextNode = targetBase + %d;\n", rf.id);
				if (RecursiveCallSet.getSpliceSets().size() > 1 && 
						i == RecursiveCallSet.getSpliceSets().size() - 1) {
					out.print("				}\n");
				}
				i++;
			}

			if (RecursiveField.totalSize > 2) {
				if (j == RecursiveField.totalSize - 1) {
					out.print("			}\n");
				}
			}
		}
	}

	private static void gen_NodeBasedOrder_BottomBody(PrintStream out, boolean isBlocked, boolean isUpper) {
		ArrayList<IfPart> ifStr = new ArrayList<IfPart>();
		for (RecursiveCallSet rcs : RecursiveCallSet.getSpliceSets()) {
			RecursiveField firstField = rcs.recursiveFields.get(0);
			IfPart ifPart = new IfPart();
			ifPart.cond = String.format("targetRem == %d", firstField.id);

			ArrayList<String> args = new ArrayList<String>();
			for (int j = isUpper ? 1 : 0; j < rcs.recursiveFields.size(); j++) {
				RecursiveField nextField = rcs.recursiveFields.get(j);
				args = new ArrayList<String>();
				if (isBlocked) args.add("_block");
				else args.add("point");
				if (!isBlocked) {
					addAdditionalLoopVariantArgs(args, j, isUpper);
				}
				args.addAll(getAllLoopInvariantArgsWithPrefix("sn."));
				args.add(String.format("treeMap[targetBase + %d]", nextField.id));
				if (isBlocked && !MethodInfo.getPdecNameToCallSetNumMap().isEmpty()) args.add(String.format("%d", j));
				if (isBlocked) ifPart.lines.add(String.format("processBottomPhase_Block(%s);\n", concat(args)));
				else ifPart.lines.add(String.format("processBottomPhase(%s);\n", concat(args))); 
			}
			ifStr.add(ifPart);
		}
		out.print(getIfStr("\t\t", ifStr));
	}

	private static void gen_GeneralOrder(PrintStream out) {
		out.printf("	private void topPhase(final int depth, final int phase) %s {\n", getThrows());
		out.print("		if (depth == 0) return;\n" +
				"		int start = exp(depth);\n" +
				"		final int end = start * maxChildren;\n" +
				"		\n" +
				"		for (int i = start; i < numNodes; i++) {\n" +
				"			if (spliceNodesDest[i] == null) continue;\n" +
				"			swapSpliceSets(i);\n" +
				"			_SpliceNode dest = spliceNodesDest[i];\n" +
				"			dest.clear();\n" +
				"		}\n" +
				"		\n" +
				"		for (int i = start; i < end; i++) {\n" +
				"			_SpliceNode targetsn = spliceNodesSrc[i];\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			int parent = targetBase / maxChildren;\n" +
				"			int subStart = i;\n" +
				"			int subEnd = i + 1;\n" +
				"			while (subStart < numNodes) {\n" +
				"				for (int k = subStart; k < subEnd; k++) {\n" +
				"					_SpliceNode sn = spliceNodesSrc[k];\n" +
				"					if (sn == null) continue;\n" +
				"					for (_SplicePoint _sp : sn.points) {\n" +
				"						int spliceState = getSpliceState(_sp._pointIndex, depth);\n" +
		"						int nextNode = -1;\n");
		gen_GeneralOrder_TopBody(out);
		// for points to be saved in parent set, save to dest set
		out.print("						if (nextNode == parent) spliceNodesDest[nextNode].add(_sp);\n" +
				// for points in current depth phase, call processTopPhase				
		"						else {\n");

		ArrayList<String> args = new ArrayList<String>();
		args.add("_sp");
		args.addAll(getAllLoopInvariantArgsWithPrefix("targetsn."));
		args.add("treeMap[nextNode]");
		args.add("depth");
		args.add("phase");
		args.add("nextNode");
		out.format("							processTopPhase(%s);\n", concat(args));

		out.print("						}\n" +
				"					} \n" +
				"				}\n" +
				"				subStart *= maxChildren;\n" +
				"				subEnd *= maxChildren;\n" +
				"			}\n" +
				"		}\n" +
				"\n" +
		"	}\n");

		out.printf("	private void bottomUpperPhase(final int depth, final int phase) %s {\n", getThrows());
		out.print("		int start = exp(depth);\n" +
		"		final int end = start * maxChildren;\n");
		out.print("		for (int i = start; i < end; i++) {\n" +
				"			_SpliceNode targetsn = spliceNodesDest[i];\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			int subStart = i;\n" +
				"			int subEnd = i + 1;\n" +
				"			while (subStart < numNodes) {\n" +
				"				for (int k = subStart; k < subEnd; k++) {\n" +
				"					_SpliceNode sn = spliceNodesDest[k];\n" +
				"					if (sn == null) continue;\n" +
				"					for (_SplicePoint _sp : sn.points) {\n" +
		"						int spliceState = getSpliceState(_sp._pointIndex, depth); \n");
		out.printf("						doBottomUpperPhase(_sp, targetBase, targetsn, spliceState, depth);\n");
		out.print("					}\n" +
				"				}\n" +
				"				subStart *= maxChildren;\n" +
				"				subEnd *= maxChildren;\n" +
		"			}\n");
		out.print("		}\n" +
				"	}\n" +
		"	\n");
		out.printf("	private void doBottomUpperPhase(_SplicePoint point, int targetBase, _SpliceNode sn, int spliceState, int depth) %s {\n", getThrows());
		gen_GeneralOrder_BottomBody(out, false, true);
		out.print("	}\n");


		out.printf("	private void bottomPhase() %s {\n", getThrows());
		out.print("		for (int i = numLeafNodes; i < numNodes; i++) {\n" +
				"			_SpliceNode sn = spliceNodesDest[i];\n" +
				"			if (sn == null) continue;\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			for (_SplicePoint _sp : sn.points) {\n" +
		"				int spliceState = getSpliceState(_sp._pointIndex, depthParam + 1);\n");
		out.printf("						doBottomPhase(_sp, targetBase, sn, spliceState);\n");
		out.print("			}\n");
		out.print("		}\n" +
				"	}\n" +
				"	\n");
		out.printf("	private void doBottomPhase(_SplicePoint point, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
		gen_GeneralOrder_BottomBody(out, false, false);
		out.print("	}\n");
	}

	private static void gen_GeneralOrder_Block(PrintStream out) {
		out.printf("	private void bottomUpperPhase_Block(final int depth, final int phase) %s {\n", getThrows());
		out.print("		int start = exp(depth);\n" +
		"		final int end = start * maxChildren;\n");
		gen_GeneralOrder_BlockDecs(out);
		out.print("		for (int i = start; i < end; i++) {\n" +
				"			_SpliceNode targetsn = spliceNodesDest[i];\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			int subStart = i;\n" +
				"			int subEnd = i + 1;\n" +
				"			while (subStart < numNodes) {\n" +
				"				for (int k = subStart; k < subEnd; k++) {\n" +
				"					_SpliceNode sn = spliceNodesDest[k];\n" +
				"					if (sn == null) continue;\n" +
				"					for (_SplicePoint _sp : sn.points) {\n" +
		"						int spliceState = getSpliceState(_sp._pointIndex, depth); \n");
		gen_GeneralOrder_AddToBlocks(out, "addBottomUpperPhase_Block");
		out.print("					}\n" +
				"				}\n" +
				"				subStart *= maxChildren;\n" +
				"				subEnd *= maxChildren;\n" +
		"			}\n");
		gen_GeneralOrder_DoIfNotEmptyStmts(out, "doBottomUpperPhaseIfNotEmpty_Block");
		out.print("		}\n" +
				"	}\n" +
				"	\n");
		out.format("	private void addBottomUpperPhase_Block(_SplicePoint _sp, _Block _block, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
		out.format("					_block.add(%s);\n", concat(getAllLoopVariantArgsWithPrefix("_sp.")));
		out.print("		if (_block.size != _Block.maxBlockSize) return;\n" +
				"		doBottomUpperPhase_Block(_block, targetBase, sn, spliceState);\n" +
				"		_block.recycle();\n" +
				"	}\n" +
				"	\n");
				out.format("	private void doBottomUpperPhaseIfNotEmpty_Block(_Block _block, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
				out.print("		if (_block.isEmpty()) return;\n" +
				"		doBottomUpperPhase_Block(_block, targetBase, sn, spliceState);\n" +
				"		_block.recycle();\n" +
				"	}\n" +
				"\n");
		out.format("	private void doBottomUpperPhase_Block(_Block _block, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
		gen_GeneralOrder_BottomBody(out, true, true);
		out.print("	}\n");


		out.printf("	private void bottomPhase_Block() %s {\n", getThrows());
		gen_GeneralOrder_BlockDecs(out);

		out.print("		for (int i = numLeafNodes; i < numNodes; i++) {\n" +
				"			_SpliceNode sn = spliceNodesDest[i];\n" +
				"			_SpliceNode targetsn = sn;\n" +
				"			if (sn == null) continue;\n" +
				"			int target = i;\n" +
				"			int targetRem = target % maxChildren;\n" +
				"			int targetBase = target - targetRem;\n" +
				"			for (_SplicePoint _sp : sn.points) {\n" +
		"				int spliceState = getSpliceState(_sp._pointIndex, depthParam + 1);\n");
		gen_GeneralOrder_AddToBlocks(out, "addBottomPhase_Block");
		out.print("			}\n");
		gen_GeneralOrder_DoIfNotEmptyStmts(out, "doBottomPhaseIfNotEmpty_Block");
		out.print("		}\n" +
				"	}\n" +
				"	\n");
		out.printf("	private void addBottomPhase_Block(_SplicePoint _sp, _Block _block, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
		out.format("					_block.add(%s);\n", concat(getAllLoopVariantArgsWithPrefix("_sp.")));
		out.print("		if (_block.size != _Block.maxBlockSize) return;\n" +
				"		doBottomPhase_Block(_block, targetBase, sn, spliceState);\n" +
				"		_block.recycle();\n" +
				"	}\n" +
				"	\n");
				out.printf("	private void doBottomPhaseIfNotEmpty_Block(_Block _block, int targetBase, _SpliceNode sn, int spliceState) %s{\n", getThrows());
				out.print("		if (_block.isEmpty()) return;\n" +
				"		doBottomPhase_Block(_block, targetBase, sn, spliceState);\n" +
				"		_block.recycle();\n" +
				"	}\n" +
				"\n");
		out.printf("	private void doBottomPhase_Block(_Block _block, int targetBase, _SpliceNode sn, int spliceState) %s {\n", getThrows());
		gen_GeneralOrder_BottomBody(out, true, false);
		out.print("	}\n");
	}

	private static void gen_GeneralOrder_TopBody(PrintStream out) {
		ArrayList<IfPart> ifStr = new ArrayList<IfPart>();
		for (int i = 0; i < RecursiveCallSet.getSpliceSets().size(); i++) {
			RecursiveCallSet rcs = RecursiveCallSet.getSpliceSets().get(i);
			IfPart ifPart = new IfPart();
			ifPart.cond = String.format("spliceState == %d", i);
			ArrayList<IfPart> subIfStr = new ArrayList<IfPart>();
			// this assumes (RecursiveField.totalSize == maxChildren) == maxPhases
			for (int j = 1; j < RecursiveField.totalSize; j++) {
				IfPart subIfPart = new IfPart();
				subIfPart.cond = String.format("phase == %d", j);
				RecursiveField rf = null;
				if (j < rcs.recursiveFields.size()) { 
					rf = rcs.recursiveFields.get(j);
				}
				if (rf == null) {
					subIfPart.lines.add("nextNode = parent;\n");
				} else {
					subIfPart.lines.add(String.format("nextNode = targetBase + %d;\n", rf.id));
				}
				subIfStr.add(subIfPart);
			}
			for (String line : getIfStrLines("", subIfStr)) {
				ifPart.lines.add(line);	
			}
			ifStr.add(ifPart);
		}
		out.print(getIfStr("\t\t\t\t\t\t", ifStr));
	}

	private static void gen_GeneralOrder_BlockDecs(PrintStream out) {
		for (int i = 0; i < RecursiveCallSet.getSpliceSets().size(); i++) {
			out.format("			_Block _block%d = _Block.getInstance%d();\n", i, i);
		}
	}

	private static void gen_GeneralOrder_DoIfNotEmptyStmts(PrintStream out, String methName) {
		for (int i = 0; i < RecursiveCallSet.getSpliceSets().size(); i++) {
			out.format("			%s(_block%d, targetBase, targetsn, %d);\n", methName, i, i);
		}
	}

	private static void gen_GeneralOrder_AddToBlocks(PrintStream out, String methName) {
		ArrayList<IfPart> ifStr = new ArrayList<IfPart>();
		for (int i = 0; i < RecursiveCallSet.getSpliceSets().size(); i++) {
			IfPart ifPart = new IfPart();
			ifPart.cond = String.format("spliceState == %d", i);
			ifPart.lines.add(String.format("%s(_sp, _block%d, targetBase, targetsn, %d);\n", methName, i, i));
			ifStr.add(ifPart);
		}
		out.print(getIfStr("\t\t", ifStr));
	}

	private static void gen_GeneralOrder_BottomBody(PrintStream out, boolean isBlocked, boolean isUpper) {
		ArrayList<IfPart> ifStr = new ArrayList<IfPart>();
		for (int i = 0; i < RecursiveCallSet.getSpliceSets().size(); i++) {
			RecursiveCallSet rcs = RecursiveCallSet.getSpliceSets().get(i);
			IfPart ifPart = new IfPart();
			ifPart.cond = String.format("spliceState == %d", i);

			ArrayList<String> args = new ArrayList<String>();
			for (int j = isUpper ? 1 : 0; j < rcs.recursiveFields.size(); j++) {
				RecursiveField nextField = rcs.recursiveFields.get(j);
				args = new ArrayList<String>();
				if (isBlocked) args.add("_block");
				else args.add("point");
				if (!isBlocked) {
					addAdditionalLoopVariantArgs(args, j, isUpper);
				}
				args.addAll(getAllLoopInvariantArgsWithPrefix("sn."));
				args.add(String.format("treeMap[targetBase + %d]", nextField.id));
				if (isBlocked && !MethodInfo.getPdecNameToCallSetNumMap().isEmpty()) args.add(String.format("%d", j));
				if (isBlocked) ifPart.lines.add(String.format("processBottomPhase_Block(%s);\n", concat(args)));
				else ifPart.lines.add(String.format("processBottomPhase(%s);\n", concat(args))); 
			}
			ifStr.add(ifPart);
		}
		out.print(getIfStr("\t\t", ifStr));
	}
	
	private static void addAdditionalLoopVariantArgs(ArrayList<String> args, int phase, boolean isUpper) {
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAdditionalLoopVariantParams()) {
			Integer callSetNum = MethodInfo.getPdecNameToCallSetNumMap().get(pdec.getID());
			ArrayList<String> args2 = new ArrayList<String>();
			args2.add("point._pointIndex");
			args2.add(isUpper ? "depth" : "depthParam + 1");
			String methName = "getSpliceLocal_" + pdec.getID();
			if (callSetNum != null) methName += phase; 
			args.add(String.format("%s(%s)", methName, concat(args2)));
		} 
	}
}
