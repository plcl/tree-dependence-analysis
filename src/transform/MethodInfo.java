/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import AST.ASTNode;
import AST.Block;
import AST.ClassDecl;
import AST.CompilationUnit;
import AST.Expr;
import AST.ExprStmt;
import AST.List;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Modifier;
import AST.Opt;
import AST.ParameterDeclaration;
import AST.Stmt;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;

public class MethodInfo {
	

	public MethodDecl mdec;		// decl of intermediary method	
	public MethodAccess macc;	// call of next method within this method
	public ArrayList<Integer> loopVariantArgIdxs = new ArrayList<Integer>();
	public ArrayList<Integer> loopVariantParamIdxs = new ArrayList<Integer>();
	public ArrayList<Expr> loopVariantArgs = new ArrayList<Expr>();
	public ArrayList<Expr> loopInvariantArgs = new ArrayList<Expr>();
	public ArrayList<ParameterDeclaration> loopVariantParams = new ArrayList<ParameterDeclaration>();
	public ArrayList<ParameterDeclaration> loopInvariantParams = new ArrayList<ParameterDeclaration>();
	
	public MethodInfo(MethodDecl mdec, MethodAccess macc) {
		this.mdec = mdec;
		this.macc = macc;
	}
	
	private static ArrayList<MethodInfo> methodInfos = new ArrayList<MethodInfo>();
	private static ArrayList<Integer> outerForLoopVariantArgIdxs = new ArrayList<Integer>();
	private static ArrayList<Expr> outerForLoopVariantArgs = new ArrayList<Expr>();
	private static ArrayList<Expr> outerForLoopInvariantArgs = new ArrayList<Expr>();
	private static ArrayList<Integer> recursiveMethodLoopVariantParamIdxs = new ArrayList<Integer>();
	private static ArrayList<ParameterDeclaration> recursiveMethodLoopVariantParams = new ArrayList<ParameterDeclaration>();
	private static ArrayList<ParameterDeclaration> recursiveMethodLoopInvariantParams = new ArrayList<ParameterDeclaration>();
	private static ArrayList<ParameterDeclaration> recursiveMethodAdditionalLoopVariantParams = new ArrayList<ParameterDeclaration>();
	private static ArrayList<Integer> recursiveMethodAdditionalLoopVariantParamsIdxs = new ArrayList<Integer>();
	private static ArrayList<ParameterDeclaration> recursiveMethodAllLoopVariantParams = new ArrayList<ParameterDeclaration>();
	
	/*
	 * excluding nodeParam, and additional loop invariant args
	 */
	private static ArrayList<ParameterDeclaration> recursiveMethodAllLoopInvariantParams = new ArrayList<ParameterDeclaration>();	
	private static ArrayList<Integer> recursiveMethodAllLoopVariantParamsIdxs = new ArrayList<Integer>();
	
	/*
	 * excluding additional loop invariant args
	 */
	private static ArrayList<ParameterDeclaration> recursiveMethodAllLoopInvariantParamsIncludingNode = new ArrayList<ParameterDeclaration>();
	
	private static HashMap<String, Integer> pdecNameToCallSetNumMap = new HashMap<String, Integer>();
	private static MethodDecl recursiveMdec;
	private static MethodAccess entryMacc;
	private static ClassDecl recursiveCdec;
	
	public static HashMap<String, Integer> getPdecNameToCallSetNumMap() {
		return pdecNameToCallSetNumMap;
	}
	
	public static ArrayList<ParameterDeclaration> getRecursiveMethodAdditionalLoopVariantParams() {
		return recursiveMethodAdditionalLoopVariantParams;
	}

	public static ArrayList<Integer> getRecursiveMethodAdditionalLoopVariantParamsIdxs() {
		return recursiveMethodAdditionalLoopVariantParamsIdxs;
	}
	
	/*
	 * excluding nodeParam, and additional loop invariant args
	 */
	public static ArrayList<ParameterDeclaration> getRecursiveMethodAllLoopInvariantParams() {
		return recursiveMethodAllLoopInvariantParams;
	}
	
	/*
	 * excluding additional loop invariant args
	 */
	public static ArrayList<ParameterDeclaration> getRecursiveMethodAllLoopInvariantParamsIncludingNode() {
		return recursiveMethodAllLoopInvariantParamsIncludingNode;
	}
	
	public static ArrayList<ParameterDeclaration> getRecursiveMethodAllLoopVariantParams() {
		return recursiveMethodAllLoopVariantParams;
	}

	public static ArrayList<Integer> getRecursiveMethodAllLoopVariantParamsIdxs() {
		return recursiveMethodAllLoopVariantParamsIdxs;
	}

	public static ArrayList<MethodInfo> getMethodInfos() {
		return methodInfos;
	}
	
	public static ArrayList<Integer> getOuterForloopVariantArgIdxs() {
		return outerForLoopVariantArgIdxs;
	}
	
	public static ArrayList<Expr> getOuterForLoopVariantArgs() {
		return outerForLoopVariantArgs;
	}
	
	public static ArrayList<Expr> getOuterForLoopInvariantArgs() {
		return outerForLoopInvariantArgs;
	}
	
	public static ArrayList<Integer> getRecursiveMethodLoopVariantParamIdxs() {
		return recursiveMethodLoopVariantParamIdxs;
	}
	
	public static ArrayList<ParameterDeclaration> getRecursiveMethodLoopVariantParams() {
		return recursiveMethodLoopVariantParams;
	}
	
	public static ArrayList<ParameterDeclaration> getRecursiveMethodLoopInvariantParams() {
		return recursiveMethodLoopInvariantParams;
	}
	
	public static MethodDecl getRecursiveMdec() {
		return recursiveMdec;
	}
	
	public static ClassDecl getRecursiveCdec() {
		return recursiveCdec;
	}
	
	public static MethodAccess getEntryMacc() {
		return entryMacc;
	}
	
	public static void analyze() {
		generateMethodInfo();
		setNodeParam();
		analyzeRecursiveMethod();
	}
	
	private static void analyzeRecursiveMethod() {
		for (ParameterDeclaration pdec : recursiveMethodLoopVariantParams) {
			recursiveMethodAllLoopVariantParams.add(pdec);
		}
		for (int i : recursiveMethodLoopVariantParamIdxs) {
			recursiveMethodAllLoopVariantParamsIdxs.add(i);
		}
		
		/*
		 * propagate loop variant args
		 */
		Set<VarAccess> loopVariantArgsPropagation = new HashSet<VarAccess>();
		Set<Stmt> loopVariantControlStmts = new HashSet<Stmt>();
		for (ParameterDeclaration pdec : recursiveMethodLoopVariantParams) {
			loopVariantArgsPropagation.add(new VarAccess(pdec.getID()));
		}
		ArrayList<MethodAccess> recursionPoints = Utils.getRecursionPoints(recursiveMdec);
		ArrayList<Stmt> recursionPointStmts = new ArrayList<Stmt>();
		for (MethodAccess macc : recursionPoints) {
			recursionPointStmts.add(Utils.getEnclosingStmt(macc));
		}
		Utils.gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, recursiveMdec.getBlock(), recursionPointStmts);
		
		/*
		 * add pdecs which are loop variant within recursive method
		 */
		HashSet<Integer> pdecIdxs = new HashSet<Integer>();
		for (MethodAccess macc : recursionPoints) {
			for (int i = 0 ; i < macc.getNumArg(); i++) {
				Expr arg = macc.getArg(i);
				if (!(arg instanceof VarAccess)) continue;
				VarAccess vacc = (VarAccess) arg;
				boolean found = false;
				for (ParameterDeclaration pdec : recursiveMethodLoopVariantParams) {
					if (pdec.getID().equals(vacc.getID())) {
						found = true;
					}
				}
				if (found) continue;
				for (VarAccess vacc2 : loopVariantArgsPropagation) {
					if (vacc.equals(vacc2)) {
						pdecIdxs.add(i);
						break;
					}
				}
			}
		}
		for (int i : pdecIdxs) {
			ParameterDeclaration pdec = recursiveMdec.getParameter(i);
			recursiveMethodAdditionalLoopVariantParams.add(pdec);
			recursiveMethodAdditionalLoopVariantParamsIdxs.add(i);
			recursiveMethodAllLoopVariantParams.add(pdec);
			recursiveMethodAllLoopVariantParamsIdxs.add(i);
		}
		
		for (ParameterDeclaration pdec : recursiveMethodLoopInvariantParams) {
			if (recursiveMethodAdditionalLoopVariantParams.contains(pdec)) continue;
			recursiveMethodAllLoopInvariantParamsIncludingNode.add(pdec);
			if (Globals.nodeParam == null) {
				recursiveMethodAllLoopInvariantParams.add(pdec);					
			} else {
				if (!Globals.nodeParam.getID().equals(pdec.getID())) {
					recursiveMethodAllLoopInvariantParams.add(pdec);
				}
			}
		}
		
		for (Modifier mod : recursiveMdec.getModifiers().getModifiers()) {
			if (mod.getID().equals("static")) Globals.isStaticRecursiveMethod = true;
		}
	}
	
	private static void setNodeParam() {
		for (int i = 0; i < recursiveMdec.getNumParameter(); i++) {
			ParameterDeclaration pdec = recursiveMdec.getParameter(i);
			if (pdec.type().getID().equals(RecursiveField.recursiveClass.getID())) {
				Globals.nodeParam = pdec;
				Globals.nodeParamIdx = i;
			}
		}
	}
	
	private static void generateMethodInfo() {
		outerForLoopVariantArgIdxs = findLoopVariantArgIdxs();
		ArrayList<Integer> loopVariantParamIdxs = outerForLoopVariantArgIdxs;
		for (int i = Globals.numMethods - 1; i >= 1; i--) {
			MethodAccess prevMacc = Globals.mainPath.get(i - 1);
			MethodAccess macc = Globals.mainPath.get(i);
			MethodDecl mdec = macc.decl();
			MethodInfo minfo = new MethodInfo(mdec, prevMacc);
			for (int j = 0; j < mdec.getNumParameter(); j++) {
				if (loopVariantParamIdxs.contains(j)) {
					minfo.loopVariantParams.add(mdec.getParameter(j));
				} else {
					minfo.loopInvariantParams.add(mdec.getParameter(j));
				}
			}
			ArrayList<VariableDeclaration> vdecs = new ArrayList<VariableDeclaration>();
			mdec.gatherVarDecs(vdecs);
			for (int j = 0; j < prevMacc.getNumArg(); j++) {
				Expr arg = prevMacc.getArg(j);
				if (!(arg instanceof VarAccess)) continue;
				VarAccess vacc = (VarAccess) arg;
				boolean isLoopVariantArg = false;
				for (int k : loopVariantParamIdxs) {
					ParameterDeclaration pdec = mdec.getParameter(k);
					if (vacc.getID().equals(pdec.getID())) {
						isLoopVariantArg = true;
						break;
					}
				}
				for (VariableDeclaration vdec : vdecs) {
					if (vacc.getID().equals(vdec.getID())) {
						isLoopVariantArg = true;
						break;
					}
				}
				if (isLoopVariantArg) {
					minfo.loopVariantArgIdxs.add(j);
					minfo.loopVariantArgs.add(arg);
				} else {
					minfo.loopInvariantArgs.add(arg);
				}
			}
			minfo.loopVariantParamIdxs = loopVariantParamIdxs;
			methodInfos.add(minfo);
			loopVariantParamIdxs = minfo.loopVariantArgIdxs;
		}

		recursiveMethodLoopVariantParamIdxs = loopVariantParamIdxs;
		recursiveMdec = Globals.mainPath.get(0).decl();
		entryMacc = Globals.mainPath.get(Globals.numMethods - 1);
		
		for (int j = 0; j < recursiveMdec.getNumParameter(); j++) {
			if (loopVariantParamIdxs.contains(j)) {
				recursiveMethodLoopVariantParams.add(recursiveMdec.getParameter(j));
			} else {
				recursiveMethodLoopInvariantParams.add(recursiveMdec.getParameter(j));
			}
		}
		
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(recursiveMdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		recursiveCdec = (ClassDecl) tdec;
			
		int i = 0;
		for (MethodInfo minfo : methodInfos) {
			System.out.printf("%d \t %s \t %s: ", i++, minfo.mdec.getID(), minfo.macc);
			for (int j : minfo.loopVariantArgIdxs) {
				System.out.printf("%s ", minfo.macc.getArg(j));
			}
			System.out.println();
		}
	}
	
	private static ArrayList<Integer> findLoopVariantArgIdxs() {
		ArrayList<Integer> loopVariantArgs = new ArrayList<Integer>();
		MethodAccess entry = Globals.mainPath.get(Globals.numMethods - 1);
		for (int i = 0; i < entry.getNumArg(); i++) {
			boolean isLoopVariant = false;
			Expr arg = entry.getArg(i);
			if (arg instanceof VarAccess) {					
				VarAccess vacc = (VarAccess) arg;
				if (!vacc.isLoopInvariant(Globals.outerFor)) {
					isLoopVariant = true;
				}
			}
			if (isLoopVariant) {
				loopVariantArgs.add(i);
				outerForLoopVariantArgs.add(arg);
			} else {
				outerForLoopInvariantArgs.add(arg);
			}
		}
		return loopVariantArgs;
	}
	
	public static void setupPdecNameToCallSetNumMap(MethodDecl mdec, ArrayList<RecursiveCallSet> rcss) {
		/*
		 * mark pdecs which vary per call 
		 * this assumes that maccs have same form as mdec, so no morphing should occur before this
		 */
		if (!pdecNameToCallSetNumMap.isEmpty()) return;	// do only once
		for (RecursiveCallSet rcs : rcss) {
			for (int i = 0; i < mdec.getNumParameter(); i++) {
				if (i == Globals.nodeParamIdx) continue;
				MethodAccess macc1 = rcs.maccs.get(0);
				for (int j = 1; j < rcs.maccs.size(); j++) {
					MethodAccess macc2 = rcs.maccs.get(j);
					if (!macc1.getArg(i).toString().equals(macc2.getArg(i).toString())) {
						Integer callSetNum = pdecNameToCallSetNumMap.get(mdec.getParameter(i).getID());
						if (callSetNum == null) {
							callSetNum = 0; 
						}
						callSetNum = callSetNum > rcs.maccs.size() ? callSetNum : rcs.maccs.size();
						pdecNameToCallSetNumMap.put(mdec.getParameter(i).getID(), callSetNum);
					}
				}				
			}
		}
	}
}
