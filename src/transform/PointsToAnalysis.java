/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import AST.*;

public class PointsToAnalysis {
	
	HashMap<String, HashSet<Expr>> set = new HashMap<String, HashSet<Expr>>();
	
	public PointsToAnalysis(MethodDecl mdec) {
		analyze(mdec);
	}
	
	public void analyze(MethodDecl mdec) {
		ArrayList<AssignExpr> assigns = new ArrayList<AssignExpr>();
		mdec.gatherAssigns(assigns);
		
		for (AssignExpr aexpr : assigns) {
			Expr dest = aexpr.getDest();
			if (dest instanceof VarAccess) {
				VarAccess vacc = (VarAccess) dest;
				String key = vacc.getID();
				HashSet<Expr> list = set.get(key);
				if (list == null) {
					list = new HashSet<Expr>();
					set.put(key, list);
				}
				list.add(aexpr.getSource());
			}
		}
		ArrayList<VariableDeclaration> varDecs = new ArrayList<VariableDeclaration>();
		mdec.gatherVarDecs(varDecs);
		
		for (VariableDeclaration varDec : varDecs) {
			if (varDec.hasInit()) {
				Expr src = new VarAccess(varDec.getID());
				VarAccess vacc = (VarAccess) src;
				String key = vacc.getID();
				HashSet<Expr> list = set.get(key);
				if (list == null) {
					list = new HashSet<Expr>();
					set.put(key, list);
				}
				list.add(varDec.getInit());
			}
		}
	}
	
	public boolean pointsToMany(VarAccess vacc) {
		String key = vacc.getID();
		HashSet<Expr> list = set.get(key);
		if (list == null) {
			return false;
		}
		if (list.size() > 1) {
			return true;
		} 
		for (Expr expr : list) {
			if (expr instanceof VarAccess) {
				VarAccess nextVacc = (VarAccess) expr;
				return pointsToMany(nextVacc);
			}
		}
		return false;
	}
	
	public Expr getFinalPointsTo(VarAccess vacc) {
		String key = vacc.getID();
		HashSet<Expr> list = set.get(key);
		if (list == null) {
			return vacc;
		}
		if (list.size() > 1) {
			return null;
		} 
		for (Expr expr : list) {
			if (expr instanceof VarAccess) {
				VarAccess nextVacc = (VarAccess) expr;
				return getFinalPointsTo(nextVacc);
			}
			return expr;
		}
		return vacc;
	}
}
