/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import AST.AbstractDot;
import AST.Access;
import AST.AddExpr;
import AST.AssignSimpleExpr;
import AST.Block;
import AST.BooleanLiteral;
import AST.ClassDecl;
import AST.CompilationUnit;
import AST.ContinueStmt;
import AST.Expr;
import AST.ExprStmt;
import AST.IfStmt;
import AST.ImportDecl;
import AST.IntegerLiteral;
import AST.List;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Opt;
import AST.PackageAccess;
import AST.ParameterDeclaration;
import AST.PrimitiveTypeAccess;
import AST.ReturnStmt;
import AST.SingleTypeImportDecl;
import AST.Stmt;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;

public class CommonTransform {

	private static void addImport(CompilationUnit cu, String name) {
		boolean found = false;
		ImportDecl newIdec = new SingleTypeImportDecl(new PackageAccess(name));
		for (ImportDecl idec : cu.getImportDecls()) {
			if (idec.toString().equals(newIdec.toString())) {
				found = true;
			}
		}
		if (!found) {
			cu.addImportDecl(newIdec);
		}
	}

	private static void transformProEpilogue(MethodInfo minfo, boolean last) {
		MethodDecl mdec = minfo.mdec;
		MethodAccess macc = minfo.macc;
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;

		Expr maccLeft = null;
		if (macc.getParent() instanceof AbstractDot) {
			AbstractDot dot = (AbstractDot) macc.getParent();
			maccLeft = dot.getLeft();
		}

		/*
		 * divide block into prologue and epilogue
		 */
		Stmt maccStmt = Utils.getEnclosingStmt(macc);		
		ArrayList<Stmt> prologue = new ArrayList<Stmt>();
		ArrayList<Stmt> epilogue = new ArrayList<Stmt>();
		ArrayList<VariableDeclaration> prologueVarDecs = new ArrayList<VariableDeclaration>();
		boolean nextEntryFound = false;
		for (Stmt stmt : mdec.getBlock().getStmts()) {
			boolean nextEntryFoundNow = false;
			if (stmt == maccStmt) {
				nextEntryFoundNow = true;	
				nextEntryFound = true;
			}
			if (!nextEntryFoundNow) {
				if (!nextEntryFound) {
					if (stmt instanceof VariableDeclaration) {
						VariableDeclaration vdec = (VariableDeclaration) stmt;
						prologueVarDecs.add(vdec);
						prologue.add(stmt);
					} else {
						prologue.add(stmt);
					}
				} else {
					epilogue.add(stmt);
				}
			}
		}

		/*
		 * all local variables declared in prologue and used in epilogue need to be saved in IntermediaryState
		 */
		ArrayList<VariableDeclaration> usedInEpilogueVarDecs = new ArrayList<VariableDeclaration>();
		for (VariableDeclaration vdec : prologueVarDecs) {
			boolean used = false;
			for (Stmt stmt : epilogue) {
				if (stmt.findVarUse(vdec.getID())) {
					used = true;
					break;
				}
			}
			if (maccStmt.findVarUse(vdec.getID())) {
				used = true;
			}
			if (used) {
				usedInEpilogueVarDecs.add(vdec);
			}
		}

		/*
		 * generate prologue splice method
		 */
		if (Globals.useSplice) {
			Block prologueBody = new Block();
			for (Stmt stmt : prologue) {
				prologueBody.addStmt((Stmt) stmt.fullCopy());
			}
			for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
				String methodName = IntermediaryStateClasses.saveInterState(vdec, mdec.getID());
				MethodAccess saveMacc = new MethodAccess(methodName, new List<Expr>());
				saveMacc.addArg(new VarAccess(vdec.getID()));
				prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState), saveMacc)));
			}
			if (last) {
				MethodAccess prologueMacc = macc.fullCopy();
				prologueMacc.setID(prologueMacc.getID() + Utils.methName_spliceSuffix);
				if (Globals.usePointIndex) {
					prologueMacc.addArg(new AbstractDot(new VarAccess(Utils.varName_InterState),
							new MethodAccess(Utils.methName_IntermediaryState_getIndex, new List())));
				}
				prologueMacc.addArg(new IntegerLiteral(0));	// _depth
				if (Globals.optimizationLevel != Globals.OptimizationLevel.NO_ORDER) {
					prologueMacc.addArg(new IntegerLiteral(1));	// _index
				}
				prologueMacc.addArg(new AbstractDot(new VarAccess(Utils.varName_InterState), new VarAccess(Utils.varName_InterState_SpliceSet)));
				Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
				prologueBody.addStmt(new ExprStmt(prologueMaccExpr));
			} else {
				MethodAccess prologueMacc = macc.fullCopy();
				prologueMacc.setID(prologueMacc.getID() + Utils.methName_prologueSpliceSuffix);
				prologueMacc.addArg(new VarAccess(Utils.varName_InterState));
				Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
				prologueBody.addStmt(new ExprStmt(prologueMaccExpr));
			}
			MethodDecl prologueMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
					new PrimitiveTypeAccess("void"),
					mdec.getID() + Utils.methName_prologueSpliceSuffix,
					mdec.getParameters().fullCopy(),
					mdec.getExceptionList().fullCopy(),
					new Opt<Block>(prologueBody));
			prologueMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_InterState));
			cdec.addMemberMethod(prologueMdec);
		}

		/*
		 * generate prologue method
		 */
		if (Globals.useBlock) {
			Block prologueBody = new Block();
			for (Stmt stmt : prologue) {
				prologueBody.addStmt((Stmt) stmt.fullCopy());
			}
			for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
				String methodName = IntermediaryStateClasses.saveInterState(vdec, mdec.getID());
				MethodAccess saveMacc = new MethodAccess(methodName, new List<Expr>());
				saveMacc.addArg(new VarAccess(vdec.getID()));
				prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState), saveMacc)));
			}
			if (last) {
				MethodAccess addMacc = new MethodAccess(Utils.methName_Block_add, new List());
				for (Expr arg : minfo.loopVariantArgs) {
					addMacc.addArg((Expr) arg.fullCopy());
				}
				prologueBody.addStmt(new ExprStmt(new AbstractDot(
						new AbstractDot(new VarAccess(Utils.varName_InterState), new VarAccess("block")), addMacc)));				
			} else {
				MethodAccess prologueMacc = macc.fullCopy();
				prologueMacc.setID(prologueMacc.getID() + Utils.methName_prologueSuffix);
				prologueMacc.addArg(new VarAccess(Utils.varName_InterState));
				Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
				prologueBody.addStmt(new ExprStmt(prologueMaccExpr));
			}
			MethodDecl prologueMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
					new PrimitiveTypeAccess("void"),
					mdec.getID() + Utils.methName_prologueSuffix,
					mdec.getParameters().fullCopy(),
					mdec.getExceptionList().fullCopy(),
					new Opt<Block>(prologueBody));
			prologueMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_InterState));
			cdec.addMemberMethod(prologueMdec);
		}

		/*
		 * generate epilogue method
		 */
		Block epilogueBody = new Block();
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.getLoadInterStateMethodName(vdec, mdec.getID());
			MethodAccess loadMacc = new MethodAccess(methodName, new List<Expr>());
			VariableDeclaration loadVdec = new VariableDeclaration((Access) vdec.getTypeAccess().fullCopy(), vdec.getID(),
					new AbstractDot(new VarAccess(Utils.varName_InterState), loadMacc));
			epilogueBody.addStmt(loadVdec);
		}
		if (!last) {
			MethodAccess epilogueMacc = macc.fullCopy();
			epilogueMacc.setID(epilogueMacc.getID() + Utils.methName_epilogueSuffix);
			epilogueMacc.addArg(new VarAccess(Utils.varName_InterState));
			Stmt epilogueMaccStmt = (Stmt) maccStmt.fullCopy();
			epilogueMaccStmt.findMethodAccess(macc.getID()).swapWith(epilogueMacc);
			epilogueBody.addStmt(epilogueMaccStmt);
		}
		for (Stmt stmt : epilogue) {
			epilogueBody.addStmt((Stmt) stmt.fullCopy());
		}
		MethodDecl epilogueMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				(Access) mdec.getTypeAccess().fullCopy(),
				mdec.getID() + Utils.methName_epilogueSuffix,
				mdec.getParameters().fullCopy(),
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(epilogueBody));
		epilogueMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_InterState));
		cdec.addMemberMethod(epilogueMdec);
	}

	private static void transformProEpilogues() {
		ArrayList<MethodInfo> minfos = MethodInfo.getMethodInfos();
		for (int i = 0; i < minfos.size(); i++) {
			MethodInfo minfo = minfos.get(i);
			addImport(Utils.getEnclosingCompilationUnit(minfo.mdec), Globals.blockPackageName + "." + Utils.typeName_IntermediaryState);			
			transformProEpilogue(minfo, i == minfos.size() - 1);
		}
		addImport(Utils.getEnclosingCompilationUnit(MethodInfo.getEntryMacc()), Globals.blockPackageName + "." + Utils.typeName_IntermediaryState);
	}

	private static void transformAutotuneIntermediary(MethodInfo minfo, boolean last) {
		MethodDecl mdec = minfo.mdec;
		MethodAccess macc = minfo.macc;
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;

		MethodDecl tuneMdec = mdec.fullCopy();
		tuneMdec.setID(mdec.getID() + Utils.methName_autotuneSuffix);
		tuneMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_Autotuner), Utils.varName_Autotuner));
		MethodAccess tuneMacc = tuneMdec.findMethodAccess(macc.getID());
		tuneMacc.setID(macc.getID() + Utils.methName_autotuneSuffix);
		if (last) {
			tuneMacc.addArg(new IntegerLiteral(0));
		}
		tuneMacc.addArg(new VarAccess(Utils.varName_Autotuner));
		cdec.addMemberMethod(tuneMdec);
	}

	private static void transformAutotuneRecursive(MethodDecl mdec) {
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;

		MethodDecl tuneMdec = mdec.fullCopy();
		tuneMdec.setID(mdec.getID() + Utils.methName_autotuneSuffix);
		tuneMdec.addParameter(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_Depth));
		tuneMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_Autotuner), Utils.varName_Autotuner));
		ArrayList<MethodAccess> maccs = new ArrayList<MethodAccess>(); 
		tuneMdec.gatherMethodAccesses(maccs);
		for (MethodAccess macc : maccs) {
			if (!macc.getID().equals(mdec.getID())) continue;
			macc.setID(mdec.getID() + Utils.methName_autotuneSuffix);
			macc.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
			macc.addArg(new VarAccess(Utils.varName_Autotuner));
		}
		cdec.addMemberMethod(tuneMdec);

		if (Globals.useBlock) {
			tuneMdec.getBlock().getStmt(0).insertStmtBefore(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
					new MethodAccess(Utils.methName_Block_profileWorkDone, new List<Expr>().add(new IntegerLiteral(1))))));
		}
		if (Globals.useSplice) {
			transformAutotuneRecursive_Splice(tuneMdec);
		}
	}

	private static void transformAutotuneRecursive_Splice(MethodDecl tuneMdec) {
		Block body = tuneMdec.getBlock();

		/*
		 * add boolean _pointStopped = true;
		 */
		body.addStmtStart(new VariableDeclaration(new PrimitiveTypeAccess("boolean"), Utils.varName_PointStopped,
				new BooleanLiteral(true)));

		/*
		 * add if (_pointStopped) _SpliceSet.profileDepth(_depth); at end
		 */
		List<Expr> profileDepthArgs = new List<Expr>();
		profileDepthArgs.add(new VarAccess(Utils.varName_Depth));
		IfStmt ifPointStoppedProfileDepth = new IfStmt(new VarAccess(Utils.varName_PointStopped),
				new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
						new MethodAccess(Utils.methName_SpliceSet_profileDepth, profileDepthArgs))));
		body.addStmtEnd(ifPointStoppedProfileDepth);

		/*
		 * add if (_pointStopped) _SpliceSet.profileDepth(_depth); before returns
		 */
		ArrayList<ReturnStmt> returns = new ArrayList<ReturnStmt>();
		body.gatherReturns(returns);
		for (ReturnStmt stmt : returns) {
			stmt.insertStmtBefore(new IfStmt(new VarAccess(Utils.varName_PointStopped),
					new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
							new MethodAccess(Utils.methName_SpliceSet_profileDepth, profileDepthArgs.fullCopy()))))); // fullCopy because used previously
		}

		ArrayList<ParameterDeclaration> loopVariantPdecs = MethodInfo.getRecursiveMethodLoopVariantParams();
		ArrayList<RecursiveCallSet> rcsSets = RecursiveCallSet.analysis(tuneMdec, loopVariantPdecs);
		for (RecursiveCallSet rcs : rcsSets) {
			Stmt last = Utils.getEnclosingStmt(rcs.maccs.get(rcs.maccs.size() - 1));
			last.insertStmtAfter(new ExprStmt(new AssignSimpleExpr(new VarAccess(Utils.varName_PointStopped), new BooleanLiteral(false))));
		}

		List<Expr> profileConvergenceArgs = new List<Expr>();
		profileConvergenceArgs.add(new VarAccess(MethodInfo.getRecursiveMethodLoopVariantParams().get(0).getID()));
		if (Globals.isImplicitRecursiveStructure) {
			profileConvergenceArgs.add(new VarAccess("this"));	
		} else {
			profileConvergenceArgs.add(new VarAccess(Globals.nodeParam.getID()));
		}
		AbstractDot profileConvergence = new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
				new MethodAccess(Utils.methName_SpliceSet_profileConvergence, profileConvergenceArgs));
		body.addStmtStart(new ExprStmt(profileConvergence));
	}

	private static void transformAutotune() {
		ArrayList<MethodInfo> minfos = MethodInfo.getMethodInfos();
		for (int i = 0; i < minfos.size(); i++) {
			MethodInfo minfo = minfos.get(i);
			addImport(Utils.getEnclosingCompilationUnit(minfo.mdec), Globals.blockPackageName + "." + Utils.typeName_Autotuner);
			transformAutotuneIntermediary(minfo, i == minfos.size() - 1);
		}	
		addImport(Utils.getEnclosingCompilationUnit(MethodInfo.getEntryMacc()), Globals.blockPackageName + "." + Utils.typeName_Autotuner);
		addImport(Utils.getEnclosingCompilationUnit(MethodInfo.getRecursiveMdec()), Globals.blockPackageName + "." + Utils.typeName_Autotuner);
		transformAutotuneRecursive(MethodInfo.getRecursiveMdec());
	}

	public static void transform() {
		transformProEpilogues();
		if (Globals.useAutotune) {
			transformAutotune();
		}
	}
}
