package transform;

import java.util.ArrayList;

public class AccessNode {
	String vartype;
	String varname;
	ArrayList<String> aliases;
	String condition;
	
	public AccessNode(String type, String name, String cond) {
		this.vartype = type;
		this.varname = name;
		this.aliases = new ArrayList<String>();
		this.condition = cond;
	}
	
	public AccessNode(String type, String name) {
		this.vartype = type;
		this.varname = name;
		this.aliases = new ArrayList<String>();
		this.condition = "";
	}
	
	public void addAlias(String s) {
		aliases.add(s);
	}
	
	public void setCondition(String s) {
		condition = s;
	}
	
	public String getType() {
		return vartype;
	}
	
	public String getVarname() {
		return varname;
	}
	
	public void setName(String name) {
		varname = name;
	}
	
	public void setType(String type) {
		vartype = type;
	}
	
	public ArrayList<String> getAliases() {
		return aliases;
	}
	
	public String getCondition() {
		return condition;
	}
}