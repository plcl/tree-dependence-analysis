/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import AST.ASTNode;
import AST.AbstractDot;
import AST.Access;
import AST.AddExpr;
import AST.ArrayAccess;
import AST.ArrayTypeAccess;
import AST.AssignExpr;
import AST.AssignPlusExpr;
import AST.AssignSimpleExpr;
import AST.Binary;
import AST.Block;
import AST.BooleanLiteral;
import AST.BreakStmt;
import AST.ClassDecl;
import AST.ClassInstanceExpr;
import AST.CompilationUnit;
import AST.ConditionalExpr;
import AST.ContinueStmt;
import AST.EQExpr;
import AST.Expr;
import AST.ExprStmt;
import AST.ForStmt;
import AST.IfStmt;
import AST.IntegerLiteral;
import AST.LTExpr;
import AST.List;
import AST.LogNotExpr;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Modifier;
import AST.Modifiers;
import AST.NEExpr;
import AST.Opt;
import AST.ParameterDeclaration;
import AST.PostIncExpr;
import AST.PrimitiveTypeAccess;
import AST.ReturnStmt;
import AST.Stmt;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;
import AST.WhileStmt;

public class BlockTransform {

	public static void transform() {
		if (MethodInfo.getRecursiveMethodLoopVariantParams().size() +
				MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size() > 1) {
			Globals.usePointClass = true;
		}
		transformMapRoot();
		if (!Globals.useSplice) transformEntry();
		else addBlockClassImports(Utils.getEnclosingCompilationUnit(MethodInfo.getEntryMacc()));
		MethodDecl blockMdec = transformBlockRecursive();
		if (Globals.useAutotune) {
			transformAutotuneBlockRecursive(blockMdec);
		}
	}

	private static void transformMapRoot() {
		for (int i = 0; i < MethodInfo.getMethodInfos().size(); i++) {	
			transformMapRootIntermediary(MethodInfo.getMethodInfos().get(i), i == MethodInfo.getMethodInfos().size() - 1);
		}
		transformMapRootRecursive();
	}

	private static void transformMapRootRecursive() {
		MethodDecl mdec = MethodInfo.getRecursiveMdec();
		ClassDecl cdec = MethodInfo.getRecursiveCdec();

		Block mapBody = new Block();
		List<ParameterDeclaration> pdecs = new List<ParameterDeclaration>();
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopInvariantParamsIncludingNode()) {
			pdecs.add(pdec.fullCopy());
		}
		MethodDecl mapMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID() + Utils.methName_mapRootSuffix,
				pdecs,
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(mapBody));
		cdec.addMemberMethod(mapMdec);

		List<Expr> mapArgs = new List<Expr>();
		if (Globals.isImplicitRecursiveStructure || !Globals.isStaticRecursiveMethod) {
			mapArgs.add(new VarAccess("this"));
		}
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopInvariantParamsIncludingNode()) {
			mapArgs.add(new VarAccess(pdec.getID()));
		}
		MethodAccess mapMacc = new MethodAccess(Utils.methName_Block_mapRoot, mapArgs);
		mapBody.addStmt(new ExprStmt(new AbstractDot(
				new VarAccess(Utils.typeName_Block),
				mapMacc)));
	}

	private static void transformMapRootIntermediary(MethodInfo minfo, boolean last) {
		MethodDecl mdec = minfo.mdec;
		MethodAccess macc = minfo.macc;
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;		
		Block mapBody = new Block();
		List<ParameterDeclaration> pdecs = new List<ParameterDeclaration>();
		for (ParameterDeclaration pdec : minfo.loopInvariantParams) {
			pdecs.add(pdec);
		}
		MethodDecl mapMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID() + Utils.methName_mapRootSuffix,
				pdecs,
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(mapBody));
		cdec.addMemberMethod(mapMdec);

		Expr maccLeft = Utils.getDotLeft(macc);

		// propagate loop variant args
		Set<VarAccess> loopVariantArgsPropagation = new HashSet<VarAccess>();
		for (int i : minfo.loopVariantParamIdxs) {
			loopVariantArgsPropagation.add(new VarAccess(mdec.getParameter(i).getID()));
		}

		Stmt maccStmt = Utils.getEnclosingStmt(macc);	
		Stmt mapMaccStmt = null;
		for (Stmt stmt : mdec.getBlock().getStmts()) {
			if (stmt == maccStmt) {
				if (stmt instanceof ExprStmt) {
					ExprStmt estmt = (ExprStmt) maccStmt;
					Expr expr = estmt.getExpr();
					if (expr instanceof AssignExpr) {
						// ret = scene.intersectRay_MapTree(_spliceSet); -> remove ret
						AssignExpr aexpr = (AssignExpr) expr;
						loopVariantArgsPropagation.add((VarAccess) aexpr.getDest());
					}
				}
				List<Expr> mapArgs = new List<Expr>();
				if (last) {
					for (int i = 0; i < macc.getNumArg(); i++) {
						if (MethodInfo.getRecursiveMethodAllLoopVariantParamsIdxs().contains(i)) continue;
						mapArgs.add(macc.getArg(i));
					}					
				} else {
					for (int i = 0; i < macc.getNumArg(); i++) {
						if (minfo.loopVariantArgIdxs.contains(i)) continue;
						mapArgs.add(macc.getArg(i));
					}
				}
				MethodAccess mapMacc = new MethodAccess(macc.getID() + Utils.methName_mapRootSuffix, mapArgs);
				Expr mapMaccExpr = maccLeft == null ? mapMacc : new AbstractDot((Expr) maccLeft.fullCopy(), mapMacc);
				mapBody.addStmt(new ExprStmt(mapMaccExpr));
			}
			mapBody.addStmt((Stmt) stmt.fullCopy());
		}

		ArrayList<Stmt> recursionPointStmts = new ArrayList<Stmt>();
		ArrayList<Stmt> toRemoveStmts = new ArrayList<Stmt>();
		recursionPointStmts.add(mapMaccStmt);
		Utils.removeLoopVariantStmts(loopVariantArgsPropagation, mapBody, recursionPointStmts, toRemoveStmts);
		for (Stmt stmt : toRemoveStmts) {
			stmt.removeSelf();
		}

		/*
		 * remove return values
		 */
		ArrayList<ReturnStmt> returns = new ArrayList<ReturnStmt>();
		mapMdec.gatherReturns(returns);
		for (ReturnStmt rstmt : returns) {
			rstmt.swapStmtWith(new ReturnStmt());
		}
	}

	private static void addBlockClassImports(CompilationUnit cu) {
		Utils.addImport(cu, Globals.blockPackageName + "." + Utils.typeName_Block);
		if (Globals.usePointClass) {
			Utils.addImport(cu, Globals.blockPackageName + "." + Utils.typeName_Point);
		}
	}

	private static void transformEntry() {
		MethodAccess macc = MethodInfo.getEntryMacc();
		Expr maccLeft = Utils.getDotLeft(macc);
		Stmt maccStmt = Utils.getEnclosingStmt(macc);

		Block entryBody = new Block();

		/*
		 * divide block into prologue and epilogue
		 */
		ArrayList<Stmt> prologue = new ArrayList<Stmt>();
		ArrayList<Stmt> epilogue = new ArrayList<Stmt>();
		ArrayList<VariableDeclaration> prologueVarDecs = new ArrayList<VariableDeclaration>();
		ArrayList<VariableDeclaration> loopVariantArgDecs = new ArrayList<VariableDeclaration>();
		boolean nextEntryFound = false;
		for (Stmt stmt : ((Block) Globals.outerFor.getStmt()).getStmts()) {
			boolean nextEntryFoundNow = false;
			if (stmt == maccStmt) {
				nextEntryFoundNow = true;	
				nextEntryFound = true;
			}
			if (!nextEntryFoundNow) {
				if (!nextEntryFound) {
					if (stmt instanceof VariableDeclaration) {
						VariableDeclaration vdec = (VariableDeclaration) stmt;
						boolean isLoopVariantArg = false;
						for (int j : MethodInfo.getOuterForloopVariantArgIdxs()) {
							if (vdec.getID().equals(macc.getArg(j).toString())) {
								isLoopVariantArg = true;
								break;
							}
						}
						if (isLoopVariantArg) {
							loopVariantArgDecs.add(vdec);
						} else {
							prologueVarDecs.add(vdec);
							prologue.add(stmt);
						}
					} else {
						prologue.add(stmt);
					}
				} else {
					epilogue.add(stmt);
				}
			}
		}

		/*
		 * all local variables declared in prologue and used in epilogue need to be saved in IntermediaryState
		 */
		ArrayList<VariableDeclaration> usedInEpilogueVarDecs = new ArrayList<VariableDeclaration>();
		for (VariableDeclaration vdec : prologueVarDecs) {
			for (Stmt stmt : epilogue) {
				if (stmt.findVarUse(vdec.getID())) {
					usedInEpilogueVarDecs.add(vdec);
					break;
				}
			}
		}

		/*
		 * get info from outerFor
		 */
		assert Globals.outerFor.getNumInitStmt() == 1;	// assuming single init stmt
		VariableDeclaration outerForInitStmt = (VariableDeclaration) Globals.outerFor.getInitStmt(0);
		String indexName = outerForInitStmt.getID();
		LTExpr cond = (LTExpr) Globals.outerFor.getCondition();
		assert cond.getLeftOperand().toString().equals(outerForInitStmt.getID());	// assuming init and cond use same index
		Expr condRight = cond.getRightOperand();
		// also assuming int type, and increment by one

		Block blockPath = new Block();

		/*
		 * ComputeForce_MapRoot(root, diameter, itolsq, s, dthf, epssq);
		 */
		List<Expr> mapRootArgs = new List<Expr>();
		for (Expr arg : MethodInfo.getOuterForLoopInvariantArgs()) {
			mapRootArgs.add((Expr) arg.fullCopy());
		}
		MethodAccess mapRootMacc = new MethodAccess(macc.getID() + Utils.methName_mapRootSuffix, mapRootArgs);
		Expr mapRootExpr = maccLeft == null ? mapRootMacc : new AbstractDot((Expr) maccLeft.fullCopy(), mapRootMacc);
		entryBody.addStmt(new ExprStmt(mapRootExpr));

		if (Globals.useAutotune) {
			entryBody.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_Autotuner), Utils.varName_Autotuner,
					new ClassInstanceExpr(new TypeAccess(Utils.typeName_Autotuner), new List().add(condRight.fullCopy()))));			
		}

		addTuneBlockAndInterDec(entryBody);

		if (Globals.useAutotune) {
			transformAutotuneEntry(entryBody, macc, maccStmt, prologue, epilogue, indexName, loopVariantArgDecs, usedInEpilogueVarDecs, maccLeft);
		}

		/*
		 * _IntermediaryState _interState = new _IntermediaryState();
		 * _Block _block = new _Block();
		 * _interState.block = _block;
		 */
		blockPath.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_InterState,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_IntermediaryState), new List())));
		blockPath.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_Block), Utils.varName_Block,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_Block), new List())));
		blockPath.addStmt(new ExprStmt(new AssignSimpleExpr(
				new AbstractDot(new VarAccess(Utils.varName_InterState), new VarAccess("block")),
				new VarAccess(Utils.varName_Block))));

		ForStmt blockIncrementFor = new ForStmt();
		blockPath.addStmt(blockIncrementFor);

		blockIncrementFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_start", outerForInitStmt.getInit()));
		blockIncrementFor.setCondition(new LTExpr(new VarAccess("_start"), (Expr) condRight.fullCopy()));
		blockIncrementFor.addUpdateStmt(new ExprStmt(new AssignPlusExpr(new VarAccess("_start"), getMaxBlockSizeExpr())));
		Block blockIncrementForBody = new Block();
		blockIncrementFor.setStmt(blockIncrementForBody);

		blockIncrementForBody.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_end",
				new ConditionalExpr(new LTExpr(new AddExpr(new VarAccess("_start"), getMaxBlockSizeExpr()),
						(Expr) cond.getRightOperand().fullCopy()),
						new AddExpr(new VarAccess("_start"), getMaxBlockSizeExpr()), (Expr) cond.getRightOperand().fullCopy())));

		blockIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate prologue body
		 */
		ForStmt prologueFor = new ForStmt();
		prologueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName, new VarAccess("_start")));
		prologueFor.setCondition(new LTExpr(new VarAccess(indexName), new VarAccess("_end")));
		prologueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess(indexName))));
		Block prologueBody = new Block();
		prologueFor.setStmt(prologueBody);
		if (Globals.useAutotune) {
			prologueBody.addStmt(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			prologueBody.addStmt(vdec.fullCopy());
		}
		for (Stmt stmt : prologue) {
			prologueBody.addStmt((Stmt) stmt.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.saveInterState(vdec, "entry");
			MethodAccess saveMacc = new MethodAccess(methodName, new List<Expr>());
			saveMacc.addArg(new VarAccess(vdec.getID()));
			prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState), saveMacc)));
		}
		MethodAccess prologueMacc = macc.fullCopy();
		prologueMacc.setID(prologueMacc.getID() + Utils.methName_prologueSuffix);
		prologueMacc.addArg(new VarAccess(Utils.varName_InterState));
		Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
		prologueBody.addStmt(new ExprStmt(prologueMaccExpr));
		prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		blockIncrementForBody.addStmt(prologueFor);

		/*
		 * _block.flushBlock();
		 * _interState.resetIndex();
		 */
		blockIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Block),
				new MethodAccess(Utils.methName_Block_flushBlock, new List()))));
		blockIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate epilogue method
		 */
		ForStmt epilogueFor = new ForStmt();
		epilogueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName, new VarAccess("_start")));
		epilogueFor.setCondition(new LTExpr(new VarAccess(indexName), new VarAccess("_end")));
		epilogueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess(indexName))));
		Block epilogueBody = new Block();
		epilogueFor.setStmt(epilogueBody);
		if (Globals.useAutotune) {
			epilogueBody.addStmt(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			epilogueBody.addStmt(vdec.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.getLoadInterStateMethodName(vdec, "entry");
			MethodAccess loadMacc = new MethodAccess(methodName, new List<Expr>());
			VariableDeclaration loadVdec = new VariableDeclaration((Access) vdec.getTypeAccess().fullCopy(), vdec.getID(),
					new AbstractDot(new VarAccess(Utils.varName_InterState), loadMacc));
			epilogueBody.addStmt(loadVdec);
		}
		MethodAccess epilogueMacc = macc.fullCopy();
		epilogueMacc.setID(epilogueMacc.getID() + Utils.methName_epilogueSuffix);
		epilogueMacc.addArg(new VarAccess(Utils.varName_InterState));
		Stmt epilogueMaccStmt = (Stmt) maccStmt.fullCopy();
		epilogueMaccStmt.findMethodAccess(macc.getID()).swapWith(epilogueMacc);
		epilogueBody.addStmt(epilogueMaccStmt);
		for (Stmt stmt : epilogue) {
			epilogueBody.addStmt((Stmt) stmt.fullCopy());
		}
		epilogueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		blockIncrementForBody.addStmt(epilogueFor);

		CompilationUnit cu = Utils.getEnclosingCompilationUnit(macc);
		addBlockClassImports(cu);

		ForStmt basePath = Globals.outerFor.fullCopy();
		if (Globals.useAutotune) {
			Block basePathBody = (Block) basePath.getStmt();
			basePathBody.getStmt(0).insertStmtBefore(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}

		entryBody.addStmt(new IfStmt(new LogNotExpr(new AbstractDot(new VarAccess(Utils.typeName_Block), 
				new MethodAccess(Utils.methName_Block_useBlock, new List()))),
				basePath, blockPath));
		Globals.outerFor.swapStmtWith(entryBody);
	}

	public static void transformAutotuneEntry(Block entryBody, MethodAccess macc, Stmt maccStmt, ArrayList<Stmt> prologue, ArrayList<Stmt> epilogue,
			String indexName, ArrayList<VariableDeclaration> loopVariantArgDecs, ArrayList<VariableDeclaration> usedInEpilogueVarDecs,
			Expr maccLeft) {
		entryBody.addStmt(new VariableDeclaration(
				new ArrayTypeAccess(new ArrayTypeAccess(new PrimitiveTypeAccess("int"))),
				"_tuneIndexes",
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tune", new List()))));
		ForStmt tuneOuterFor = new ForStmt();
		entryBody.addStmt(tuneOuterFor);

		tuneOuterFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_t", new IntegerLiteral(0)));
		tuneOuterFor.setCondition(new LTExpr(new VarAccess("_t"),
				new AbstractDot(new VarAccess("_tuneIndexes"), new VarAccess("length"))));
		tuneOuterFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess("_t"))));
		Block tuneOuterForBody = new Block();
		tuneOuterFor.setStmt(tuneOuterForBody);

		tuneOuterForBody.addStmt(new VariableDeclaration(
				new ArrayTypeAccess(new PrimitiveTypeAccess("int")),
				"_indexes",
				new AbstractDot(new VarAccess("_tuneIndexes"), new ArrayAccess(new VarAccess("_t")))));
		if (Globals.useSplice) {
			tuneOuterForBody.addStmt(new ExprStmt(
					new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneEntrySplice", new List()))));			
		}
		tuneOuterForBody.addStmt(new ExprStmt(
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneEntryBlock", new List()))));

		Block tuneBasePath = new Block();
		Block tuneBlockPath = new Block();
		IfStmt ifstmt = new IfStmt(new EQExpr(new AbstractDot(new VarAccess("_indexes"), new VarAccess("length")), new IntegerLiteral(1)),
				tuneBasePath, tuneBlockPath);
		tuneOuterForBody.addStmt(ifstmt);

		tuneBasePath.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName,
				new AbstractDot(new VarAccess("_indexes"), new ArrayAccess(new IntegerLiteral(0)))));
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			tuneBasePath.addStmt(vdec.fullCopy());
		}
		for (Stmt stmt : prologue) {
			tuneBasePath.addStmt((Stmt) stmt.fullCopy());
		}
		MethodAccess baseTuneMacc = macc.fullCopy();
		baseTuneMacc.setID(baseTuneMacc.getID() + Utils.methName_autotuneSuffix);
		baseTuneMacc.addArg(new VarAccess(Utils.varName_Autotuner));
		Stmt baseTuneMaccStmt = (Stmt) maccStmt.fullCopy();
		baseTuneMaccStmt.findMethodAccess(macc.getID()).swapWith(baseTuneMacc);
		tuneBasePath.addStmt(baseTuneMaccStmt);
		for (Stmt stmt : epilogue) {
			tuneBasePath.addStmt((Stmt) stmt.fullCopy());
		}

		tuneBlockPath.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneInterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate prologue body
		 */
		ForStmt prologueFor = new ForStmt();
		prologueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_tt", new IntegerLiteral(0)));
		prologueFor.setCondition(new LTExpr(new VarAccess("_tt"), new AbstractDot(new VarAccess("_indexes"), new VarAccess("length"))));
		prologueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess("_tt"))));
		Block prologueBody = new Block();
		prologueFor.setStmt(prologueBody);
		prologueBody.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName,
				new AbstractDot(new VarAccess("_indexes"), new ArrayAccess(new VarAccess("_tt")))));
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			prologueBody.addStmt(vdec.fullCopy());
		}
		for (Stmt stmt : prologue) {
			prologueBody.addStmt((Stmt) stmt.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.saveInterState(vdec, "entry");
			MethodAccess saveMacc = new MethodAccess(methodName, new List<Expr>());
			saveMacc.addArg(new VarAccess(vdec.getID()));
			prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneInterState), saveMacc)));
		}
		MethodAccess prologueMacc = macc.fullCopy();
		prologueMacc.setID(prologueMacc.getID() + Utils.methName_prologueSuffix);
		prologueMacc.addArg(new VarAccess(Utils.varName_TuneInterState));		
		Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
		prologueBody.addStmt(new ExprStmt(prologueMaccExpr));		
		prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneInterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		tuneBlockPath.addStmt(prologueFor);

		/*
		 * _block.flushBlockAutotune(_autotuner);
		 * _interState.resetIndex();
		 */
		tuneBlockPath.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneBlock),
				new MethodAccess(Utils.methName_Block_flushBlockAutotune, new List().add(new VarAccess(Utils.varName_Autotuner))))));
		tuneBlockPath.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneInterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate epilogue method
		 */
		ForStmt epilogueFor = new ForStmt();
		epilogueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_tt", new IntegerLiteral(0)));
		epilogueFor.setCondition(new LTExpr(new VarAccess("_tt"), new AbstractDot(new VarAccess("_indexes"), new VarAccess("length"))));
		epilogueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess("_tt"))));
		Block epilogueBody = new Block();
		epilogueFor.setStmt(epilogueBody);
		epilogueBody.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName,
				new AbstractDot(new VarAccess("_indexes"), new ArrayAccess(new VarAccess("_tt")))));
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			epilogueBody.addStmt(vdec.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.getLoadInterStateMethodName(vdec, "entry");
			MethodAccess loadMacc = new MethodAccess(methodName, new List<Expr>());
			VariableDeclaration loadVdec = new VariableDeclaration((Access) vdec.getTypeAccess().fullCopy(), vdec.getID(),
					new AbstractDot(new VarAccess(Utils.varName_TuneInterState), loadMacc));
			epilogueBody.addStmt(loadVdec);
		}
		MethodAccess epilogueMacc = macc.fullCopy();
		epilogueMacc.setID(epilogueMacc.getID() + Utils.methName_epilogueSuffix);
		epilogueMacc.addArg(new VarAccess(Utils.varName_TuneInterState));
		Stmt epilogueMaccStmt = (Stmt) maccStmt.fullCopy();
		epilogueMaccStmt.findMethodAccess(macc.getID()).swapWith(epilogueMacc);
		epilogueBody.addStmt(epilogueMaccStmt);
		for (Stmt stmt : epilogue) {
			epilogueBody.addStmt((Stmt) stmt.fullCopy());
		}
		epilogueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_TuneInterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		tuneBlockPath.addStmt(epilogueFor);

		/*
		 * _autotuner.tuneExitBlock(_t);
		 */
		tuneOuterForBody.addStmt(new ExprStmt(
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneExitBlock", new List().add(new VarAccess("_t"))))));
		if (Globals.useSplice) {
			tuneOuterForBody.addStmt(new ExprStmt(
					new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneExitSplice", new List()))));			
		}

		/*
		 * _autotuner.tuneFinished();
		 */
		entryBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
				new MethodAccess(Utils.methName_Autotuner_tuneFinished, new List()))));
	}

	private static void transformAutotuneBlockRecursive(MethodDecl blockMdec) {
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(blockMdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;
		MethodDecl tuneMdec = blockMdec.fullCopy();
		cdec.addMemberMethod(tuneMdec);

		tuneMdec.setID(blockMdec.getID() + Utils.methName_autotuneSuffix);
		tuneMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_Autotuner), Utils.varName_Autotuner));
		ArrayList<MethodAccess> maccs = new ArrayList<MethodAccess>(); 
		tuneMdec.gatherMethodAccesses(maccs);
		for (MethodAccess macc : maccs) {
			if (!macc.getID().equals(blockMdec.getID())) continue;
			macc.setID(blockMdec.getID() + Utils.methName_autotuneSuffix);
			macc.addArg(new VarAccess(Utils.varName_Autotuner));
		}

		Stmt recycleStmt = Utils.getEnclosingStmt(tuneMdec.findMethodAccess(Utils.methName_Block_recycle));
		recycleStmt.insertStmtBefore(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
				new MethodAccess(Utils.methName_Block_profileWorkDone, new List<Expr>().add(
						new AbstractDot(new VarAccess(Utils.varName_Block), new VarAccess(Utils.varName_Block_Size)))))));

		if (Globals.useSplice) {
			Block body = tuneMdec.getBlock();
			/*
			 * add boolean _pointStopped = true;
			 */
			body.addStmtStart(new VariableDeclaration(new PrimitiveTypeAccess("boolean"), Utils.varName_PointStopped,
					new BooleanLiteral(true)));

			/*
			 * add if (_pointStopped) _SpliceSet.profileDepth(_depth); at end
			 */
			List<Expr> profileDepthArgs = new List<Expr>();
			profileDepthArgs.add(new VarAccess(Utils.varName_Depth));
			IfStmt ifPointStoppedProfileDepth = new IfStmt(new VarAccess(Utils.varName_PointStopped),
					new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
							new MethodAccess(Utils.methName_SpliceSet_profileDepth, profileDepthArgs))));
			body.addStmtEnd(ifPointStoppedProfileDepth);

			/*
			 * add if (_pointStopped) _SpliceSet.profileDepth(_depth); before returns
			 */
			ArrayList<ContinueStmt> continues = new ArrayList<ContinueStmt>();
			body.gatherContinues(continues);
			for (ContinueStmt stmt : continues) {
				stmt.insertStmtBefore(new IfStmt(new VarAccess(Utils.varName_PointStopped),
						new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
								new MethodAccess(Utils.methName_SpliceSet_profileDepth, profileDepthArgs.fullCopy()))))); // fullCopy because used previously
			}

			ArrayList<ParameterDeclaration> loopVariantPdecs = MethodInfo.getRecursiveMethodLoopVariantParams();
			ArrayList<RecursiveCallSet> rcsSets = RecursiveCallSet.analysis(tuneMdec, loopVariantPdecs);
			for (RecursiveCallSet rcs : rcsSets) {
				Stmt last = Utils.getEnclosingStmt(rcs.maccs.get(rcs.maccs.size() - 1));
				last.insertStmtAfter(new ExprStmt(new AssignSimpleExpr(new VarAccess(Utils.varName_PointStopped), new BooleanLiteral(false))));
			}

			List<Expr> profileConvergenceArgs = new List<Expr>();
			profileConvergenceArgs.add(new VarAccess(MethodInfo.getRecursiveMethodLoopVariantParams().get(0).getID()));
			if (Globals.isImplicitRecursiveStructure) {
				profileConvergenceArgs.add(new VarAccess("this"));	
			} else {
				profileConvergenceArgs.add(new VarAccess(Globals.nodeParam.getID()));
			}
			AbstractDot profileConvergence = new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_SpliceSet_profileConvergence, profileConvergenceArgs));
			ForStmt blockFor = null;
			for (Stmt stmt : body.getStmts()) {
				if (stmt instanceof ForStmt) {
					blockFor = (ForStmt) stmt;
					break;
				}
			}
			Block blockForBody = (Block) blockFor.getStmt(); 
			VariableDeclaration vdec = null;
			for (Stmt stmt : blockForBody.getStmts()) {
				if (stmt instanceof VariableDeclaration) {
					vdec = (VariableDeclaration) stmt;
				} else {
					break;
				}
			}			
			vdec.insertStmtAfter(new ExprStmt(profileConvergence));
		}
	}

	private static MethodDecl transformBlockRecursive() {
		MethodDecl mdec = MethodInfo.getRecursiveMdec();
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;

		Block body = new Block();
		List<ParameterDeclaration> blockMdecPdecs = new List<ParameterDeclaration>();
		ArrayList<ParameterDeclaration> loopVariantPdecs = MethodInfo.getRecursiveMethodLoopVariantParams();
		MethodDecl blockMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID(),
				blockMdecPdecs,
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(body));
		cdec.addMemberMethod(blockMdec);

		/*
		 * BlockSet bset = stack.get(depth);	
		 */
		List<Expr> args = new List<Expr>();
		args.add(new VarAccess(Utils.varName_Depth));		
		AbstractDot dot = new AbstractDot(new VarAccess(Utils.varName_BlockStack), new MethodAccess(Utils.methName_BlockStack_get, args));
		VariableDeclaration vdec = new VariableDeclaration(new TypeAccess(Utils.typeName_BlockSet), Utils.varName_BlockSet, dot);
		body.addStmt(vdec);

		/*
		 * Block block = bset.block;
		 */
		dot = new AbstractDot(new VarAccess(Utils.varName_BlockSet), new VarAccess(Utils.varName_BlockSet_Block));
		VariableDeclaration blockDec = new VariableDeclaration(new TypeAccess(Utils.typeName_Block), Utils.varName_Block, dot);
		body.addStmt(blockDec);

		if (Globals.useUtilProfiler) {
			AbstractDot getLauncherDot = new AbstractDot(new VarAccess(Utils.typeName_Launcher), new MethodAccess(Utils.methodName_Launcher_getProfiler, new List()));
			List<Expr> effectiveBlockArgs = new List<Expr>();
			effectiveBlockArgs.add(new AbstractDot(new VarAccess(Utils.varName_Block), new VarAccess(Utils.varName_Block_Size)));
			AbstractDot effectiveBlockDot = new AbstractDot(getLauncherDot, new MethodAccess(Utils.methodName_Profiler_effectiveBlock, effectiveBlockArgs));
			body.addStmt(new ExprStmt(effectiveBlockDot));			
			Utils.addImport(Utils.getEnclosingCompilationUnit(mdec), "util.Launcher");
			Utils.addImport(Utils.getEnclosingCompilationUnit(mdec), "util.BlockProfiler");
		}

		/*
		 * for(int _blockIndex = 0; _blockIndex < _block.size; _blockIndex++)
		 */
		VariableDeclaration init1 = new VariableDeclaration(new PrimitiveTypeAccess("int"),
				Utils.varName_BlockIndex, new IntegerLiteral("0"));
		List<Stmt> initList = new List<Stmt>();
		initList.add(init1);
		LTExpr cond = new LTExpr(new VarAccess(Utils.varName_BlockIndex),
				new AbstractDot(new VarAccess(Utils.varName_Block), new VarAccess(Utils.varName_Block_Size)));
		Opt<Expr> condOpt = new Opt<Expr>(cond);
		PostIncExpr step = new PostIncExpr(new VarAccess(Utils.varName_BlockIndex));
		List<Stmt> stepList = new List<Stmt>();
		stepList.add(new ExprStmt(step));
		Block block = new Block();
		ForStmt forstmt = new ForStmt(initList, condOpt, stepList, block);

		body.addStmt(forstmt);
		Block forBlock = (Block) forstmt.getStmt();
		Stmt pointDecRef = Utils.addPointDecs(forBlock, loopVariantPdecs);
		if (Globals.useUtilProfiler) {
			List<Expr> touchArgs = new List<Expr>();
			touchArgs.add(new VarAccess(loopVariantPdecs.get(0).getID()));
			if (Globals.isImplicitRecursiveStructure) {
				touchArgs.add(new VarAccess("this"));	
			} else {
				touchArgs.add(new VarAccess(Globals.nodeParam.getID()));
			}
			AbstractDot getLauncherDot = new AbstractDot(new VarAccess(Utils.typeName_Launcher), new MethodAccess(Utils.methodName_Launcher_getProfiler, new List()));
			AbstractDot touchDot = new AbstractDot(getLauncherDot, new MethodAccess(Utils.methodName_Profiler_touch, touchArgs));
			forBlock.addStmt(new ExprStmt(touchDot));
		}

		/*
		 * copy entire method
		 */
		for (Stmt stmt : mdec.getBlock().getStmts()) {
			forBlock.addStmt((Stmt) stmt.fullCopy());
		}	

		/*
		 * setup recursive call sets
		 */
		RecursiveCallSet.setBlockSets(RecursiveCallSet.analysis(blockMdec, loopVariantPdecs));

		/*
		 * mark pdecs which vary per call 
		 * this assumes that maccs have same form as mdec, so no morphing should occur before this
		 */
		MethodInfo.setupPdecNameToCallSetNumMap(mdec, RecursiveCallSet.getBlockSets());
		HashMap<String, Integer> pdecNameToCallSetNumMap = MethodInfo.getPdecNameToCallSetNumMap();

		/*
		 * add point decs for additional loop invariant params
		 */
		ArrayList<ParameterDeclaration> additionalLoopVariantPdecs = MethodInfo.getRecursiveMethodAdditionalLoopVariantParams();
		ArrayList<Integer> additionalLoopVariantPdecIdxs = MethodInfo.getRecursiveMethodAdditionalLoopVariantParamsIdxs();
		for (ParameterDeclaration pdec : additionalLoopVariantPdecs) {
			Utils.addPointDec(pointDecRef, pdec, pdecNameToCallSetNumMap.get(pdec.getID()));
		}

		/*
		 *  replace largest possible block with _nextBlock._add(p)
		 */
		for (RecursiveCallSet rcs : RecursiveCallSet.getBlockSets()) {
			MethodAccess blockAddMacc = new MethodAccess(Utils.methName_Block_add, new List<Expr>());
			for (ParameterDeclaration pdec : loopVariantPdecs) {
				blockAddMacc.addArg(new VarAccess(pdec.getID()));
			}
			for (ParameterDeclaration pdec : additionalLoopVariantPdecs) {
				if (pdecNameToCallSetNumMap.get(pdec.getID()) != null) {
					for (int i = 0; i < mdec.getNumParameter(); i++) {
						if (pdec.getID().equals(mdec.getParameter(i).getID())) {
							for (int j = 0; j < rcs.maccs.size(); j++) {
								blockAddMacc.addArg(rcs.maccs.get(j).getArg(i));
							}
						}
					}
				}
			}
			dot = new AbstractDot(new VarAccess(Utils.varName_NextBlock + rcs.id), blockAddMacc);
			Block addToNextBlockBody = new Block();
			addToNextBlockBody.addStmt(new ExprStmt(dot));
			rcs.stmt.swapStmtWith(addToNextBlockBody);	
			for (Stmt stmt : rcs.identicalStmts) {
				stmt.swapStmtWith(addToNextBlockBody.fullCopy());
			}
			Block nextBlockNotEmptyCode = new Block();
			nextBlockNotEmptyCode.addStmt(rcs.stmt);

			/*
			 * stack.get(depth + 1).block = nextBlock;
			 */
			args = new List<Expr>();
			args.add(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));		
			dot = new AbstractDot(new VarAccess(Utils.varName_BlockStack), new MethodAccess(Utils.methName_BlockStack_get, args));
			dot = new AbstractDot(dot, new VarAccess(Utils.varName_BlockSet_Block));
			AssignExpr aexpr = new AssignSimpleExpr(dot, new VarAccess(Utils.varName_NextBlock + rcs.id));
			nextBlockNotEmptyCode.addStmtStart(new ExprStmt(aexpr));

			/*
			 * if (!nextBlock.isEmpty()) {
			 *	//...
			 */
			dot = new AbstractDot(new VarAccess(Utils.varName_NextBlock + rcs.id), new MethodAccess(Utils.methName_Block_isEmpty, new List()));
			IfStmt nextBlockNotEmptyIf = new IfStmt(new LogNotExpr(dot),
					nextBlockNotEmptyCode);
			body.addStmt(nextBlockNotEmptyIf);

			/*
			 * Block nextBlock = bset.nextBlock;
			 * nextBlock.recycle();
			 */
			dot = new AbstractDot(new VarAccess(Utils.varName_NextBlock + rcs.id), new MethodAccess(Utils.methName_Block_recycle, new List()));
			blockDec.insertStmtAfter(new ExprStmt(dot));

			dot = new AbstractDot(new VarAccess(Utils.varName_BlockSet), new VarAccess(Utils.varName_BlockSet_NextBlock + rcs.id));
			vdec = new VariableDeclaration(new TypeAccess(Utils.typeName_Block),
					Utils.varName_NextBlock + rcs.id, dot);
			blockDec.insertStmtAfter(vdec);
		}	

		/*
		 * morph recursionPoints at the end
		 */
		ArrayList<Integer> loopVariantParamIdxs = MethodInfo.getRecursiveMethodLoopVariantParamIdxs();
		for (RecursiveCallSet rcs : RecursiveCallSet.getBlockSets()) {
			for (int j = 0; j < rcs.maccs.size(); j++) {
				MethodAccess macc = rcs.maccs.get(j);
				List<Expr> newArgs = new List<Expr>();
				for (int i = 0; i < macc.getNumArg(); i++) {
					if (loopVariantParamIdxs.contains(i) || additionalLoopVariantPdecIdxs.contains(i)) continue;
					newArgs.add(macc.getArg(i));
				}
				newArgs.add(new VarAccess(Utils.varName_BlockStack));
				newArgs.add(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
				if (!pdecNameToCallSetNumMap.isEmpty()) {
					newArgs.add(new IntegerLiteral(j));
				}
				macc.setArgList(newArgs);
			}
		}

		/*
		 * set params at the end 
		 */
		for (int i = 0; i < mdec.getNumParameter(); i++) {
			if (loopVariantParamIdxs.contains(i) || additionalLoopVariantPdecIdxs.contains(i)) continue;
			blockMdecPdecs.add(mdec.getParameter(i));
		}
		blockMdecPdecs.add(new ParameterDeclaration(new TypeAccess(Utils.typeName_BlockStack), Utils.varName_BlockStack));
		blockMdecPdecs.add(new ParameterDeclaration(new TypeAccess("int"), Utils.varName_Depth));
		if (!pdecNameToCallSetNumMap.isEmpty()) {
			blockMdec.addParameter(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_CallIndex));
		}

		/*
		 * entire function body will be enclosed in block for
		 * change all returns to continues
		 */
		ArrayList<ReturnStmt> returns = new ArrayList<ReturnStmt>();
		body.gatherReturns(returns);
		for (ReturnStmt rstmt : returns) {
			assert !rstmt.hasResult();
			rstmt.swapStmtWith(new ContinueStmt());
		}
		return blockMdec;
	}

	public static void addTuneBlockAndInterDec(Block entryBody) {
		/*
		 * _Block _tuneBlock = new _Block();
		 * _IntermediaryState _tuneInterState = new _IntermediaryState();
		 * _interState.block = _tuneBlock;
		 */
		entryBody.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_Block), Utils.varName_TuneBlock,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_Block), new List())));
		entryBody.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_TuneInterState,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_IntermediaryState), new List().add(getMaxBlockSizeExpr()))));
		entryBody.addStmt(new ExprStmt(new AssignSimpleExpr(
				new AbstractDot(new VarAccess(Utils.varName_TuneInterState), new VarAccess("block")),
				new VarAccess(Utils.varName_TuneBlock))));
	}

	public static Expr getMaxBlockSizeExpr() {
		return new AbstractDot(new VarAccess(Utils.typeName_Block), new VarAccess(Utils.varName_Block_MaxSize));
	}
}
