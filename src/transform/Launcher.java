/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;

import AST.*;

public class Launcher extends Frontend {
	private static long transformTime = 0;
	private static String inputDir = "tests";

	public static void usage() {
		System.out.println("java -cp transform.Launcher [options] <java file dirs>");
		System.out.println(" --noauto             : no autotuning for point blocking AND traversal splicing");
		System.out.println(" --noblock            : no point blocking");
		System.out.println(" --nosplice           : no traversal splicing");
		System.out.println(" --noelision          : no splice node elision");
		System.out.println(" --spliceopt <level>  : splicing optimization level [0-2, default:2]");
		System.out.println(" --blocksize <size>   : block size for --noauto");
		System.out.println(" --splicedepth <depth>: splice depth for --noauto");
		System.out.printf (" --indir <path>       : input base path [default: %s]\n", inputDir);
		System.out.printf (" --outdir <path>      : output base path [default: %s]\n", Globals.outputDir);
		System.out.println(" --help               : print help");
		System.out.println(" --utilstats          : record autotuned params in util.Launcher stats");
		System.out.println(" --utilprofiler       : use block profiler to record average traversal size, effective block size");
		System.exit(1);
	}

	public static void main(String args[]) {
		String [] dirList = null;
		String argstr = "";
		for (int i = 0; i < args.length; i++) {
			argstr += args[i] + " ";
		} 
		System.out.println("TreeSplicer args: " + argstr);
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.equals("--noauto")) {
				Globals.useAutotune = false;
			} else if (arg.equals("--noblock")) {
				Globals.useBlock = false;
			} else if (arg.equals("--nosplice")) {
				Globals.useSplice = false;
			} else if (arg.equals("--noelision")) {
				Globals.useElision = false;
			} else if (arg.equals("--spliceopt")) {
				Globals.maxOptLevel = Integer.parseInt(args[++i]);
			} else if (arg.equals("--blocksize")) {
				Globals.blockSizeParam = Integer.parseInt(args[++i]);
			} else if (arg.equals("--splicedepth")) {
				Globals.spliceDepthParam = Integer.parseInt(args[++i]);
			} else if (arg.equals("--indir")) {
				inputDir = args[++i];
			} else if (arg.equals("--outdir")) {
				Globals.outputDir = args[++i];
			} else if (arg.equals("--help")) {
				usage();
			} else if (arg.equals("--utilstats")) {
				Globals.useUtilStats = true;
			} else if (arg.equals("--utilprofiler")) {
				Globals.useUtilProfiler = true;
				Globals.useUtilStats = true;
			} else {
				dirList = Arrays.asList(args).subList(i, args.length).toArray(new String[args.length - i]);
				break;
			}
		}
		
		if (!Globals.useAutotune && !Globals.useUtilStats && Globals.blockSizeParam == 0) {
			System.out.println("Specify --blocksize for --noauto");
			usage();
		}
		
		String [] fileList = dirListToFileList(dirList);
		if (fileList.length == 0) {
			System.out.println("No input files found");
			usage();
		}

		long start = System.nanoTime();
		compile(fileList);
		long end = System.nanoTime();
		float msec = (float) (end - start) / 1000000;
		float msec2 = (float) transformTime / 1000000;
		System.out.printf("total time (ms): %.2f\n", msec);
		System.out.printf("analysis time (ms): %.2f\n\n\n", msec2);
	}

	private static String [] dirListToFileList(String [] list) {
		ArrayList<String> fileList = new ArrayList<String>();
		LinkedList<String> dirList = new LinkedList<String>();
		for (String dirName : list) {
			dirList.add(inputDir + "/" + dirName);
		}
		// recursively add all java files
		while (true) {
			String dirName = dirList.poll();
			if (dirName == null) break;
			createOutputDir(dirName);
			File dir = new File(dirName);
			File [] files = dir.listFiles();
			if (files == null) {
				System.out.printf("%s has no files\n", dirName);
				continue;
			}
			for (File file : files) {
				if (file.getName().endsWith(".java")) {
					fileList.add(file.getPath());
				}
				if (file.isDirectory()) {
					dirList.add(file.getPath());
				}
			}
		}
		return fileList.toArray(new String[fileList.size()]);
	}

	private static void createOutputDir(String dirName) {
		String [] split = dirName.split("/");
		dirName = Globals.outputDir;
		for (int i = 1; i < split.length; i++) {
			dirName += ("/" + split[i]);
		}
		deleteDir(new File(dirName));
		File file = new File(dirName); 
		if (!file.mkdir()) {
			System.out.printf("failed to create dir %s\n", file.getAbsolutePath());
			System.exit(1);
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i=0; i<children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// The directory is now empty so delete it
		return dir.delete();
	}

	public static boolean compile(String args[]) {
		boolean ret;
		ret = new Launcher().process(
				args,
				new BytecodeParser(),
				new JavaParser() {
					public CompilationUnit parse(java.io.InputStream is, String fileName) throws java.io.IOException, beaver.Parser.Exception {
						return new parser.JavaParser().parse(is, fileName);
					}
				}
		);
		return ret;
	}

	protected void processPre(CompilationUnit unit) {
		long transformStart = System.nanoTime();
		RecursiveStructureAnalysis.processPre(unit);
		transformTime += System.nanoTime() - transformStart;
	}

	protected void processMain(CompilationUnit unit) {
		long transformStart = System.nanoTime();
		RecursiveStructureAnalysis.process(unit);
		transformTime += System.nanoTime() - transformStart;
	}

	protected void process() {
		long transformStart = System.nanoTime();
		TransformMain.process();
		transformTime += System.nanoTime() - transformStart;
	}

	protected void processPost(CompilationUnit unit) {
		try {
			String relName = unit.relativeName();
			String [] split = relName.split("/");
			String outName = "";
			for (int i = 1; i < split.length; i++) {
				outName += split[i];
				if (i != split.length - 1) {
					outName += "/";
				}
			}
			PrintStream out = new PrintStream(new FileOutputStream(Globals.outputDir + "/" + outName, false));
			out.print(unit.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected String name() { return "Java1.4Frontend + Java1.5Extensions"; }
	protected String version() { return "R20071015"; }
}
