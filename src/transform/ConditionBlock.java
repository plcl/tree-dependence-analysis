package transform;

import java.util.ArrayList;

import AST.ASTNode;
import AST.Access;
import AST.Annotation;
import AST.ArithmeticExpr;
import AST.ArrayAccess;
import AST.ArrayDecl;
import AST.AssignExpr;
import AST.Block;
import AST.BodyDecl;
import AST.CastExpr;
import AST.ClassDecl;
import AST.ClassInstanceExpr;
import AST.CompilationUnit;
import AST.Dot;
import AST.EqualityExpr;
import AST.Expr;
import AST.ExprStmt;
import AST.FieldDeclaration;
import AST.ForStmt;
import AST.IfStmt;
import AST.List;
import AST.LogNotExpr;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Modifier;
import AST.Modifiers;
import AST.Opt;
import AST.ParExpr;
import AST.ParameterDeclaration;
import AST.ParameterDeclarationSubstituted;
import AST.PostfixExpr;
import AST.PreDecExpr;
import AST.PreIncExpr;
import AST.PrimitiveTypeAccess;
import AST.RelationalExpr;
import AST.SimpleSet;
import AST.Stmt;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;

public class ConditionBlock {
	ASTNode start;
	ArrayList<String> conditions;
	ArrayList<String> postconds;
	
	public ConditionBlock(ASTNode n) {
		conditions = new ArrayList<String>();
		postconds = new ArrayList<String>();
		start = n;
	}
	
	public void addCondition(String cond) {
		conditions.add(cond);
	}
	
	public void addPostCond(String cond) {
		postconds.add(cond);
	}
	
	public void setConditions(ArrayList<String> conds) {
		conditions = conds;
	}
	
	public ASTNode getStart() {
		return start;
	}
	
	public ArrayList<String> getConditions() {
		return conditions;
	}
}