/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.HashSet;
import java.util.Set;
import java.io.*;
import java.util.ArrayList;

import AST.ASTNode;
import AST.Access;
import AST.AddExpr;
import AST.AndLogicalExpr;
import AST.Annotation;
import AST.ArithmeticExpr;
import AST.ArrayAccess;
import AST.ArrayDecl;
import AST.AssignExpr;
import AST.Block;
import AST.BodyDecl;
import AST.BooleanLiteral;
import AST.CastExpr;
import AST.ClassDecl;
import AST.ClassInstanceExpr;
import AST.CompilationUnit;
import AST.DivExpr;
import AST.Dot;
import AST.DoubleLiteral;
import AST.EQExpr;
import AST.EqualityExpr;
import AST.Expr;
import AST.ExprStmt;
import AST.FieldDeclaration;
import AST.FloatingPointLiteral;
import AST.ForStmt;
import AST.GEExpr;
import AST.GTExpr;
import AST.IfStmt;
import AST.IntegerLiteral;
import AST.LEExpr;
import AST.LTExpr;
import AST.List;
import AST.Literal;
import AST.LogNotExpr;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.ModExpr;
import AST.Modifier;
import AST.Modifiers;
import AST.MulExpr;
import AST.NEExpr;
import AST.NullLiteral;
import AST.Opt;
import AST.OrLogicalExpr;
import AST.ParExpr;
import AST.ParameterDeclaration;
import AST.ParameterDeclarationSubstituted;
import AST.PostfixExpr;
import AST.PreDecExpr;
import AST.PreIncExpr;
import AST.PrimitiveTypeAccess;
import AST.RelationalExpr;
import AST.ReturnStmt;
import AST.SimpleSet;
import AST.Stmt;
import AST.SubExpr;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;

public class RecursiveStructureAnalysis {
	private static final boolean annotations = false;
	
	/**
	 * first gather access points and recursive fields
	 * @param cu
	 */
	public static void processPre(CompilationUnit cu) {
		cu.gatherAccessPoints();
		if (cu.getNumTypeDecl() != 1) {
			System.out.println(cu);
			for (TypeDecl tdec : cu.getTypeDeclList()) {
				System.out.println(tdec + "\n");
			}
			assert false;
		}
		TypeDecl tdec = cu.getTypeDecl(0);
		if (tdec instanceof ClassDecl) {
			ClassDecl cdec = (ClassDecl) cu.getTypeDecl(0);
			cdec.gatherRecursiveFields();
		}

		String relName = cu.relativeName();
		String [] split = relName.split("/");
		String packageName = "";
		for (int i = 1; i < split.length - 1; i++) {
			packageName += split[i];
			if (i != split.length - 2) {
				packageName += ".";  
			}
		}
		if (!packageName.equals("")) {
			Globals.packageNames.add(packageName);
		}
	}

	/**
	 * next look for recursive structures
	 * @param cu
	 */
	public static void process(CompilationUnit cu) {
		TypeDecl tdec = cu.getTypeDecl(0);
		if (tdec instanceof ClassDecl) {
			// System.out.println("processing " + cu.pathName());
			// for now, just find one recursive structure, look for only if not found yet
			if (Globals.mainPath.isEmpty()) {
				findRecursiveStructure((ClassDecl) cu.getTypeDecl(0));
				if (!Globals.mainPath.isEmpty()) {
					setBlockPaths(cu);
				}
			}
		}
	}
	
	private static void findRecursiveStructure(ClassDecl classDec) {	
		for (BodyDecl bdec : classDec.getBodyDeclList()) {
			if (bdec instanceof MethodDecl) {
				MethodDecl mdec = (MethodDecl) bdec;
				
				if (annotations) {
					if (mdec.getID().equals("traversal_transform")) {
						// System.out.printf("\t candidate recursive method: %s\n", mdec.getID());
						processReadWritesSplitting(mdec);
						// System.out.printf("\t %s was selected for transformation\n", mdec.getID());
						// reverse mainPath to match usage throughout rest of program					
						
						ArrayList<MethodAccess> temp = new ArrayList<MethodAccess>();
						for (MethodAccess macc : Globals.mainPath) {
							temp.add(macc);
						}
						Globals.mainPath.clear();
						for (int i = temp.size() - 1; i >= 0; i--) {
							Globals.mainPath.add(temp.get(i));
						}
					}
				}
				else {
					if (mdec.isRecursive()) {
						if (findRecursiveUse(mdec)) {
							processRecursiveStructure(mdec);
							Globals.isImplicitRecursiveStructure = false;
							break;
						} else if (mdec.getNumParameter() > 0) {
							if (classDec.isRecursive()){
								if (findRecursiveImplicitUse(mdec, classDec)) {
									processRecursiveStructure(mdec);
									Globals.isImplicitRecursiveStructure = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	private static boolean findRecursiveImplicitUse(MethodDecl mdec, ClassDecl cdec) {
		boolean ret = false;
		RecursiveField.clearFields();
		for (MethodAccess macc : mdec.accessPoints) {
			if (Utils.getEnclosingMethodDecl(macc) == mdec) {
				ASTNode node = macc.getParent();
				if (node instanceof Dot) {
					Dot dot = (Dot) node;
					Expr left = dot.getLeft();
					if (left instanceof ParExpr) {
						ParExpr pexpr = (ParExpr) left;
						left = pexpr.getExpr();
					}
					if (left instanceof CastExpr) {
						CastExpr cexpr = (CastExpr) left;
						left = cexpr.getExpr();
					}
					if (left instanceof VarAccess) {
						VarAccess lvacc = (VarAccess) left;
						PointsToAnalysis pointsTo = new PointsToAnalysis(mdec);
						if (pointsTo.pointsToMany(lvacc)) {
							return false;
						}
						TypeDecl ltdec = lvacc.type();
						if (ltdec == cdec) {
							for (FieldDeclaration fdec : cdec.recursiveFields) {
								if (fdec.getID().equals(lvacc.getID())) {
									ret = true;
									RecursiveField.addRecursiveField(fdec, cdec);
								}
							}
						} else {
							while (cdec.hasSuperclass()) {
								cdec = cdec.superclass();
								if (ltdec == cdec) {
									for (FieldDeclaration fdec : cdec.recursiveFields) {
										if (fdec.getID().equals(lvacc.getID())) {
											ret = true;
											RecursiveField.addRecursiveField(fdec, cdec);
										}
									}								
								}
							}
						}
					}
				}
			}
		}
		return ret;
	}

	private static boolean findRecursiveUse(MethodDecl mdec) {
		boolean ret = false;
		RecursiveField.clearFields();
		System.out.println("Method name: " + mdec.getID());
		if (mdec.getNumParameter() <= 1) {
			System.out.println("Falling through because not enough parameters.");
			return ret;
		}
		if (mdec.findRecursiveVarDef()) {
			System.out.println("Recursive structure is being modified.");
			// return ret;
		}

		for (MethodAccess macc : mdec.accessPoints) {
			if (Utils.getEnclosingMethodDecl(macc) == mdec) {
				// this is recursion point
				for (Expr arg : macc.getArgs()) {
					arg = Utils.getMainExpr(arg);
					/*
					 * if arg is local variable, get value it was initialized with
					 * cf) Node child = nn.children[treeIndex];
					 * to be general, need reaching def analysis
					 */
					if (arg instanceof VarAccess) {
						VarAccess vacc = (VarAccess) arg;
						PointsToAnalysis pointsTo = new PointsToAnalysis(mdec);
						Expr ptexpr = pointsTo.getFinalPointsTo(vacc);
						if (ptexpr == null) {
							System.out.println("A local variable points to null: " + vacc.getID());
							// return false;
						}
						arg = ptexpr;
					}

					if (arg instanceof Dot) {
						Dot dot = (Dot) arg;
						Expr left = dot.getLeft();
						TypeDecl ltdec = null;
						/*
						 * if left arg was casted, use casted type
						 */
						if (left instanceof ParExpr) {
							left = ((ParExpr) left).getExpr();
							if (left instanceof CastExpr) {
								CastExpr cexpr = (CastExpr) left;
								left = cexpr.getExpr();
								ltdec = cexpr.type();
							}
						}
						Expr right = dot.getRight();
						// get var part for arrays
						if (right instanceof Dot) {
							dot = (Dot) right;
							if (dot.getRight() instanceof ArrayAccess) {
								right = dot.getLeft();
							}
						}
						if (left instanceof VarAccess && right instanceof VarAccess) {
							VarAccess lvacc = (VarAccess) left;
							VarAccess rvacc = (VarAccess) right;
							/*
							 * if left arg was not casted, use original type
							 */
							if (ltdec == null) {
								ltdec = lvacc.type();
							}
							if (ltdec instanceof ArrayDecl) {
								ArrayDecl adec = (ArrayDecl) ltdec;
								ltdec = (TypeDecl) adec.getParent();
							}
							if (ltdec instanceof ClassDecl) {
								ClassDecl cdec = (ClassDecl) ltdec;
								for (FieldDeclaration fdec : cdec.recursiveFields) {
									if (fdec.getID().equals(rvacc.getID())) {
										ret = true;
										RecursiveField.addRecursiveField(fdec, cdec);
									}
								}
								while (cdec.hasSuperclass()) {
									cdec = cdec.superclass();
									for (FieldDeclaration fdec : cdec.recursiveFields) {
										if (fdec.getID().equals(rvacc.getID())) {
											ret = true;
											RecursiveField.addRecursiveField(fdec, cdec);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return ret;
	}

	private static void processRecursiveStructure(MethodDecl mdec) {
		System.out.printf("\t candidate recursive method: %s\n", mdec.getID());
		boolean found = false;
		boolean recursiveEntryFound = false;
		TypeAccess retTacc = (TypeAccess) mdec.getTypeAccess();
		if (!retTacc.getID().equals("void")) {
			System.out.printf("\t\t skipped because return type is not void [%s]\n", retTacc.getID());
			return;
		}
		if (!isPseudoTailRecursive(mdec)) {
			System.out.printf("\t\t skipped because not pseudo-tail recursive\n");
			return;			
		}
		for (MethodAccess macc : mdec.accessPoints) {
			if (Utils.getEnclosingMethodDecl(macc) != mdec) {
				// currently assuming one access point for the recursive entry
				assert !found;
				found = true;
				if (findEnclosingForLoop(macc)) {
					Globals.mainPath.add(macc);
					recursiveEntryFound = true;
				}
			}
		}
		//if (!recursiveEntryFound) {
		//	System.out.printf("\t\t skipped because enclosing parallel for loop not found\n");
		//	Globals.mainPath.clear();
		//} else {
			System.out.printf("\t\t selected for transformation\n");
			// reverse mainPath to match usage throughout rest of program
			
			processReadWritesSplitting(mdec);
			
			ArrayList<MethodAccess> temp = new ArrayList<MethodAccess>();
			for (MethodAccess macc : Globals.mainPath) {
				temp.add(macc);
			}
			Globals.mainPath.clear();
			for (int i = temp.size() - 1; i >= 0; i--) {
				Globals.mainPath.add(temp.get(i));
			}
		//}
	}
	
	private static void processReadWritesSplitting(MethodDecl mdec) {
		String curcond = "true";
		
		ArrayList<AccessNode> reads = new ArrayList<AccessNode>();
		ArrayList<AccessNode> writes = new ArrayList<AccessNode>();
		ArrayList<String> recursecond = new ArrayList<String>();
		ArrayList<String> conddecs = new ArrayList<String>();
		
		try {
            File file = new File("../out/", "ast.txt");
            file.getParentFile().mkdirs();
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            
            printAST("", true, mdec, output);
            
            output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
		
		
		
		for (int i = 0; i < mdec.getNumChild(); i++) {
			curcond = splitASTConditions(mdec.getChild(i), curcond, reads, writes, recursecond, conddecs);
		}
		
		try {
            File file = new File("../out/", "accesslist.txt");
            file.getParentFile().mkdirs();
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            
            output.write("\nList of reads:\n");
    		for (int i = 0; i < reads.size(); i++) {
    			output.write(reads.get(i).vartype + " " + reads.get(i).varname);
    			// output.write("\n\tCondition: " + reads.get(i).getCondition());
    			output.write("\n");
    		}
    		
    		output.write("\nList of writes:\n");
    		for (int i = 0; i < writes.size(); i++) {
    			output.write(writes.get(i).vartype + " " + writes.get(i).varname);
    			// output.write("\n\tCondition: " + writes.get(i).getCondition());
    			output.write("\n");
    		}
            
            output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
		
		/*
		System.out.printf("\nRecurse conditions:\n");
		for (int i = 0; i < recursecond.size(); i++) {
			System.out.printf("(" + i + ") " + recursecond.get(i) + "\n");
		}
		
		*/
		
		ArrayList<String> condpair0 = new ArrayList<String>();
		ArrayList<String> condpair1 = new ArrayList<String>();
		ArrayList<String> fromname = new ArrayList<String>();
		ArrayList<String> toname = new ArrayList<String>();
		
		int allcollisions = 0;
		
		try {
            File file = new File("../out/", "collisions.txt");
            file.getParentFile().mkdirs();
            BufferedWriter output = new BufferedWriter(new FileWriter(file));

		for (int i = 0; i < writes.size(); i++) {
			String type = writes.get(i).getType();
			if (type.equals("Node")) {
				String fullname = writes.get(i).getVarname();
				
				for (int j = 0; j < reads.size(); j++) {
					String fullname2 = reads.get(j).getVarname();
					String type2 = reads.get(j).getType();
					
					if (fullname.equals("root") && type2.equals("Node")) {
						// System.out.printf("\nWrite " + fullname + "  Cond: " + writes.get(i).getCondition() + "\n");
						// System.out.printf("\tcollides with read " + fullname2 + "  Cond: " + reads.get(j).getCondition() + "\n");
						output.write("\nWrite " + fullname + "*\n");
						output.write("\tcollides with read " + fullname2 + "\n");
						
						condpair1.add(writes.get(i).getCondition());
						condpair0.add(reads.get(j).getCondition());
						fromname.add(fullname);
						toname.add(fullname2);
						allcollisions++;
					}
					else if (fullname2.equals("root") && type.equals("Node")) {
						// System.out.printf("\nWrite " + fullname + "  Cond: " + writes.get(i).getCondition() + "\n");
						// System.out.printf("\tcollides with read " + fullname2 + "  Cond: " + reads.get(j).getCondition() + "\n");
						output.write("\nWrite " + fullname + "\n");
						output.write("\tcollides with read " + fullname2 + "*\n");
						
						condpair0.add(writes.get(i).getCondition());
						condpair1.add(reads.get(j).getCondition());
						fromname.add(fullname2);
						toname.add(fullname);
						allcollisions++;
					}
				}
			}
			
			String name = getSuffix(writes.get(i).getVarname());
			String prefix = getPrefix(writes.get(i).getVarname());
			String unrooted = writes.get(i).getVarname().replace("root.","");
			// System.out.printf("Write: " + prefix + "." + name + "\n");
			
			for (int j = 0; j < reads.size(); j++) {
				String accname = getSuffix(reads.get(j).getVarname());
				String accprefix = getPrefix(reads.get(j).getVarname());
				String accunrooted = reads.get(j).getVarname().replace("root.","");
				
				if (name.equals(accname)) {
					allcollisions++;
					if (!prefix.equals(accprefix)) {
						if (unrooted.contains(accunrooted)) {
							// System.out.printf("\nWrite " + prefix + "." + name + "  Cond: " + writes.get(i).getCondition() + "\n");
							// System.out.printf("\tcollides with read " + accprefix + "." + accname + "  Cond: " + reads.get(j).getCondition() + "\n");
							output.write("\nWrite " + prefix + "." + name + "\n");
							output.write("\tcollides with read " + accprefix + "." + accname + "*\n");
							
							condpair0.add(writes.get(i).getCondition());
							condpair1.add(reads.get(j).getCondition());
							fromname.add(accprefix + "." + accname);
							toname.add(prefix + "." + name);
						}
						else if (accunrooted.contains(unrooted)) {
							// System.out.printf("\nWrite " + prefix + "." + name + "  Cond: " + writes.get(i).getCondition() + "\n");
							// System.out.printf("\tcollides with read " + accprefix + "." + accname + "  Cond: " + reads.get(j).getCondition() + "\n");
							output.write("\nWrite " + prefix + "." + name + "*\n");
							output.write("\tcollides with read " + accprefix + "." + accname + "\n");
							
							condpair1.add(writes.get(i).getCondition());
							condpair0.add(reads.get(j).getCondition());
							toname.add(accprefix + "." + accname);
							fromname.add(prefix + "." + name);
						}
                        else {
                        	output.write("\nWrite " + prefix + "." + name + "\n");
                        	output.write("\tinterferes with read " + accprefix + "." + accname + " (prefixes don't match)\n");
                        }
					}
                    else {
                    	output.write("\nWrite " + prefix + "." + name + "\n");
                    	output.write("\tinterferes with read " + accprefix + "." + accname + "\n");
                    }
				}
			}
		}
		
		// System.out.printf("\nWrites colliding with writes:\n");
		for (int i = 0; i < writes.size(); i++) {
			String type = writes.get(i).getType();
			if (type.equals("Node")) {
				String fullname = writes.get(i).getVarname();
				// System.out.printf("Write: " + type + " " + fullname + "\n\tCond: " + writes.get(i).getCondition() + "\n");
				
				for (int j = 0; j < writes.size(); j++) {
					String fullname2 = writes.get(j).getVarname();
					String type2 = writes.get(j).getType();
					
					if (fullname.equals("root") && type2.equals("Node")) {
						// System.out.printf("\nWrite " + fullname + "  Cond: " + writes.get(i).getCondition() + "\n");
						// System.out.printf("\tcollides with write " + fullname2 + "  Cond: " + writes.get(j).getCondition() + "\n");
						output.write("\nWrite " + fullname + "*\n");
						output.write("\tcollides with write " + fullname2 + "\n");
						
						condpair1.add(writes.get(i).getCondition());
						condpair0.add(writes.get(j).getCondition());
						fromname.add(fullname);
						toname.add(fullname2);
						allcollisions++;
					}
					else if (fullname2.equals("root") && type.equals("Node")) {
						// System.out.printf("\nWrite " + fullname + "  Cond: " + writes.get(i).getCondition() + "\n");
						// System.out.printf("\tcollides with write " + fullname2 + "  Cond: " + writes.get(j).getCondition() + "\n");
						output.write("\nWrite " + fullname + "\n");
						output.write("\tcollides with write " + fullname2 + "*\n");
						
						condpair0.add(writes.get(i).getCondition());
						condpair1.add(writes.get(j).getCondition());
						fromname.add(fullname2);
						toname.add(fullname);
						allcollisions++;
					}
				}
			}
			
			String name = getSuffix(writes.get(i).getVarname());
			String prefix = getPrefix(writes.get(i).getVarname());
			String unrooted = writes.get(i).getVarname().replace("root.","");
			
			for (int j = i; j < writes.size(); j++) {
				String accname = getSuffix(writes.get(j).getVarname());
				String accprefix = getPrefix(writes.get(j).getVarname());
				String accunrooted = writes.get(j).getVarname().replace("root.","");
				
				if (name.equals(accname)) {
					allcollisions++;
					if (!prefix.equals(accprefix)) {
						if (unrooted.contains(accunrooted)) {
							// System.out.printf("\nWrite " + prefix + "." + name + "  Cond: " + writes.get(i).getCondition() + "\n");
							// System.out.printf("\tcollides with write " + accprefix + "." + accname + "  Cond: " + writes.get(j).getCondition() + "\n");
							output.write("\nWrite " + prefix + "." + name + "\n");
							output.write("\tcollides with write " + accprefix + "." + accname + "*\n");
							condpair0.add(writes.get(i).getCondition());
							condpair1.add(writes.get(j).getCondition());
							fromname.add(accprefix + "." + accname);
							toname.add(prefix + "." + name);
						}
						else if (accunrooted.contains(unrooted)) {
							// System.out.printf("\nWrite " + prefix + "." + name + "  Cond: " + writes.get(i).getCondition() + "\n");
							// System.out.printf("\tcollides with write " + accprefix + "." + accname + "  Cond: " + writes.get(j).getCondition() + "\n");
							output.write("\nWrite " + prefix + "." + name + "*\n");
							output.write("\tcollides with write " + accprefix + "." + accname + "\n");
							condpair1.add(writes.get(i).getCondition());
							condpair0.add(writes.get(j).getCondition());
							toname.add(accprefix + "." + accname);
							fromname.add(prefix + "." + name);
						}
                        else {
                        	output.write("\nWrite " + prefix + "." + name + "\n");
                        	output.write("\tinterferes with write " + accprefix + "." + accname + " (prefixes don't match)\n");
                        }
					}
                    else {
                    	output.write("\nWrite " + prefix + "." + name + "\n");
                    	output.write("\tinterferes with write " + accprefix + "." + accname + "\n");
                    }
				}
			}
		}
		output.close();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
		
		//System.out.printf("\nNumber of total accesses: " + (writes.size() + reads.size()));
		
		/*
		System.out.printf("\nDeclarations of everything used in a condition:\n");
		for (int i = 0; i < conddecs.size(); i++) {
			System.out.printf(conddecs.get(i) + "\n");
		}
		System.out.printf("\n");
		*/
		
		ArrayList<Integer> satcollisions = new ArrayList<Integer>();
		ArrayList<Integer> errors = new ArrayList<Integer>();
		// System.out.printf("\nNumber of total collisions, including ones ruled out by matching names: " + allcollisions);
		// System.out.printf("\nNumber of pairs of collision conditions: " + condpair0.size());
		// System.out.printf("\nNumber of recurse conditions: " + recursecond.size());
		// System.out.printf("\nExpected number of z3 calls: " + (condpair0.size() * recursecond.size()) + "\n");
		
		int actualz3calls = 0;
		// Set<String> mcpset = new HashSet<String>();
		
        float z3timetotal = 0.0f;
        
		for (int i = 0; i < condpair0.size(); i++) {
			// System.out.printf(condpair0.get(i) + "\n");
			// System.out.printf(condpair1.get(i) + "\n");
			
			// System.out.printf("\nTurns into:\n");
			for (int j = 0; j < recursecond.size(); j++) {
				int condsetnum = (recursecond.size() * i) + j;
				if (condsetnum%1000 == 0 || condsetnum == (condpair0.size() * recursecond.size()))
					System.out.printf("Condition set " + condsetnum + "\n");
				// System.out.printf("Recurse condition " + j + "\n");
				
				
				String postreplacement = condpair1.get(i).replaceAll(fromname.get(i), toname.get(i));
				postreplacement = "(and " + postreplacement + " " + recursecond.get(j) + ")";
				
				// System.out.printf(condpair0.get(i) + "\n");
				// System.out.printf(postreplacement + "\n\n");
					
                // String filename = "conditionset" + condsetnum + ".smt2";
                String filename = "conditionset0.smt2";
                
                try {
                    File file = new File(filename);
                    BufferedWriter output = new BufferedWriter(new FileWriter(file));
                    for (int k = 0; k < conddecs.size(); k++) {
                        output.write(conddecs.get(k) + "\n");
                        if (conddecs.get(k).contains(fromname.get(i))) {
                            String condpostreplacement = conddecs.get(k).replaceAll(fromname.get(i), toname.get(i));
                            boolean contained = false;
                            for (int l = 0; l < conddecs.size(); l++) {
                                if (condpostreplacement.equals(conddecs.get(l))) {
                                    contained = true;
                                }
                            }
                            
                            if (!contained) {
                                output.write(condpostreplacement + "\n");
                                // System.out.printf("Added the following to condition declarations: " + condpostreplacement + "\n");
                            }
                            // We need to add a new declaration if it isn't already declared
                        }
                    }
                    output.write("(assert " + condpair0.get(i) + ")\n");
                    output.write("(assert " + postreplacement + ")\n");
                    output.write("(check-sat)");
                    output.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
                
                long z3callstart = System.nanoTime();
                // String z3output = executeCommand("../../z3/build/z3 " + filename);
                String z3output = executeCommand("z3 " + filename);
                actualz3calls++;
                long z3callend = System.nanoTime();
                
                float z3timesingle = (float) (z3callend - z3callstart) / 1000000;
                z3timetotal += z3timesingle;
                
                z3output = z3output.replaceAll("\\W", "");
                // System.out.println("Output returned: " + z3output);
                if (z3output.equals("unsat")) {
                    // System.out.println("Collision can never actually happen, conditions are safe\n");
                }
                else if (z3output.equals("sat")){
                    // System.out.println("Accesses in collision may both happen, conditions are unsafe\n");
                    satcollisions.add(condsetnum);
                }
                else {
                    // System.out.println("ERROR: Unknown output returned!\n");
                    errors.add(condsetnum);
                }
            }
		}
		
		if (errors.size() != 0) {
			System.out.printf("\n*--------------------------------------------------------------");
			System.out.printf("\n*\tOne or more sets of collisions errored! NEEDS DEBUGGING!");
			System.out.printf("\n*\tCondition sets that produced error results: ");
			for (int i = 0; i < errors.size(); i++)
				System.out.printf(errors.get(i) + " ");
			System.out.printf("\n*--------------------------------------------------------------");
		}
		else if (satcollisions.size() != 0) {
			System.out.printf("\n*--------------------------------------------------------------");
			System.out.printf("\n*\tOne or more sets of collisions returned sat. Transformation was determined UNSAFE!");
			System.out.printf("\n*\tCondition sets that produced sat results: ");
			for (int i = 0; i < satcollisions.size(); i++)
				System.out.printf(satcollisions.get(i) + " ");
			System.out.printf("\n*--------------------------------------------------------------");
		}
		else {
			System.out.printf("\n*--------------------------------------------------------------");
			System.out.printf("\n*\tAll sets of collisions returned unsat. Transformation was determined SAFE!");
			System.out.printf("\n*--------------------------------------------------------------");
		}
        
        System.out.printf("\n*\tTotal number of actual z3 calls: " + actualz3calls);
        System.out.printf("\n*\tTotal time spent in z3 calls: %.2f", z3timetotal);
        System.out.printf("\n*--------------------------------------------------------------\n\n");
	}
	
	private static String executeCommand(String command) {
		 
		StringBuffer output = new StringBuffer();
 
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
                            new BufferedReader(new InputStreamReader(p.getInputStream()));
 
                        String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}
 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
		return output.toString();
 
	}
	
	private static String splitASTConditions(ASTNode root, String cc, ArrayList<AccessNode> r, ArrayList<AccessNode> w, ArrayList<String> rc, ArrayList<String> cd) {
		if (root instanceof IfStmt) {
			StringBuilder tc = new StringBuilder();
			processCond(root.getChild(0), tc, cd);
			varAccToANCond(root.getChild(0), r, cc); // Everything in the condition is read
			String tempcond = tc.toString();
			
			String nextconds = "(and " + cc + " " + tempcond + ")";
			String elseconds = "(and " + cc + " (not " + tempcond + "))";
			// Create conditions for each branch of if statement
			
			nextconds = splitASTConditions(root.getChild(1), nextconds, r, w, rc, cd);
			
			if (root.getChild(2).getNumChild() != 0) {
				elseconds = splitASTConditions(root.getChild(2), elseconds, r, w, rc, cd);
				// Only check else if else exists
			}
			
			cc = "(or " + nextconds + " " + elseconds + ")"; // Merge with or
			
			return cc;
		}
		
		if (root instanceof VariableDeclaration) {
			AccessNode tempnode = new AccessNode("", "", cc);
			String name = "ERROR";
			String type = "ERROR";
			
			for (int i = 0; i < root.getNumChild(); i++) {
				if (root.getChild(i) instanceof TypeAccess) {
					type = root.getChild(i).toString();
					VariableDeclaration vdec = (VariableDeclaration) root;
					name = vdec.getID();
					
					tempnode.setName(name);
					tempnode.setType(type);
					// Set name and type of variable being declared
				}
				else if (root.getChild(i) instanceof Opt) {
					if (root.getChild(i).getNumChild() != 0) {
						
						if (type.equals("Node")) {
							String smtdec = "(declare-const " + name + " " + "Int" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("int")) {
							String smtdec = "(declare-const " + name + " " + "Int" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("boolean")) {
							String smtdec = "(declare-const " + name + " " + "Bool" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("double")) {
							String smtdec = "(declare-const " + name + " " + "Real" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("float")) {
							String smtdec = "(declare-const " + name + " " + "Real" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("Long")) {
							String smtdec = "(declare-const " + name + " " + "Int" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						else if (type.equals("String")) {
							String smtdec = "(declare-const " + name + " " + "String" + ")";
							Boolean contained = false;
							for (int j = 0; j < cd.size(); j++) {
								if (cd.get(j).equals(smtdec))
									contained = true;
							}
							if (!contained)
								cd.add(smtdec);
						}
						// Sloppy bit copied from processCond because I have to declare what's left of the assignment if we declare and initialize a variable
						
						ASTNode assignment = root.getChild(i).getChild(0);
						if (assignment instanceof Dot || assignment instanceof VarAccess) {
							tempnode.addAlias(assignment.toString());
							varAccToANCond(assignment, r, cc); // Any reads on the right side of assignment picked up here
							
							StringBuilder rightcond = new StringBuilder();
							processCond(assignment, rightcond, cd);
							
							cc = "(and " + cc + " (= " + tempnode.getVarname() + " " + rightcond.toString() + "))";
						}
					}
				}
			}
			
			// Note that we only care about nodes for possible collisions (writelist),
			// but all declarations could be used for conditions (cc)
			if (tempnode.getType().equals("Node")) {
				w.add(tempnode);
			}
			
			return cc;
		}
		
		if (root instanceof ParameterDeclaration) {
			AccessNode tempnode = new AccessNode("", "", cc);
			
			for (int i = 0; i < root.getNumChild(); i++) {
				if (root.getChild(i) instanceof TypeAccess) {
					String type = root.getChild(i).toString();
					ParameterDeclaration pdec = (ParameterDeclaration) root;
					String name = pdec.getID();
					
					tempnode.setName(name);
					tempnode.setType(type);
				}
			}
			
			// Only care about reads of nodes, no conditions should be affected
			if (tempnode.getType().equals("Node")) {
				r.add(tempnode);
			}
			
			return cc;
		}
		
		if (root instanceof AssignExpr) {
			String leftaccess, leftorigin;
			
			ASTNode assignleft = root.getChild(0);
			ASTNode assignright = root.getChild(1);
			while (assignleft instanceof Dot) {
				Dot dot = (Dot) assignleft;
				assignleft = dot.getRight();
			}
			VarAccess lvacc = (VarAccess) assignleft;
			TypeDecl ltdecl = lvacc.type();
			leftaccess = ltdecl.getID();

			assignleft = root.getChild(0);
			while (assignleft instanceof Dot) {
				Dot dot = (Dot) assignleft;
				assignleft = dot.getLeft();
			}
			if (assignleft instanceof VarAccess) {
				VarAccess lorigvacc = (VarAccess) assignleft;
				TypeDecl lorigtdecl = lorigvacc.type();
				leftorigin = lorigtdecl.getID();
			}
			else
				leftorigin = "Not a variable access";
			
			if (leftaccess.equals("Node") || leftorigin.equals("Node")) {
				AccessNode tempnode = new AccessNode("", "", cc);
				Boolean contained = false;
				String leftname = root.getChild(0).toString();
				for (int i = 0; i < w.size(); i++) {
					if (leftname.equals(w.get(i).getVarname()) && w.get(i).getCondition().equals(cc)) {
						tempnode = w.get(i);
						contained = true;
					}
				}
				if (!contained) {
					tempnode.setName(leftname);
					tempnode.setType(leftaccess);
				}
				
				if (leftaccess.equals("Node")) {
					if (assignright instanceof Dot || assignright instanceof VarAccess)
						tempnode.addAlias(root.getChild(1).toString());
				}
					
				if (!contained)
					w.add(tempnode);
				
				StringBuilder dummy = new StringBuilder();
				processCond(root.getChild(0), dummy, cd);
					
				// System.out.printf("Statement: " + root.toString() + "\nType found: " + leftaccess + "\n");
				// System.out.printf("Left originating structure type: " + leftorigin + "\n");
				// System.out.printf("Current condition: " + cc + "\n");
				
				// Also need to process read part of assignment
				// System.out.printf("Also adding right side to read list if needed.\n\n");
				varAccToANCond(root.getChild(1), r, cc);
				
				// All assignments should end up creating a condition
				StringBuilder rightcond = new StringBuilder();
				processCond(root.getChild(1), rightcond, cd);
				
				cc = "(and " + cc + " (= " + root.getChild(0).toString() + " " + rightcond.toString() + "))";
				// System.out.printf("Post-assign condition: " + cc + "\n\n");
			}
			return cc;
		}
		
		if (root instanceof PostfixExpr || root instanceof PreDecExpr || root instanceof PreIncExpr) {
			String leftaccess, leftorigin;
			
			ASTNode assignleft = root.getChild(0);
			while (assignleft instanceof Dot) {
				Dot dot = (Dot) assignleft;
				assignleft = dot.getRight();
			}
			VarAccess lvacc = (VarAccess) assignleft;
			TypeDecl ltdecl = lvacc.type();
			leftaccess = ltdecl.getID();

			assignleft = root.getChild(0);
			while (assignleft instanceof Dot) {
				Dot dot = (Dot) assignleft;
				assignleft = dot.getLeft();
			}
			if (assignleft instanceof VarAccess) {
				VarAccess lorigvacc = (VarAccess) assignleft;
				TypeDecl lorigtdecl = lorigvacc.type();
				leftorigin = lorigtdecl.getID();
			}
			else
				leftorigin = "Not a variable access";
			
			if (leftaccess.equals("Node") || leftorigin.equals("Node")) {
				AccessNode tempnode = new AccessNode("", "", cc);
				Boolean contained = false;
				String leftname = root.getChild(0).toString();
				for (int i = 0; i < w.size(); i++) {
					if (leftname.equals(w.get(i).getVarname()) && w.get(i).getCondition().equals(cc)) {
						tempnode = w.get(i);
						contained = true;
					}
				}
				if (!contained) {
					tempnode.setName(leftname);
					tempnode.setType(leftaccess);
				}
					
				if (!contained)
					w.add(tempnode);
			}
			
			// System.out.printf("Statement: " + root.toString() + "\nType found: " + leftaccess + "\n");
			// System.out.printf("Originating structure type: " + leftorigin + "\n\n");
			
			// This doesn't generate a condition, need better analysis on actual numbers for this to work
			return cc;
		}
		
		if (root instanceof ReturnStmt) {
			// There should never be anything executed after a return statement
			cc = "false";
			return cc;
		}
		
		if (root instanceof MethodAccess) {
			// Special case here since everything will be inlined, this is only able to call recursive function
			varAccToANCond(root, r, cc);
			rc.add(cc);
		}
		
		// Should only happen if one of the other statements didn't return
		for (int i = 0; i < root.getNumChild(); i++) {
			cc = splitASTConditions(root.getChild(i), cc, r, w, rc, cd);
		}
		
		return cc;
	}
	
	private static void processCond(ASTNode root, StringBuilder s, ArrayList<String> cd) {
		if (root instanceof EQExpr) {
			s.append("(= ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof GTExpr) {
			s.append("(> ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof GEExpr) {
			s.append("(>= ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof LTExpr) {
			s.append("(< ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof LEExpr) {
			s.append("(<= ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof NEExpr) {
			s.append("(distinct ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof AddExpr) {
			s.append("(+ ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof DivExpr) {
			s.append("(/ ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof ModExpr) {
			s.append("(mod ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof MulExpr) {
			s.append("(* ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof SubExpr) {
			s.append("(- ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof AndLogicalExpr) {
			s.append("(and ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof OrLogicalExpr) {
			s.append("(or ");
			processCond(root.getChild(0), s, cd);
			s.append(" ");
			processCond(root.getChild(1), s, cd);
			s.append(")");
		} else if (root instanceof LogNotExpr) {
			s.append("(not ");
			processCond(root.getChild(0), s, cd);
			s.append(")");
		} else if (root instanceof ClassInstanceExpr) {
			s.append("1");
		} else if (root instanceof VarAccess) {
			s.append(root.toString());
			VarAccess vacc = (VarAccess) root;
			TypeDecl tdecl = vacc.type();
			String vartype = tdecl.getID();
			if (vartype.equals("Node")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("int")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("boolean")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Bool" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("double")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Real" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("float")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Real" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("Long")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("String")) {
				String smtdec = "(declare-const " + root.toString() + " " + "String" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
		} else if (root instanceof Dot) {
			s.append(root.toString());
			
			ASTNode typecheck = root;
			while (typecheck instanceof Dot) {
				Dot dot = (Dot) typecheck;
				typecheck = dot.getRight();
			}
			
			VarAccess vacc = (VarAccess) typecheck;
			TypeDecl tdecl = vacc.type();
			String vartype = tdecl.getID();
			if (vartype.equals("Node")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("int")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("boolean")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Bool" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("double")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Real" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("float")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Real" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("Long")) {
				String smtdec = "(declare-const " + root.toString() + " " + "Int" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
			else if (vartype.equals("String")) {
				String smtdec = "(declare-const " + root.toString() + " " + "String" + ")";
				Boolean contained = false;
				for (int i = 0; i < cd.size(); i++) {
					if (cd.get(i).equals(smtdec))
						contained = true;
				}
				if (!contained)
					cd.add(smtdec);
			}
		} else if (root instanceof NullLiteral) {
			s.append("0");
		} else if (root instanceof IntegerLiteral || root instanceof BooleanLiteral) {
			s.append(root.toString());
		} else if (root instanceof DoubleLiteral || root instanceof FloatingPointLiteral) {
			String digits = root.toString().replaceAll("[^0-9.]", "");
			s.append(digits);
		}
	}
	
	private static void varAccToANCond(ASTNode root, ArrayList<AccessNode> acclist, String cond) {
		// System.out.printf("Made it to varAccToANCond\n");
		// printAST("", true, root);
		if (root instanceof VarAccess) {
			VarAccess vacc = (VarAccess) root;
			TypeDecl tdec = vacc.type();
			String vartype = tdec.getID();
			String varname = root.toString();
			
			Boolean contained = false;
			for (int i = 0; i < acclist.size(); i++) {
				if (varname.equals(acclist.get(i).getVarname()) && acclist.get(i).getCondition().equals(cond))
					contained = true;
			}
			
			if (!contained && vartype.equals("Node")) {
				AccessNode tempnode = new AccessNode(vartype, varname, cond);
				acclist.add(tempnode);
				// System.out.printf("\tAdding " + vartype + " " + varname + "\n");
			}
		}
		else if (root instanceof Dot) {
			Dot fulldot = (Dot) root;
			if (fulldot.getLeft() instanceof VarAccess) {
				VarAccess lvacc = (VarAccess) fulldot.getLeft();
				TypeDecl ltdec = lvacc.type();
				// Only do something if we have a dot where the left side is an access to a Node
				if (ltdec.getID().equals("Node")) {
					String varname = root.toString();
					
					ASTNode tempvar = root;
					while (tempvar instanceof Dot) {
						Dot dot = (Dot) tempvar;
						tempvar = dot.getRight();
					}
					
					VarAccess vacc = (VarAccess) tempvar;
					TypeDecl tdec = vacc.type();
					String vartype = tdec.getID();
					
					Boolean contained = false;
					for (int j = 0; j < acclist.size(); j++) {
						if (varname.equals(acclist.get(j).getVarname()) && acclist.get(j).getCondition().equals(cond))
							contained = true;
					}
					
					if (!contained) {
						AccessNode tempnode = new AccessNode(vartype, varname, cond);
						acclist.add(tempnode);
						// System.out.printf("\tAdding " + vartype + " " + varname + "\n");
					}
				}
			}
		}
		else {
			for (int i = 0; i < root.getNumChild(); i++) {
				varAccToANCond(root.getChild(i), acclist, cond);
			}
		}
	}
		
	private static String getSuffix(String fullname) {
		String[] splits = fullname.split("\\.");
		return splits[splits.length - 1];
	}
	
	private static String getPrefix(String fullname) {
		String[] splits = fullname.split("\\.");
		
		if (splits.length == 1)
			return "";
		
		String ret = "";
		for (int i = 0; i < splits.length - 1; i++) {
			ret = ret + splits[i];
			if (i < splits.length - 2)
				ret = ret + ".";
		}
		
		return ret;
	}
	
	private static void printAST(String prefix, boolean isTail, ASTNode root, BufferedWriter output) throws IOException {
		if (root.getNumChild() > 0) {
			output.write(prefix + (isTail ? "└── " : "├── ") + root.getClass().getName());
			if (root instanceof ParameterDeclaration) {
				ParameterDeclaration pdec = (ParameterDeclaration) root;
				output.write(" NAME: [" + pdec.getID() + "]\n");
			} else if (root instanceof VariableDeclaration) {
				VariableDeclaration vdec = (VariableDeclaration) root;
				output.write(" NAME: [" + vdec.getID() + "]\n");
			} else
				output.write("\n");
		} else {
			if (root instanceof List) {
				if (root instanceof Modifiers) {
					output.write(prefix + (isTail ? "└── " : "├── ") + root.getClass().getName() + " [Empty list of modifiers]\n");
				} else
					output.write(prefix + (isTail ? "└── " : "├── ") + root.getClass().getName() + " [Unknown type of empty list]\n"); 
			} else if (root instanceof Opt) {
				output.write(prefix + (isTail ? "└── " : "├── ") + root.getClass().getName() + " [Optional attributes empty]\n");
			} else
				output.write(prefix + (isTail ? "└── " : "├── ") + root + " [" + root.getClass().getName() + "]\n");
		}
        for (int i = 0; i < root.getNumChild() - 1; i++) {
            printAST(prefix + (isTail ? "    " : "│   "), false, root.getChild(i), output);
        }
        if (root.getNumChild() >= 1) {
            printAST(prefix + (isTail ?"    " : "│   "), true, root.getChild(root.getNumChild() - 1), output);
        }
    }
	
	private static boolean findEnclosingForLoop(MethodAccess macc) {
		ASTNode node = macc;
		while (node != null) {
			node = node.getParent();
			if (node instanceof ForStmt) {
				MethodDecl encMdec = Utils.getEnclosingMethodDecl(node);
				// transform only if enclosing method has @Parallel annotation
				boolean parallelAnnotation = false;
				for (Modifier mod : encMdec.getModifiers().getModifierList()) {
					if (mod instanceof Annotation) {
						Annotation annot = (Annotation) mod;
						Access acc = annot.getAccess();
						if (acc instanceof TypeAccess) {
							TypeAccess tacc = (TypeAccess) acc;
							if (tacc.getID().equals(Utils.annotationName_parallel)) {
								parallelAnnotation = true;
								break;
							}
						}
					}
				}
				if (parallelAnnotation) {
					Globals.outerFor = (ForStmt) node;
					return true;
				}
			} else if (node instanceof MethodDecl) {
				MethodDecl nextMdec = (MethodDecl) node;
				for (MethodAccess nextMacc : nextMdec.accessPoints) {
					if (findEnclosingForLoop(nextMacc)) {
						Globals.mainPath.add(nextMacc);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static boolean isPseudoTailRecursive(MethodDecl mdec) {
		ArrayList<Stmt> recursionStmts = new ArrayList<Stmt>();
		for (MethodAccess macc : mdec.accessPoints) {
			if (Utils.getEnclosingMethodDecl(macc) != mdec) continue;
			recursionStmts.add(Utils.getEnclosingStmt(macc));
		}
		for (Stmt recursionStmt : recursionStmts) {
			ASTNode node = recursionStmt;
			while (node != null) {
				node = node.getParent();
				if (node instanceof Block) {
					Block block = (Block) node;
					boolean found = false;
					for (int i = 0; i < block.getNumStmt(); i++) {
						Stmt stmt = block.getStmt(i);
						if (stmt == recursionStmt) {
							found = true;
						} else if (found) {
							if (!recursionStmts.contains(stmt)) {
								System.out.printf(" [%s]\n followed by\n [%s]\n", recursionStmt, stmt);
								return false;
							}
						}
					}
					recursionStmt = block;
				} else if (node instanceof ForStmt) {
					System.out.printf("\t\t %s within for loop\n", recursionStmt);
					return false;
				} else if (node instanceof IfStmt) {
					recursionStmt = (Stmt) node;
				} else if (node instanceof MethodDecl) {
					break;
				}
			}
		}
		return true;
	}
	
	private static void setBlockPaths(CompilationUnit cu) {
		String relName = cu.relativeName();
		String [] split = relName.split("/");
		for (int i = 1; i < split.length - 1; i++) {
			Globals.blockPathName += split[i];
			Globals.blockPackageName += split[i];
			if (i != split.length - 2) {
				Globals.blockPathName += "/";
				Globals.blockPackageName += ".";  
			}
		}	
	}
}
