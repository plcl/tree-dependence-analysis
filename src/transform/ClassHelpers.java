/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;

import AST.Access;
import AST.List;

public class ClassHelpers {
	
	public static class IfPart {
		public String cond;
		public ArrayList<String> lines = new ArrayList<String>();
	}
	
	protected static String getIfStr(String prefix, ArrayList<IfPart> ifparts) {
		String ret = "";
		for (String line : getIfStrLines(prefix, ifparts)) {
			ret += line;
		}
		return ret;
	}
	
	protected static ArrayList<String> getIfStrLines(String prefix, ArrayList<IfPart> ifparts) {
		ArrayList<String> ret = new ArrayList<String>();
		if (ifparts.size() == 1) {
			for (String line : ifparts.get(0).lines) {
				ret.add(String.format("%s%s", prefix, line));	
			}
		} else if (ifparts.size() == 2) {
			ret.add(String.format("%sif (%s) {\n", prefix, ifparts.get(0).cond));
			for (String line : ifparts.get(0).lines) {
				ret.add(String.format("%s\t%s", prefix, line));
			}
			ret.add(String.format("%s} else {\n", prefix));
			for (String line : ifparts.get(1).lines) {
				ret.add(String.format("%s\t%s", prefix, line));
			}
			ret.add(String.format("%s}\n", prefix));
		} else {
			ret.add(String.format("%sif (%s) {\n", prefix, ifparts.get(0).cond));
			for (String line : ifparts.get(0).lines) {
				ret.add(String.format("%s\t%s", prefix, line));
			}
			for (int i = 1; i < ifparts.size() - 1; i++) {
				ret.add(String.format("%s} else if (%s) {\n", prefix, ifparts.get(i).cond));
				for (String line : ifparts.get(i).lines) {
					ret.add(String.format("%s\t%s", prefix, line));
				}			}
			ret.add(String.format("%s} else {\n", prefix));
			for (String line : ifparts.get(ifparts.size() - 1).lines) {
				ret.add(String.format("%s\t%s", prefix, line));
			}
			ret.add(String.format("%s}\n", prefix));
		}
		return ret;
	}
	
	protected static String concat(ArrayList<String> strs) {
		String ret = "";
		for (int i = 0; i < strs.size(); i++) {
			ret += strs.get(i);
			if (i != strs.size() - 1) ret += ", ";
		}
		return ret;
	}
	
	/*
	 * param helpers
	 */
	protected static ArrayList<String> getAllLoopInvariantParams() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAllLoopInvariantParams().size(); i++) {
			ret.add(MethodInfo.getRecursiveMethodAllLoopInvariantParams().get(i).toString());
		}
		return ret;
	}

	protected static ArrayList<String> getAdditionalLoopVariantParams() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size(); i++) {
			ret.add(MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().get(i).toString());
		}
		return ret;
	}
	
	protected static ArrayList<String> getAllLoopVariantParams() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAllLoopVariantParams().size(); i++) {
			ret.add(MethodInfo.getRecursiveMethodAllLoopVariantParams().get(i).toString());
		}
		return ret;
	}
	
	protected static ArrayList<String> getLoopVariantParams() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodLoopVariantParams().size(); i++) {
			ret.add(MethodInfo.getRecursiveMethodLoopVariantParams().get(i).toString());
		}
		return ret;
	}
	
	/*
	 * arg helpers
	 */
	protected static ArrayList<String> getAllLoopVariantArgs() {
		return getAllLoopVariantArgsWithPrefix("");
	}
	protected static ArrayList<String> getAllLoopVariantArgsWithPrefix(String prefix) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAllLoopVariantParams().size(); i++) {
			ret.add(prefix + MethodInfo.getRecursiveMethodAllLoopVariantParams().get(i).getID());
		}
		return ret;
	}

	protected static ArrayList<String> getAdditionalLoopVariantArgs() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size(); i++) {
			ret.add(MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().get(i).getID());
		}
		return ret;
	}
	
	protected static ArrayList<String> getLoopVariantArgs() {
		return getLoopVariantArgsWithPrefix("");
	}
	protected static ArrayList<String> getLoopVariantArgsWithPrefix(String prefix) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodLoopVariantParams().size(); i++) {
			ret.add(prefix + MethodInfo.getRecursiveMethodLoopVariantParams().get(i).getID());
		}
		return ret;
	}

	protected static ArrayList<String> getAllArgs() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMdec().getNumParameter(); i++) {
			ret.add(MethodInfo.getRecursiveMdec().getParameter(i).getID());
		}
		return ret;
	}

	/*
	 * excluding nodeParam, and additional loop invariant args
	 */
	protected static ArrayList<String> getAllLoopInvariantArgs() {
		return getAllLoopInvariantArgsWithPrefix("");
	}
	protected static ArrayList<String> getAllLoopInvariantArgsWithPrefix(String prefix) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAllLoopInvariantParams().size(); i++) {
			ret.add(prefix + MethodInfo.getRecursiveMethodAllLoopInvariantParams().get(i).getID());
		}
		return ret;
	}
	
	/*
	 * excluding additional loop invariant args
	 */
	protected static ArrayList<String> getAllLoopInvariantArgsIncludingNode() {
		return getAllLoopInvariantArgsIncludingNodeWithPrefix("");
	}
	protected static ArrayList<String> getAllLoopInvariantArgsIncludingNodeWithPrefix(String prefix) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodAllLoopInvariantParamsIncludingNode().size(); i++) {
			ret.add(prefix + MethodInfo.getRecursiveMethodAllLoopInvariantParamsIncludingNode().get(i).getID());
		}
		return ret;
	}
	
	protected static ArrayList<String> getLoopInvariantArgs() {
		return getLoopInvariantArgsWithPrefix("");
	}
	protected static ArrayList<String> getLoopInvariantArgsWithPrefix(String prefix) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < MethodInfo.getRecursiveMethodLoopInvariantParams().size(); i++) {
			ret.add(prefix + MethodInfo.getRecursiveMethodLoopInvariantParams().get(i).getID());
		}
		return ret;
	}
	
	private static String throwStr = null;;
	protected static String getThrows() {
		if (throwStr != null) return throwStr;
		throwStr = "";
		List<Access> exceptions = MethodInfo.getRecursiveMdec().getExceptionList();
		for (int i = 0; i < exceptions.getNumChild(); i++) {
			if (i == 0) throwStr += "throws ";
			Access a = exceptions.getChild(i);
			throwStr += a.toString();
			if (i < exceptions.getNumChild() - 1) throwStr += ", ";
		}
		return throwStr;
	}
}
