/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import AST.ASTNode;
import AST.AbstractDot;
import AST.Access;
import AST.AddExpr;
import AST.ArrayAccess;
import AST.ArrayTypeAccess;
import AST.AssignExpr;
import AST.AssignPlusExpr;
import AST.AssignSimpleExpr;
import AST.Block;
import AST.BooleanLiteral;
import AST.BreakStmt;
import AST.CastExpr;
import AST.ClassDecl;
import AST.ClassInstanceExpr;
import AST.CompilationUnit;
import AST.ConditionalExpr;
import AST.ContinueStmt;
import AST.Dot;
import AST.EQExpr;
import AST.Expr;
import AST.ExprStmt;
import AST.ForStmt;
import AST.GTExpr;
import AST.IfStmt;
import AST.ImportDecl;
import AST.InstanceOfExpr;
import AST.IntegerLiteral;
import AST.LTExpr;
import AST.List;
import AST.LogNotExpr;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Modifier;
import AST.MulExpr;
import AST.Opt;
import AST.PackageAccess;
import AST.ParameterDeclaration;
import AST.PostIncExpr;
import AST.PrimitiveTypeAccess;
import AST.ReturnStmt;
import AST.SingleTypeImportDecl;
import AST.Stmt;
import AST.SubExpr;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;

public class SpliceTransform {

	public static void transform() {
		transformMapTree();
		transformEntry();
		transformSpliceRecursive();
	}

	/*
	 * spliceOptimizationLevelAnalysis() requires RecursiveCallSet.analysis();
	 */
	private static void spliceOptimizationLevelAnalysis() {

		boolean nodeBasedOrder = true;
		for (int phase = 0; phase < RecursiveField.totalSize; phase++) {
			HashSet<RecursiveField> set = new HashSet<RecursiveField>();
			for (RecursiveCallSet rcs : RecursiveCallSet.getSpliceSets()) {
				RecursiveField rf = null;
				if (phase < rcs.recursiveFields.size()) {
					rf = rcs.recursiveFields.get(phase);
				}
				if (rf != null) {
					nodeBasedOrder = set.add(rf);
				}
				if (!nodeBasedOrder) break;
			}
			if (!nodeBasedOrder) break;
		}

		if (RecursiveCallSet.getSpliceSets().size() == 1) {
			Globals.optimizationLevel = Globals.OptimizationLevel.NO_ORDER;
		} else if (nodeBasedOrder) {
			Globals.optimizationLevel = Globals.OptimizationLevel.NODE_BASED_ORDER;
		} else {
			Globals.optimizationLevel = Globals.OptimizationLevel.GENERAL_ORDER;
		}

		System.out.println("possible optimizaton level: " + Globals.optimizationLevel);
		if (Globals.optimizationLevel.ordinal() > Globals.maxOptLevel) {
			Globals.optimizationLevel = Globals.OptimizationLevel.values()[Globals.maxOptLevel];
		}
		System.out.println("using optimizaton level: " + Globals.optimizationLevel);

		if (MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size() > 0 ||  Globals.optimizationLevel == Globals.OptimizationLevel.GENERAL_ORDER) {
			Globals.usePointIndex = true;
		}
	}

	private static void transformMapTree() {
		for (int i = 0; i < MethodInfo.getMethodInfos().size(); i++) {
			MethodInfo minfo = MethodInfo.getMethodInfos().get(i);			
			transformMapTreeIntermediary(minfo, i == MethodInfo.getMethodInfos().size() - 1);
		}
		transformMapTreeRecursive();
	}

	private static void transformMapTreeRecursive() {
		MethodDecl mdec = MethodInfo.getRecursiveMdec();
		ClassDecl cdec = MethodInfo.getRecursiveCdec();

		List<ParameterDeclaration> pdecs = new List<ParameterDeclaration>();
		for (int i = 0; i < mdec.getNumParameter(); i++) {
			if (MethodInfo.getRecursiveMethodLoopVariantParamIdxs().contains(i) ||
					MethodInfo.getRecursiveMethodAdditionalLoopVariantParamsIdxs().contains(i)) continue;
			pdecs.add(mdec.getParameter(i));
		}
		pdecs.add(new ParameterDeclaration(new TypeAccess(Utils.typeName_SpliceSet), Utils.varName_SpliceSet));
		pdecs.add(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_Depth));
		pdecs.add(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_Index));
		MethodDecl mapMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID() + Utils.methName_mapTreeSuffix,
				pdecs,
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(mdec.getBlock().fullCopy()));
		cdec.addMemberMethod(mapMdec);
		Block mapBody = mapMdec.getBlock();
		mapMdec.renameMethodAccesses(mdec.getID(), mdec.getID() + Utils.methName_mapTreeSuffix);

		// propagate loop variant args
		Set<VarAccess> loopVariantArgsPropagation = new HashSet<VarAccess>();
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopVariantParams()) {
			loopVariantArgsPropagation.add(new VarAccess(pdec.getID()));
		}
		ArrayList<MethodAccess> recursionPoints = Utils.getRecursionPoints(mapMdec);
		ArrayList<Stmt> recursionPointStmts = new ArrayList<Stmt>();
		ArrayList<Stmt> toRemoveStmts = new ArrayList<Stmt>();
		for (MethodAccess macc : recursionPoints) {
			recursionPointStmts.add(Utils.getEnclosingStmt(macc));
		}
		Utils.removeLoopVariantStmts(loopVariantArgsPropagation, mapBody, recursionPointStmts, toRemoveStmts);
		for (Stmt stmt : toRemoveStmts) {
			stmt.removeSelf();
		}

		Stmt commonRecursionStmt = findLargestCommonRecursionBlock(recursionPointStmts);

		if (Globals.isImplicitRecursiveStructure) {
			for (MethodAccess macc : recursionPoints) {
				Dot dot = (Dot) macc.getParent();
				VarAccess vacc = (VarAccess) dot.getLeft();
				for (RecursiveField rfield : RecursiveField.getFields()) {
					if (rfield.recursiveCallStmt == null && rfield.fdec.getID().equals(vacc.getID())) {
						// id should be set here to match order of recursive calls - necessary for no order case
						rfield.setID();
						int removed = 0;
						for (int i : MethodInfo.getRecursiveMethodAllLoopVariantParamsIdxs()) {
							macc.getArgs().removeChild(i - removed++);
						}
						macc.addArg(new VarAccess(Utils.varName_SpliceSet));
						macc.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
						macc.addArg(new AddExpr(new MulExpr(new VarAccess(Utils.varName_Index), new IntegerLiteral(RecursiveField.getFields().size())),
								new IntegerLiteral(rfield.id)));
						rfield.recursiveCallStmt = (Stmt) Utils.getEnclosingStmt(macc).fullCopy();
					}
				}
			}
		} else {
			for (MethodAccess macc : recursionPoints) {
				VarAccess recursiveField = null;
				Expr recursiveArg = null;
				// will be set if macc passes array
				// e.g. RecurseForce(nd, nn.children[treeIndex], dsq * 0.25, epssq);, will be set to treeIndex
				VarAccess arrayExpr = null;
				for (Expr arg : macc.getArgs()) {
					recursiveArg = arg;
					if (arg instanceof CastExpr) {
						CastExpr cexpr = (CastExpr) arg;
						arg = cexpr.getExpr();
					}
					if (!(arg instanceof AbstractDot)) continue;
					AbstractDot dot = (AbstractDot) arg;
					if (!(dot.leftSide() instanceof VarAccess)) continue;
					VarAccess vacc = (VarAccess) dot.getLeft();
					if (!(vacc.getID().equals(Globals.nodeParam.getID()))) continue;
					Expr right = dot.getRight();
					// for arrays
					if (right instanceof Dot) {
						Dot dot2 = (Dot) right;
						Expr right2 = dot2.getRight();
						if (right2 instanceof ArrayAccess) {
							ArrayAccess aa = (ArrayAccess) right2;
							recursiveField = (VarAccess) dot2.getLeft();
							arrayExpr = (VarAccess) aa.getExpr();
						} else {
							//TODO: handle
							assert false;
						}
					} else {
						recursiveField = (VarAccess) dot.getRight();
					}
					break;
				}
				assert recursiveField != null;
				for (RecursiveField rfield : RecursiveField.getFields()) {
					if (!(rfield.recursiveCallStmt == null && rfield.fdec.getID().equals(recursiveField.getID()))) continue;
					// id should be set here to match order of recursive calls - necessary for no order case
					rfield.setID();
					int removed = 0;
					for (int i : MethodInfo.getRecursiveMethodAllLoopVariantParamsIdxs()) {
						macc.getArgs().removeChild(i - removed++);
					}
					macc.addArg(new VarAccess(Utils.varName_SpliceSet));
					macc.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
					if (arrayExpr == null) {
						macc.addArg(new AddExpr(new MulExpr(new VarAccess(Utils.varName_Index), new IntegerLiteral(RecursiveField.totalSize)),
								new IntegerLiteral(rfield.id)));
					} else {
						macc.addArg(new AddExpr(new MulExpr(new VarAccess(Utils.varName_Index), new IntegerLiteral(RecursiveField.totalSize)),
								arrayExpr.fullCopy()));
					}
					rfield.recursiveCallStmt = Utils.getEnclosingStmt(macc);
					rfield.argExpr = recursiveArg;
				}
			}
		}

		Block mapTreeRecurseBlock = new Block();
		for (RecursiveField rfield : RecursiveField.getFields()) {
			if (rfield.recursiveCallStmt == null) continue;
			if (rfield.argExpr instanceof CastExpr) {
				CastExpr cexpr = (CastExpr) rfield.argExpr;
				IfStmt ifInstanceStmt = new IfStmt(new InstanceOfExpr(cexpr.getExpr(), cexpr.getTypeAccess()), rfield.recursiveCallStmt);
				mapTreeRecurseBlock.addStmt(ifInstanceStmt);
			} else {
				mapTreeRecurseBlock.addStmt((Stmt) rfield.recursiveCallStmt.fullCopy());
			}
		}
		commonRecursionStmt.swapStmtWith(mapTreeRecurseBlock);

		/*
		 * _spliceSet.initSpliceSetAndTree(rad, _depth, _index, this);
		 */
		List<Expr> initSpliceSetAndTreeArgs = new List<Expr>();
		for (ParameterDeclaration pdec : MethodInfo.getRecursiveMethodAllLoopInvariantParams()) {
			initSpliceSetAndTreeArgs.add(new VarAccess(pdec.getID()));
		}
		initSpliceSetAndTreeArgs.add(new VarAccess(Utils.varName_Depth));
		initSpliceSetAndTreeArgs.add(new VarAccess(Utils.varName_Index));
		if (Globals.isImplicitRecursiveStructure) {
			initSpliceSetAndTreeArgs.add(new VarAccess("this"));
		} else {
			initSpliceSetAndTreeArgs.add(new VarAccess(Globals.nodeParam.getID()));
			if (!Globals.isStaticRecursiveMethod) {
				initSpliceSetAndTreeArgs.add(new VarAccess("this"));
			}
		}
		MethodAccess initSpliceSetAndTreeMacc = new MethodAccess(Utils.methName_SpliceSet_initSpliceSetAndTree, initSpliceSetAndTreeArgs);
		mapBody.addStmtStart(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
				initSpliceSetAndTreeMacc)));

		// if (_depth > SpliceSet.getDepthParam() + 1) return;
		mapBody.addStmtStart(new IfStmt(
				new GTExpr(
						new VarAccess(Utils.varName_Depth),
						new AddExpr(
								new AbstractDot(
										new VarAccess(Utils.typeName_SpliceSet),
										new MethodAccess(Utils.methName_SpliceSet_getDepthParam, new List<Expr>())
								),
								new IntegerLiteral(1)
						)
				),
				new ReturnStmt()
		)
		);
	}

	private static void transformMapTreeIntermediary(MethodInfo minfo, boolean last) {
		MethodDecl mdec = minfo.mdec;
		MethodAccess macc = minfo.macc;
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		addSpliceClassImports(cu);

		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;		
		Block mapBody = new Block();
		List<ParameterDeclaration> pdecs = new List<ParameterDeclaration>();
		for (ParameterDeclaration pdec : minfo.loopInvariantParams) {
			pdecs.add(pdec);
		}
		pdecs.add(new ParameterDeclaration(new TypeAccess(Utils.typeName_SpliceSet), Utils.varName_SpliceSet));
		MethodDecl mapMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID() + Utils.methName_mapTreeSuffix,
				pdecs,
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(mapBody));
		cdec.addMemberMethod(mapMdec);

		Expr maccLeft = Utils.getDotLeft(macc);

		// propagate loop variant args
		Set<VarAccess> loopVariantArgsPropagation = new HashSet<VarAccess>();
		for (int i : minfo.loopVariantParamIdxs) {
			loopVariantArgsPropagation.add(new VarAccess(mdec.getParameter(i).getID()));
		}

		Stmt maccStmt = Utils.getEnclosingStmt(macc);	
		Stmt mapMaccStmt = null;
		for (Stmt stmt : mdec.getBlock().getStmts()) {
			if (stmt == maccStmt) {
				if (stmt instanceof ExprStmt) {
					ExprStmt estmt = (ExprStmt) maccStmt;
					Expr expr = estmt.getExpr();
					if (expr instanceof AssignExpr) {
						// ret = scene.intersectRay_MapTree(_spliceSet); -> remove ret
						AssignExpr aexpr = (AssignExpr) expr;
						loopVariantArgsPropagation.add((VarAccess) aexpr.getDest());
					}
				}
				List<Expr> mapArgs = new List<Expr>();
				if (last) {
					for (int i = 0; i < macc.getNumArg(); i++) {
						if (MethodInfo.getRecursiveMethodAllLoopVariantParamsIdxs().contains(i)) continue;
						mapArgs.add(macc.getArg(i));
					}
					mapArgs.add(new VarAccess(Utils.varName_SpliceSet));	
					mapArgs.add(new IntegerLiteral(0));
					mapArgs.add(new IntegerLiteral(1));										
				} else {
					for (int i = 0; i < macc.getNumArg(); i++) {
						if (minfo.loopVariantArgIdxs.contains(i)) continue;
						mapArgs.add(macc.getArg(i));
					}
					mapArgs.add(new VarAccess(Utils.varName_SpliceSet));

				}
				MethodAccess mapMacc = new MethodAccess(macc.getID() + Utils.methName_mapTreeSuffix, mapArgs);
				Expr mapMaccExpr = maccLeft == null ? mapMacc : new AbstractDot((Expr) maccLeft.fullCopy(), mapMacc);
				mapBody.addStmt(new ExprStmt(mapMaccExpr));
			}
			mapBody.addStmt((Stmt) stmt.fullCopy());
		}

		ArrayList<Stmt> recursionPointStmts = new ArrayList<Stmt>();
		ArrayList<Stmt> toRemoveStmts = new ArrayList<Stmt>();
		recursionPointStmts.add(mapMaccStmt);
		Utils.removeLoopVariantStmts(loopVariantArgsPropagation, mapBody, recursionPointStmts, toRemoveStmts);
		for (Stmt stmt : toRemoveStmts) {
			stmt.removeSelf();
		}

		/*
		 * remove return values
		 */
		ArrayList<ReturnStmt> returns = new ArrayList<ReturnStmt>();
		mapMdec.gatherReturns(returns);
		for (ReturnStmt rstmt : returns) {
			rstmt.swapStmtWith(new ReturnStmt());
		}
	}

	private static void transformEntry() {
		MethodAccess macc = MethodInfo.getEntryMacc();
		Expr maccLeft = null;
		if (macc.getParent() instanceof AbstractDot) {
			AbstractDot dot = (AbstractDot) macc.getParent();
			maccLeft = dot.getLeft();
		}
		Stmt maccStmt = Utils.getEnclosingStmt(macc);

		Block entryBody = new Block();

		/*
		 * divide block into prologue and epilogue
		 */
		ArrayList<Stmt> prologue = new ArrayList<Stmt>();
		ArrayList<Stmt> epilogue = new ArrayList<Stmt>();
		ArrayList<VariableDeclaration> prologueVarDecs = new ArrayList<VariableDeclaration>();
		ArrayList<VariableDeclaration> loopVariantArgDecs = new ArrayList<VariableDeclaration>();
		boolean nextEntryFound = false;
		for (Stmt stmt : ((Block) Globals.outerFor.getStmt()).getStmts()) {
			boolean nextEntryFoundNow = false;
			if (stmt == maccStmt) {
				nextEntryFoundNow = true;	
				nextEntryFound = true;
			}
			if (!nextEntryFoundNow) {
				if (!nextEntryFound) {
					if (stmt instanceof VariableDeclaration) {
						VariableDeclaration vdec = (VariableDeclaration) stmt;
						boolean isLoopVariantArg = false;
						for (int j : MethodInfo.getOuterForloopVariantArgIdxs()) {
							if (vdec.getID().equals(macc.getArg(j).toString())) {
								isLoopVariantArg = true;
								break;
							}
						}
						if (isLoopVariantArg) {
							loopVariantArgDecs.add(vdec);
						} else {
							prologueVarDecs.add(vdec);
							prologue.add(stmt);
						}
					} else {
						prologue.add(stmt);
					}
				} else {
					epilogue.add(stmt);
				}
			}
		}

		/*
		 * all local variables declared in prologue and used in epilogue need to be saved in IntermediaryState
		 */
		ArrayList<VariableDeclaration> usedInEpilogueVarDecs = new ArrayList<VariableDeclaration>();
		for (VariableDeclaration vdec : prologueVarDecs) {
			for (Stmt stmt : epilogue) {
				if (stmt.findVarUse(vdec.getID())) {
					usedInEpilogueVarDecs.add(vdec);
					break;
				}
			}
		}

		/*
		 * get info from outerFor
		 */
		assert Globals.outerFor.getNumInitStmt() == 1;	// assuming single init stmt
		VariableDeclaration outerForInitStmt = (VariableDeclaration) Globals.outerFor.getInitStmt(0);
		String indexName = outerForInitStmt.getID();
		LTExpr cond = (LTExpr) Globals.outerFor.getCondition();
		assert cond.getLeftOperand().toString().equals(outerForInitStmt.getID());	// assuming init and cond use same index
		Expr condRight = cond.getRightOperand();
		// also assuming int type, and increment by one

		Block splicePath = new Block();

		/*
		 * _IntermediaryState _interState = new _IntermediaryState();
		 * _SpliceSet _spliceSet = new _SpliceSet();
		 * _interState.spliceSet = _spliceSet;
		 */
		splicePath.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_IntermediaryState), Utils.varName_InterState,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_IntermediaryState), new List())));
		splicePath.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_SpliceSet), Utils.varName_SpliceSet,
				new ClassInstanceExpr(new TypeAccess(Utils.typeName_SpliceSet), new List())));
		splicePath.addStmt(new ExprStmt(new AssignSimpleExpr(
				new AbstractDot(new VarAccess(Utils.varName_InterState), new VarAccess("spliceSet")),
				new VarAccess(Utils.varName_SpliceSet))));

		if (Globals.useBlock) {
			/*
			 * ComputeForce_MapRoot(root, diameter, itolsq, s, dthf, epssq);
			 */
			List<Expr> mapRootArgs = new List<Expr>();
			for (Expr arg : MethodInfo.getOuterForLoopInvariantArgs()) {
				mapRootArgs.add((Expr) arg.fullCopy());
			}
			MethodAccess mapRootMacc = new MethodAccess(macc.getID() + Utils.methName_mapRootSuffix, mapRootArgs);
			Expr mapRootExpr = maccLeft == null ? mapRootMacc : new AbstractDot((Expr) maccLeft.fullCopy(), mapRootMacc);
			entryBody.addStmt(new ExprStmt(mapRootExpr));
		}

		/*
		 * ComputeForce_MapTree(root, diameter, itolsq, s, dthf, epssq);
		 */
		List<Expr> mapTreeArgs = new List<Expr>();
		for (Expr arg : MethodInfo.getOuterForLoopInvariantArgs()) {
			mapTreeArgs.add((Expr) arg.fullCopy());
		}
		mapTreeArgs.add(new VarAccess(Utils.varName_SpliceSet));
		MethodAccess mapTreeMacc = new MethodAccess(macc.getID() + Utils.methName_mapTreeSuffix, mapTreeArgs);
		Expr mapTreeExpr = maccLeft == null ? mapTreeMacc : new AbstractDot((Expr) maccLeft.fullCopy(), mapTreeMacc);
		splicePath.addStmt(new ExprStmt(mapTreeExpr));

		if (Globals.useAutotune) {
			entryBody.addStmt(new VariableDeclaration(new TypeAccess(Utils.typeName_Autotuner), Utils.varName_Autotuner,
					new ClassInstanceExpr(new TypeAccess(Utils.typeName_Autotuner), new List().add(condRight.fullCopy()))));			
			if (Globals.useBlock) {
				BlockTransform.addTuneBlockAndInterDec(entryBody);
				BlockTransform.transformAutotuneEntry(entryBody, macc, maccStmt, prologue, epilogue, indexName, loopVariantArgDecs, usedInEpilogueVarDecs, maccLeft);
			}
			else transformAutotuneEntry(entryBody, macc, maccStmt, prologue, epilogue, indexName, loopVariantArgDecs, usedInEpilogueVarDecs, maccLeft);
		} else {
			entryBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.typeName_SpliceSet),
					new MethodAccess(Utils.methName_SpliceSet_init, new List().add(condRight.fullCopy())))));
		}

		ForStmt spliceIncrementFor = new ForStmt();
		splicePath.addStmt(spliceIncrementFor);

		spliceIncrementFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_start", outerForInitStmt.getInit()));
		spliceIncrementFor.setCondition(new LTExpr(new VarAccess("_start"), (Expr) condRight.fullCopy()));
		spliceIncrementFor.addUpdateStmt(new ExprStmt(new AssignPlusExpr(new VarAccess("_start"),
				new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), new MethodAccess(Utils.methName_SpliceSet_workSize, new List())))));
		Block spliceIncrementForBody = new Block();
		spliceIncrementFor.setStmt(spliceIncrementForBody);

		spliceIncrementForBody.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_end",
				new ConditionalExpr(new LTExpr(new AddExpr(new VarAccess("_start"),
						new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), new MethodAccess(Utils.methName_SpliceSet_workSize, new List()))),
						(Expr) cond.getRightOperand().fullCopy()),
						new AddExpr(new VarAccess("_start"),
								new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), new MethodAccess(Utils.methName_SpliceSet_workSize, new List()))),
								(Expr) cond.getRightOperand().fullCopy())));

		spliceIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate prologue body
		 */
		ForStmt prologueFor = new ForStmt();
		prologueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName, new VarAccess("_start")));
		prologueFor.setCondition(new LTExpr(new VarAccess(indexName), new VarAccess("_end")));
		prologueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess(indexName))));
		Block prologueBody = new Block();
		prologueFor.setStmt(prologueBody);
		if (Globals.useAutotune) {
			prologueBody.addStmt(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			prologueBody.addStmt(vdec.fullCopy());
		}
		for (Stmt stmt : prologue) {
			prologueBody.addStmt((Stmt) stmt.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.saveInterState(vdec, "entry");
			MethodAccess saveMacc = new MethodAccess(methodName, new List<Expr>());
			saveMacc.addArg(new VarAccess(vdec.getID()));
			prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState), saveMacc)));
		}
		MethodAccess prologueMacc = macc.fullCopy();
		prologueMacc.setID(prologueMacc.getID() + Utils.methName_prologueSpliceSuffix);
		prologueMacc.addArg(new VarAccess(Utils.varName_InterState));
		Expr prologueMaccExpr = maccLeft == null ? prologueMacc : new AbstractDot((Expr) maccLeft.fullCopy(), prologueMacc);
		prologueBody.addStmt(new ExprStmt(prologueMaccExpr));
		prologueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		spliceIncrementForBody.addStmt(prologueFor);

		/*
		 * _spliceSet.doSplice();
		 * _interState.resetIndex();
		 */
		spliceIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
				new MethodAccess(Utils.methName_SpliceSet_doSplice, new List()))));
		spliceIncrementForBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_resetIndex, new List()))));

		/*
		 * generate epilogue method
		 */
		ForStmt epilogueFor = new ForStmt();
		epilogueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName, new VarAccess("_start")));
		epilogueFor.setCondition(new LTExpr(new VarAccess(indexName), new VarAccess("_end")));
		epilogueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess(indexName))));
		Block epilogueBody = new Block();
		epilogueFor.setStmt(epilogueBody);
		if (Globals.useAutotune) {
			epilogueBody.addStmt(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}
		for (VariableDeclaration vdec : loopVariantArgDecs) {
			epilogueBody.addStmt(vdec.fullCopy());
		}
		for (VariableDeclaration vdec : usedInEpilogueVarDecs) {
			String methodName = IntermediaryStateClasses.getLoadInterStateMethodName(vdec, "entry");
			MethodAccess loadMacc = new MethodAccess(methodName, new List<Expr>());
			VariableDeclaration loadVdec = new VariableDeclaration((Access) vdec.getTypeAccess().fullCopy(), vdec.getID(),
					new AbstractDot(new VarAccess(Utils.varName_InterState), loadMacc));
			epilogueBody.addStmt(loadVdec);
		}
		MethodAccess epilogueMacc = macc.fullCopy();
		epilogueMacc.setID(epilogueMacc.getID() + Utils.methName_epilogueSuffix);
		epilogueMacc.addArg(new VarAccess(Utils.varName_InterState));
		Stmt epilogueMaccStmt = (Stmt) maccStmt.fullCopy();
		epilogueMaccStmt.findMethodAccess(macc.getID()).swapWith(epilogueMacc);
		epilogueBody.addStmt(epilogueMaccStmt);
		for (Stmt stmt : epilogue) {
			epilogueBody.addStmt((Stmt) stmt.fullCopy());
		}
		epilogueBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_InterState),
				new MethodAccess(Utils.methName_IntermediaryState_incIndex, new List()))));
		spliceIncrementForBody.addStmt(epilogueFor);

		CompilationUnit cu = Utils.getEnclosingCompilationUnit(macc);
		addSpliceClassImports(cu);

		ForStmt basePath = Globals.outerFor.fullCopy();
		if (Globals.useAutotune) {
			Block basePathBody = (Block) basePath.getStmt();
			basePathBody.getStmt(0).insertStmtBefore(new IfStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner), 
					new MethodAccess(Utils.methName_Autotuner_isSampled, new List().add(new VarAccess(indexName)))), 
					new ContinueStmt()));
		}

		entryBody.addStmt(new IfStmt(new LogNotExpr(new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), 
				new MethodAccess(Utils.methName_SpliceSet_useSplice, new List()))),
				basePath, splicePath));
		Globals.outerFor.swapStmtWith(entryBody);
	}

	private static void transformAutotuneEntry(Block entryBody, MethodAccess macc, Stmt maccStmt, ArrayList<Stmt> prologue, ArrayList<Stmt> epilogue,
			String indexName, ArrayList<VariableDeclaration> loopVariantArgDecs, ArrayList<VariableDeclaration> usedInEpilogueVarDecs,
			Expr maccLeft) {
		entryBody.addStmt(new VariableDeclaration(
				new ArrayTypeAccess(new ArrayTypeAccess(new PrimitiveTypeAccess("int"))),
				"_tuneIndexes",
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tune", new List()))));
		ForStmt tuneOuterFor = new ForStmt();
		entryBody.addStmt(tuneOuterFor);

		tuneOuterFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_t", new IntegerLiteral(0)));
		tuneOuterFor.setCondition(new LTExpr(new VarAccess("_t"),
				new AbstractDot(new VarAccess("_tuneIndexes"), new VarAccess("length"))));
		tuneOuterFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess("_t"))));
		Block tuneOuterForBody = new Block();
		tuneOuterFor.setStmt(tuneOuterForBody);

		tuneOuterForBody.addStmt(new VariableDeclaration(
				new ArrayTypeAccess(new PrimitiveTypeAccess("int")),
				"_indexes",
				new AbstractDot(new VarAccess("_tuneIndexes"), new ArrayAccess(new VarAccess("_t")))));
		tuneOuterForBody.addStmt(new ExprStmt(
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneEntrySplice", new List()))));

		/*
		 * generate inner for
		 */
		ForStmt prologueFor = new ForStmt();
		prologueFor.addInitStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), "_tt", new IntegerLiteral(0)));
		prologueFor.setCondition(new LTExpr(new VarAccess("_tt"), new AbstractDot(new VarAccess("_indexes"), new VarAccess("length"))));
		prologueFor.addUpdateStmt(new ExprStmt(new PostIncExpr(new VarAccess("_tt"))));
		Block prologueBody = new Block();
		prologueFor.setStmt(prologueBody);
		prologueBody.addStmt(new VariableDeclaration(new PrimitiveTypeAccess("int"), indexName,
				new AbstractDot(new VarAccess("_indexes"), new ArrayAccess(new VarAccess("_tt")))));

		for (VariableDeclaration vdec : loopVariantArgDecs) {
			prologueBody.addStmt(vdec.fullCopy());
		}
		for (Stmt stmt : prologue) {
			prologueBody.addStmt((Stmt) stmt.fullCopy());
		}
		MethodAccess baseTuneMacc = macc.fullCopy();
		baseTuneMacc.setID(baseTuneMacc.getID() + Utils.methName_autotuneSuffix);
		baseTuneMacc.addArg(new VarAccess(Utils.varName_Autotuner));
		Stmt baseTuneMaccStmt = (Stmt) maccStmt.fullCopy();
		baseTuneMaccStmt.findMethodAccess(macc.getID()).swapWith(baseTuneMacc);
		prologueBody.addStmt(baseTuneMaccStmt);
		for (Stmt stmt : epilogue) {
			prologueBody.addStmt((Stmt) stmt.fullCopy());
		}

		tuneOuterForBody.addStmt(prologueFor);

		/*
		 * _autotuner.tuneExitSplice();
		 */
		tuneOuterForBody.addStmt(new ExprStmt(
				new AbstractDot(new VarAccess(Utils.varName_Autotuner), new MethodAccess("tuneExitSplice", new List()))));

		/*
		 * _autotuner.tuneFinished();
		 */
		entryBody.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_Autotuner),
				new MethodAccess(Utils.methName_Autotuner_tuneFinished, new List()))));
	}

	private static void transformSpliceRecursive() {
		MethodDecl mdec = MethodInfo.getRecursiveMdec();
		CompilationUnit cu = Utils.getEnclosingCompilationUnit(mdec);
		TypeDecl tdec = cu.getTypeDecl(0);
		ClassDecl cdec = (ClassDecl) tdec;

		Block body = new Block();
		ArrayList<ParameterDeclaration> loopVariantPdecs = MethodInfo.getRecursiveMethodLoopVariantParams();
		MethodDecl spliceMdec = new MethodDecl(mdec.getModifiers().fullCopy(),
				new PrimitiveTypeAccess("void"),
				mdec.getID() + Utils.methName_spliceSuffix,
				mdec.getParameters().fullCopy(),
				mdec.getExceptionList().fullCopy(),
				new Opt<Block>(body));
		cdec.addMemberMethod(spliceMdec);

		for (Stmt stmt : mdec.getBlock().getStmts()) {
			body.addStmt((Stmt) stmt.fullCopy());
		}	
		spliceMdec.renameMethodAccesses(mdec.getID(), mdec.getID() + Utils.methName_spliceSuffix);

		/*
		 * setup recursive call sets
		 */
		RecursiveCallSet.setSpliceSets(RecursiveCallSet.analysis(spliceMdec, loopVariantPdecs));

		MethodInfo.setupPdecNameToCallSetNumMap(mdec, RecursiveCallSet.getSpliceSets());

		/*
		 * choose optimization level, uses Splice RecursiveCallSets
		 */
		spliceOptimizationLevelAnalysis();

		/*
		 * add params, uses usePointIndex set from spliceOptimizationLevelAnalysis()
		 */
		if (Globals.usePointIndex) {
			spliceMdec.addParameter(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_PointIndex));
		}
		spliceMdec.addParameter(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_Depth));
		if (Globals.optimizationLevel != Globals.OptimizationLevel.NO_ORDER) {
			spliceMdec.addParameter(new ParameterDeclaration(new PrimitiveTypeAccess("int"), Utils.varName_Index));
		}
		spliceMdec.addParameter(new ParameterDeclaration(new TypeAccess(Utils.typeName_SpliceSet), Utils.varName_SpliceSet));


		if (Globals.optimizationLevel == Globals.OptimizationLevel.NO_ORDER) {
			/*
			 * _SpliceSet _spliceSet = _SpliceSet.getSpliceNode(_depth);
			 */
			body.addStmtStart(new VariableDeclaration(new TypeAccess(Utils.typeName_SpliceNode), Utils.varName_SpliceNode,
					new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
							new MethodAccess(Utils.methName_SpliceSet_getSpliceNode, new List<Expr>().add(
									new VarAccess(Utils.varName_Depth))))));
		} else {
			/*
			 * _SpliceSet _spliceSet = _SpliceSet.getSpliceNode(index);
			 */
			body.addStmtStart(new VariableDeclaration(new TypeAccess(Utils.typeName_SpliceNode), Utils.varName_SpliceNode,
					new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
							new MethodAccess(Utils.methName_SpliceSet_getSpliceNode, new List<Expr>().add(
									new VarAccess(Utils.varName_Index))))));
		}

		/*
		 * boolean _pointStopped = true;
		 */
		body.addStmtStart(new VariableDeclaration(new PrimitiveTypeAccess("boolean"), Utils.varName_PointStopped,
				new BooleanLiteral(true)));


		MethodAccess addMacc = new MethodAccess(Utils.methName_SpliceSet_add, new List());
		for (ParameterDeclaration pdec : loopVariantPdecs) {
			addMacc.addArg(new VarAccess(pdec.getID()));
		}
		if (Globals.usePointIndex) {
			addMacc.addArg(new VarAccess(Utils.varName_PointIndex));
		}

		/*
		 * add if (_pointStopped) ss.add(node); before returns
		 */
		ArrayList<ReturnStmt> returns = new ArrayList<ReturnStmt>();
		body.gatherReturns(returns);
		for (ReturnStmt stmt : returns) {
			stmt.insertStmtBefore(new IfStmt(new VarAccess(Utils.varName_PointStopped),
					new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_SpliceNode), addMacc.fullCopy()))));
		}

		/*
		 * add if (_pointStopped) ss.add(node); at end
		 */
		body.addStmt(new IfStmt(new VarAccess(Utils.varName_PointStopped),
				new ExprStmt(new AbstractDot(new VarAccess(Utils.varName_SpliceNode), addMacc.fullCopy()))));

		for (RecursiveCallSet rcs : RecursiveCallSet.getSpliceSets()) {
			Block callSetBlock = new Block();
			MethodAccess firstMacc = rcs.maccs.get(0);
			if (Globals.usePointIndex) {
				firstMacc.addArg(new VarAccess(Utils.varName_PointIndex));
			}
			firstMacc.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));

			Expr maccLeft = Utils.getDotLeft(firstMacc);
			Expr maccExpr = maccLeft == null ? firstMacc : new AbstractDot(maccLeft, firstMacc);

			Expr nextSpliceSet;
			if (Globals.optimizationLevel != Globals.OptimizationLevel.NO_ORDER) {
				RecursiveField rf = rcs.recursiveFields.get(0);
				firstMacc.addArg(new AddExpr(new MulExpr(new VarAccess(Utils.varName_Index), new IntegerLiteral(RecursiveField.totalSize)),
						new IntegerLiteral(rf.id)));
				// _SpliceSet.getSpliceNode(index * 2 + rf.id);
				nextSpliceSet = new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
						new MethodAccess(Utils.methName_SpliceSet_getSpliceNode,
								new List<Expr>().add(new AddExpr(new MulExpr(new VarAccess(Utils.varName_Index), new IntegerLiteral(RecursiveField.totalSize)),
										new IntegerLiteral(rf.id)))));
			} else {
				// _SpliceSet.getSpliceNode(_depth + 2)
				nextSpliceSet = new AbstractDot(new VarAccess(Utils.varName_SpliceSet),
						new MethodAccess(Utils.methName_SpliceSet_getSpliceNode,
								new List<Expr>().add(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)))));
			}
			firstMacc.addArg(new VarAccess(Utils.varName_SpliceSet));

			callSetBlock.addStmt(new IfStmt(
					new LTExpr(new VarAccess(Utils.varName_Depth), 
							new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), new MethodAccess(Utils.methName_SpliceSet_getDepthParam, new List()))),
							new ExprStmt(maccExpr),
							new ExprStmt(new AbstractDot(nextSpliceSet, addMacc.fullCopy()))));
			callSetBlock.addStmt(new ExprStmt(new AssignSimpleExpr(new VarAccess(Utils.varName_PointStopped), new BooleanLiteral(false))));
			if (MethodInfo.getRecursiveMethodAdditionalLoopVariantParams().size() > 0) {
				// add _SpliceSet.saveSpliceLocals(_pointIndex, _depth + 1, local1, local2);
				MethodAccess markSpliceSet = new MethodAccess(Utils.methName_SpliceSet_saveSpliceLocals, new List());
				markSpliceSet.addArg(new VarAccess(Utils.varName_PointIndex));
				markSpliceSet.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
				for (int i : MethodInfo.getRecursiveMethodAdditionalLoopVariantParamsIdxs()) {
					for (MethodAccess macc : rcs.maccs) {
						markSpliceSet.addArg((Expr) macc.getArg(i).fullCopy());
					}
				}
				callSetBlock.addStmt(new ExprStmt(new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), markSpliceSet)));
			}
			if (Globals.optimizationLevel == Globals.OptimizationLevel.GENERAL_ORDER) {	
				// add _SpliceSet.markSpliceState(_pointIndex, _depth + 1, 0);
				MethodAccess markSpliceSet = new MethodAccess(Utils.methName_SpliceSet_markSpliceState, new List());
				markSpliceSet.addArg(new VarAccess(Utils.varName_PointIndex));
				markSpliceSet.addArg(new AddExpr(new VarAccess(Utils.varName_Depth), new IntegerLiteral(1)));
				markSpliceSet.addArg(new IntegerLiteral(rcs.id));
				AbstractDot markSpliceSetDot = new AbstractDot(new VarAccess(Utils.typeName_SpliceSet), markSpliceSet);
				callSetBlock.addStmt(new ExprStmt(markSpliceSetDot));
			}
			rcs.stmt.swapStmtWith(callSetBlock);
			for (int i = 0; i < rcs.identicalStmts.size(); i++) {
				rcs.identicalStmts.get(i).swapStmtWith(callSetBlock.fullCopy());
			}
		}
	}

	private static Stmt findLargestCommonRecursionBlock(ArrayList<Stmt> recursionPointStmts) {
		ASTNode node = recursionPointStmts.get(0);
		while (true) {
			boolean common = true;
			for (int i = 1; i < recursionPointStmts.size(); i++) {
				if (!node.find(recursionPointStmts.get(i))) {
					common = false;
				}
			}
			if (common) {
				break;
			}
			node = node.getParent();
		}
		return Utils.getEnclosingStmt(node);
	}

	private static void addSpliceClassImports(CompilationUnit cu) {
		Utils.addImport(cu, Globals.blockPackageName + "." + Utils.typeName_SpliceNode);
		Utils.addImport(cu, Globals.blockPackageName + "." + Utils.typeName_SpliceSet);
	}
}
