/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import AST.ASTNode;
import AST.AbstractDot;
import AST.Access;
import AST.AddExpr;
import AST.ArrayAccess;
import AST.ArrayDecl;
import AST.AssignExpr;
import AST.AssignPlusExpr;
import AST.AssignSimpleExpr;
import AST.Block;
import AST.BodyDecl;
import AST.BooleanLiteral;
import AST.BreakStmt;
import AST.CastExpr;
import AST.ClassDecl;
import AST.ClassInstanceExpr;
import AST.CompilationUnit;
import AST.ContinueStmt;
import AST.Dot;
import AST.EQExpr;
import AST.EnhancedForStmt;
import AST.Expr;
import AST.ExprStmt;
import AST.FieldDeclaration;
import AST.ForStmt;
import AST.GTExpr;
import AST.IfStmt;
import AST.ImportDecl;
import AST.IntegerLiteral;
import AST.LTExpr;
import AST.List;
import AST.LogNotExpr;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Modifiers;
import AST.MulExpr;
import AST.NEExpr;
import AST.Opt;
import AST.PackageAccess;
import AST.ParExpr;
import AST.ParameterDeclaration;
import AST.PostIncExpr;
import AST.PrimitiveTypeAccess;
import AST.ReturnStmt;
import AST.SimpleSet;
import AST.SingleTypeImportDecl;
import AST.Stmt;
import AST.SubExpr;
import AST.TypeAccess;
import AST.TypeDecl;
import AST.VarAccess;
import AST.VariableDeclaration;
import AST.WhileStmt;

public class TransformMain {
	/**
	 * if recursive structure found, transform code
	 */
	public static void process() {
		
		if (Globals.mainPath.isEmpty()) return;

		//Globals.useAutotune = false;
		//Globals.useBlock = false;
		//Globals.useSplice = false;
		
		/*
		
		RecursiveField.finalizeFields();
		
		Globals.numMethods = Globals.mainPath.size();
		
		MethodInfo.analyze();

		if (Globals.useSplice && Globals.useBlock) {
			BlockTransform.transform();	
			SpliceTransform.transform();
			SpliceClasses.generateSpliceClasses();
			BlockClasses.generateBlockClasses();
		} else if (Globals.useSplice) {
			SpliceTransform.transform();	
			SpliceClasses.generateSpliceClasses();
		} else {
			BlockTransform.transform();
			BlockClasses.generateBlockClasses();			
		}
		
		// CommonTransform.transformAutotuneRecursive_Splice uses info from SpliceTransform
		CommonTransform.transform();
		
		IntermediaryStateClasses.generate();
		
		if (Globals.useAutotune) {
			AutotunerClasses.generate();
		}
		
		*/
	}
}

