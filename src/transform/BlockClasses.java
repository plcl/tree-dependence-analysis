/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import AST.Access;
import AST.List;
import AST.MethodDecl;
import AST.ParameterDeclaration;

public class BlockClasses extends ClassHelpers {

	public static void generateBlockClasses() {
		try {
			generateBlockClass();
			generateBlockStackClass();
			generateBlockSetClass();
			if (Globals.usePointClass) {
				generatePointClass();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void generateBlockClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;

		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_Block + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));

		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		if (Globals.useUtilStats) {
			out.print("import util.Launcher;\n");
		}
		out.print("import java.util.Random;\n" +
				"public class " + Utils.typeName_Block + " {\n");
		if (Globals.useUtilStats) {
			out.print("	public static int maxBlockSize = Launcher.getBlockSize();\n");
		} else {
			out.print("	public static int maxBlockSize = " + Globals.blockSizeParam + ";\n");	
		}

		MethodDecl mdec = MethodInfo.getRecursiveMdec();
		ArrayList<ParameterDeclaration> loopVariantParams = MethodInfo.getRecursiveMethodLoopVariantParams();
		ArrayList<ParameterDeclaration> loopInvariantParams = MethodInfo.getRecursiveMethodLoopInvariantParams();
		ArrayList<ParameterDeclaration> additionalLoopVariantParams = MethodInfo.getRecursiveMethodAdditionalLoopVariantParams();
		HashMap<String, Integer> pdecNameToCallSetNumMap = MethodInfo.getPdecNameToCallSetNumMap();
		ArrayList<ParameterDeclaration> allLoopVariantParams = MethodInfo.getRecursiveMethodAllLoopVariantParams();
		ArrayList<ParameterDeclaration> allLoopInvariantParams = MethodInfo.getRecursiveMethodAllLoopInvariantParams();
		ArrayList<ParameterDeclaration> allLoopInvariantParamsIncludingNode = MethodInfo.getRecursiveMethodAllLoopInvariantParamsIncludingNode();
		
		if (Globals.usePointClass) {
			out.print("	public final _Point [] _points;\n");
		} else {
			for (ParameterDeclaration pdec : allLoopVariantParams) {
				Integer numCallSets = pdecNameToCallSetNumMap.get(pdec.getID());
				if (numCallSets == null) {
					out.printf("	public final %s [] %s;\n", pdec.getTypeAccess(), pdec.getID());
				} else {
					for (int i = 0; i < numCallSets; i++) {
						out.printf("	public final %s [] %s%d;\n", pdec.getTypeAccess(), pdec.getID(), i);
					}
				}
			}
		}

		out.print(
				"	public int size;\n" +
				"\n" +
				"	public " + Utils.typeName_Block + "() {\n" +
				"		this(maxBlockSize);\n" +
				"	}\n" +
				"\n" +
				"	public " + Utils.typeName_Block + "(int initSize) {\n");

		if (Globals.usePointClass) {
			out.print("		_points = new _Point[initSize];\n" +
					"		for (int i = 0; i < initSize; i++) {\n" +
			"			_points[i] = new _Point();\n		}\n");
		} else {
			for (ParameterDeclaration pdec : allLoopVariantParams) {
				Integer numCallSets = pdecNameToCallSetNumMap.get(pdec.getID());
				if (numCallSets == null) {
					out.printf("		%s = new %s[initSize];\n", pdec.getID(), pdec.getTypeAccess());
				} else {
					for (int i = 0; i < numCallSets; i++) {
						out.printf("		%s%d = new %s[initSize];\n", pdec.getID(), i, pdec.getTypeAccess());
					}
				}
			}
		}
		out.print(
				"		size = 0;\n" +
		"	}\n");
		
		/*
		 * add method without additional loop variant params
		 */
		if (!additionalLoopVariantParams.isEmpty()) {
			out.print("	public void add(");
			ArrayList<String> types = new ArrayList<String>();
			ArrayList<String> names = new ArrayList<String>();
			for (int i = 0; i < loopVariantParams.size(); i++) {
				ParameterDeclaration pdec = loopVariantParams.get(i);
				Integer numCallSets = pdecNameToCallSetNumMap.get(pdec.getID());
				if (numCallSets == null) {
					types.add(String.format("%s", pdec.getTypeAccess()));
					names.add(String.format("%s", pdec.getID()));
				} else {
					for (int j = 0; j < numCallSets; j++) {
						types.add(String.format("%s", pdec.getTypeAccess()));
						names.add(String.format("%s%d", pdec.getID(), j));
					}
				}
			}
			for (int i = 0; i < names.size(); i++) {
				out.printf("%s %s", types.get(i), names.get(i));
				if (i < names.size() - 1) out.print(", ");
			}

			out.print(") {\n");
			if (Globals.usePointClass) {
				out.print("		_Point _point = _points[size];\n");
				for (int i = 0; i < names.size(); i++) {
					out.format("		_point.%s = %s;\n", names.get(i), names.get(i));
				}
			} else {
				for (int i = 0; i < names.size(); i++) {
					out.format("		this.%s[size] = %s;\n", names.get(i), names.get(i));
				}
			}
			out.print("		size++;\n	}\n");	
		}

		/*
		 * add method
		 */
		out.print("	public void add(");
		ArrayList<String> types = new ArrayList<String>();
		ArrayList<String> names = new ArrayList<String>();
		for (int i = 0; i < allLoopVariantParams.size(); i++) {
			ParameterDeclaration pdec = allLoopVariantParams.get(i);
			Integer numCallSets = pdecNameToCallSetNumMap.get(pdec.getID());
			if (numCallSets == null) {
				types.add(String.format("%s", pdec.getTypeAccess()));
				names.add(String.format("%s", pdec.getID()));
			} else {
				for (int j = 0; j < numCallSets; j++) {
					types.add(String.format("%s", pdec.getTypeAccess()));
					names.add(String.format("%s%d", pdec.getID(), j));
				}
			}
		}
		for (int i = 0; i < names.size(); i++) {
			out.printf("%s %s", types.get(i), names.get(i));
			if (i < names.size() - 1) out.print(", ");
		}

		out.print(") {\n");
		if (Globals.usePointClass) {
			out.print("		_Point _point = _points[size];\n");
			for (int i = 0; i < names.size(); i++) {
				out.format("		_point.%s = %s;\n", names.get(i), names.get(i));
			}
		} else {
			for (int i = 0; i < names.size(); i++) {
				out.format("		this.%s[size] = %s;\n", names.get(i), names.get(i));
			}
		}
		out.print("		size++;\n	}\n");			

		out.print(
				"\n" +
				"	public void recycle() {\n" +
				"		size = 0;\n" +
				"	}\n" +
				"\n" +
				"	public boolean isEmpty() {\n" +
				"		return size == 0;\n" +
				"	}\n" +
				"	public static boolean useBlock() {\n" +
				"		return maxBlockSize > 0;\n" +
				"	}\n" +
				"	\n");
		
		/*
		 * flushBlock
		 */
		boolean leftRoot = Globals.isImplicitRecursiveStructure || !Globals.isStaticRecursiveMethod;
		out.printf("	public void flushBlock() %s {\n", getThrows());
		out.print("		_BlockStack _stack = _BlockStack.getInstance();\n" +
		"		_stack.get(0).block = this;\n");
		ArrayList<String> args = new ArrayList<String>();
		args.addAll(getAllLoopInvariantArgsIncludingNodeWithPrefix("root_"));
		args.add("_stack");
		args.add("0");
		if (MethodInfo.getPdecNameToCallSetNumMap().size() > 0) {
			args.add("0");
		} 
		out.printf("		%s.%s(%s);\n",
				leftRoot ? "root_" : MethodInfo.getRecursiveCdec().getID(),
				MethodInfo.getRecursiveMdec().getID(), concat(args));
		out.print("		recycle();\n" +
		"	}\n");

		if (Globals.useAutotune) {
			out.printf("	public void flushBlockAutotune(_Autotuner _autotuner) %s {\n", getThrows());
			out.print("		_BlockStack _stack = _BlockStack.getInstance();\n" +
			"		_stack.get(0).block = this;\n");
			args.add("_autotuner");
			out.printf("		%s.%s%s(%s);\n",
					leftRoot ? "root_" : MethodInfo.getRecursiveCdec().getID(),
					MethodInfo.getRecursiveMdec().getID(), Utils.methName_autotuneSuffix, concat(args));
			out.print("		recycle();\n" +
			"	}\n");
		}
		
		/*
		 * mapRoot
		 */
		for (ParameterDeclaration pdec : allLoopInvariantParamsIncludingNode) {
			out.printf("	private static %s root_%s;\n", pdec.getTypeAccess(), pdec.getID());
		}
		if (leftRoot) {
			out.printf("	private static %s root_;\n", MethodInfo.getRecursiveCdec().getID());
		}
		out.printf("	public static void mapRoot(");
		if (leftRoot) {
			out.printf("%s root__", MethodInfo.getRecursiveCdec().getID());
			if (allLoopInvariantParamsIncludingNode.size() > 0) {
				out.print(", ");
			}
		}
		for (int i = 0; i < allLoopInvariantParamsIncludingNode.size(); i++) {
			ParameterDeclaration pdec = allLoopInvariantParamsIncludingNode.get(i);
			out.printf("%s %s", pdec.getTypeAccess(), pdec.getID());
			if (i < allLoopInvariantParamsIncludingNode.size() - 1) {
				out.print(", ");
			}
		}
		out.print(") {\n");
		if (leftRoot) {
			out.print("		root_ = root__;\n");
		}
		for (ParameterDeclaration pdec : allLoopInvariantParamsIncludingNode) {
			out.printf("		root_%s = %s;\n", pdec.getID(), pdec.getID());
		}
		out.print("	}\n");
		
		/*
		 * ThreadLocal instances per call set
		 */
		for (int i = 0; i < RecursiveCallSet.getBlockSets().size(); i++) {
			if (!Globals.useSplice && i > 0) break;
			out.format("	static ThreadLocal<_Block> instance%d = new ThreadLocal<_Block>();\n", i);
			out.format("	public static _Block getInstance%d() {\n", i);
			out.format("		_Block ret = instance%d.get();\n", i);
			out.print("		if (ret == null) {\n" +
			"			ret = new _Block();\n");
			out.format("			instance%d.set(ret);\n", i);
			out.print("		}\n" +
					"		ret.recycle();\n" +
					"		return ret;\n" +
			"	}\n");
		}

		out.print("	public static void resetInstance() {\n");
		for (int i = 0; i < RecursiveCallSet.getBlockSets().size(); i++) {
			if (!Globals.useSplice && i > 0) break;
			out.format("		instance%d = new ThreadLocal<_Block>();\n", i);
		}
		out.print("	}\n");

		out.print("}\n");	  
	}	

	public static void generateBlockStackClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_BlockStack + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n" +
				"import java.util.ArrayList;\n" +
				"\n" +
				"public class _BlockStack {\n" +
				"	ArrayList<_BlockSet> depths = new ArrayList<_BlockSet>();\n" +
				"	\n" +
				"	static ThreadLocal<_BlockStack> threadLocal = new ThreadLocal<_BlockStack>();\n" +
				"	\n" +
				"	public static _BlockStack getInstance() {\n" +
				"		_BlockStack ret = threadLocal.get();\n" +
				"		if (ret == null) {\n" +
				"			ret = new _BlockStack();\n" +
				"			threadLocal.set(ret);\n" +
				"		}\n" +
				"		return ret;\n" +
				"	}\n" +
				"	\n" +
				"	public static void resetInstance() {\n" +
				"		threadLocal = new ThreadLocal<_BlockStack>();\n" +
				"	}\n" +
				"	\n" +
				"	public _BlockSet get(int i) {\n" +
				"		_BlockSet ret;\n" +
				"		while (i >= depths.size()) {\n" +
				"			depths.add(new _BlockSet());\n" +
				"		}\n" +
				"		ret = depths.get(i);\n" +
				"		return ret;\n" +
				"	}\n" +
		"}\n");
	}

	public static void generateBlockSetClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		ArrayList<RecursiveCallSet> recursiveCallSets = RecursiveCallSet.getBlockSets();
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_BlockSet + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n" +
				"public class _BlockSet {\n" +
		"	public _Block block;\n");
		for (int i = 0; i < recursiveCallSets.size(); i++) {
			out.print("	public _Block nextBlock" + i + " = new _Block();\n");
		}
		out.print("}  \n");
	}

	public static void generatePointClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_Point + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);

		HashMap<String, Integer> pdecNameToCallSetNumMap = MethodInfo.getPdecNameToCallSetNumMap();
		ArrayList<ParameterDeclaration> allLoopVariantParams = MethodInfo.getRecursiveMethodAllLoopVariantParams();

		out.print("public class _Point {\n");
		for (ParameterDeclaration pdec : allLoopVariantParams) {
			Integer numCallSets = pdecNameToCallSetNumMap.get(pdec.getID());
			if (numCallSets == null) {
				out.printf("	public %s %s;\n", pdec.getTypeAccess(), pdec.getID());
			} else {
				for (int i = 0; i < numCallSets; i++) {
					out.printf("	public %s %s%d;\n", pdec.getTypeAccess(), pdec.getID(), i);
				}
			}
		}		
		out.print("}  \n");
	}
}
