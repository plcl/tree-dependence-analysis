/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import AST.ASTNode;
import AST.AbstractDot;
import AST.Block;
import AST.ExprStmt;
import AST.List;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Opt;
import AST.ParameterDeclaration;
import AST.Stmt;
import AST.VarAccess;

public class RecursiveCallSet {
	public Stmt stmt;
	public ArrayList<Stmt> identicalStmts = new ArrayList<Stmt>();

	// MethodAccess and corresponding RecursiveField in call order
	public final ArrayList<MethodAccess> maccs = new ArrayList<MethodAccess>();
	public final ArrayList<RecursiveField> recursiveFields = new ArrayList<RecursiveField>();
	public int id;

	/*
	 * maintain separate sets for block and splice, as they will refer to different maccs within reach recursive method
	 */
	private static ArrayList<RecursiveCallSet> blockSets = new ArrayList<RecursiveCallSet>();
	private static ArrayList<RecursiveCallSet> spliceSets = new ArrayList<RecursiveCallSet>();

	public static ArrayList<RecursiveCallSet> getBlockSets() {
		return blockSets;
	}
	public static ArrayList<RecursiveCallSet> getSpliceSets() {
		return spliceSets;
	}
	public static void setBlockSets(ArrayList<RecursiveCallSet> sets) {
		blockSets = sets;
	}
	public static void setSpliceSets(ArrayList<RecursiveCallSet> sets) {
		spliceSets = sets;
	}

	public RecursiveCallSet(Stmt s, int id) {
		stmt = s;
		this.id = id;
	}

	public void addMethodAccess(MethodAccess macc) {
		maccs.add(macc);
		VarAccess vacc = getNodeVacc(macc);
		RecursiveField rf = RecursiveField.findField(vacc.getID());
		assert rf != null;
		recursiveFields.add(rf);
	}

	public static ArrayList<RecursiveCallSet> analysis(MethodDecl mdec, ArrayList<ParameterDeclaration> loopVariantPdecs) {
		ArrayList<RecursiveCallSet> sets = new ArrayList<RecursiveCallSet>();
		int nextId = 0;
		/*
		 * propagate loop variant args
		 */
		Set<VarAccess> loopVariantArgsPropagation = new HashSet<VarAccess>();
		Set<Stmt> loopVariantControlStmts = new HashSet<Stmt>();
		for (ParameterDeclaration pdec : loopVariantPdecs) {
			loopVariantArgsPropagation.add(new VarAccess(pdec.getID()));
		}
		ArrayList<MethodAccess> recursionPoints = Utils.getRecursionPoints(mdec);
		ArrayList<Stmt> recursionPointStmts = new ArrayList<Stmt>();
		for (MethodAccess macc : recursionPoints) {
			recursionPointStmts.add(Utils.getEnclosingStmt(macc));
		}
		Utils.gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, mdec.getBlock(), recursionPointStmts);

		/*
		 * traverse bottom up from recursion points and setup recursive call sets
		 */
		HashMap<Stmt, RecursiveCallSet> map = new HashMap<Stmt, RecursiveCallSet>();
		for (MethodAccess macc : recursionPoints) {
			ASTNode node;
			node = macc.getParent();
			ASTNode prevNode = macc;
			while (node.getParent() != null && !loopVariantControlStmts.contains(node.getParent())) {
				prevNode = node;
				node = node.getParent();
			}
			if (node instanceof Opt || node instanceof List) {
				node = prevNode;
			}
			Stmt stmt = (Stmt) node;
			RecursiveCallSet rcs = map.get(stmt);
			if (rcs == null) {
				rcs = new RecursiveCallSet(stmt, nextId++);
				sets.add(rcs);
				map.put(stmt, rcs); 				
			}
			rcs.addMethodAccess(macc);
		}

		/*
		 * merge identical call sets
		 */
		ArrayList<RecursiveCallSet> uniqueSets = new ArrayList<RecursiveCallSet>();
		for (RecursiveCallSet rcs : sets) {
			RecursiveCallSet identicalRcs = null;
			for (RecursiveCallSet rcs2 : uniqueSets) {
				if (rcs.stmt.toString().equals(rcs2.stmt.toString())) {
					identicalRcs = rcs2;
					break;
				}
			}
			if (identicalRcs == null) {
				uniqueSets.add(rcs); 
			} else {
				identicalRcs.identicalStmts.add(rcs.stmt);
			}
		}
		return uniqueSets;
	}

	public static VarAccess getNodeVacc(MethodAccess macc) {
		VarAccess ret = null;
		if (Globals.isImplicitRecursiveStructure) {
			AbstractDot dot = Utils.getEnclosingAbstractDot(macc);
			ret = (VarAccess) dot.getLeft();
		} else {
			VarAccess vacc = macc.findVarAccess(Globals.nodeParam.getID());
			AbstractDot dot = Utils.getEnclosingAbstractDot(vacc);
			ret = (VarAccess) dot.getRight();
		}
		return ret;
	}
}
