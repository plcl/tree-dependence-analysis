/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import AST.ParameterDeclaration;

public class AutotunerClasses {
	public static void generate() {
		try {
			generateAutotunerClass();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void generateAutotunerClass() throws FileNotFoundException {
		String outName = Globals.blockPathName;
		String packageName = Globals.blockPackageName;
		outName = Globals.outputDir + "/" + outName + "/" + Utils.typeName_Autotuner + ".java";
		PrintStream out = new PrintStream(new FileOutputStream(outName, false));
		out.print("package " +  packageName + ";\n");
		Utils.generateImports(out, packageName);
		out.print("import java.util.Random;\n" +
				"import util.Launcher;\n");
		if (Globals.useSplice) out.print("import java.util.HashSet;\n");
				out.print("\n" +
				"public class _Autotuner {\n" +
				"	\n" +
				"	final private static int startBlockSize = 8;\n" +
				"	final private static int maxTuneCnt = 5;\n" +
				"	final private static int tuneFraction = 100;	\n" +
				"	\n" +
				"	private int numPoints;\n" +
				"	private Random rand = new Random();\n" +
				"	private int samplePoint;	\n" +
				"	private boolean [] sampled;\n" +
				"\n" +
				"	public _Autotuner(int num) {\n" +
		"		tuneSetup(num);\n");
		if (Globals.useBlock) out.print("		tuneSetupBlock();\n");
		if (Globals.useSplice) out.print("		tuneSetupSplice();\n");
		out.print("		resetInstances();\n" +
				"	}\n" +
		"	\n");
		if (Globals.useBlock) {
			out.print("	public int [][] tune() {\n" +
					"		int [][] tuneIndexes = new int[maxTuneCnt * blockSizes.length][];\n" +
					"		for (int i = 0; i < maxTuneCnt; i++) {\n" +
					"			for (int j = 0; j < blockSizes.length; j++) {\n" +
					"				int size = blockSizes[j];\n" +
					"				setupSample(tuneIndexes, i * blockSizes.length + j, size);\n" +
					"			}\n" +
					"		}\n" +
					"		return tuneIndexes;\n" +
			"	}\n");
		} else {
			out.print("	public int [][] tune() {\n" +
					"		int [][] tuneIndexes = new int[maxTuneCnt][];\n" +
					"		for (int i = 0; i < maxTuneCnt; i++) {\n" +
					"			setupSample(tuneIndexes, i, 2);\n" +
					"		}\n" +
					"		return tuneIndexes;\n" +
			"	}\n");
		}
		out.print("	\n" +
		"	public void tuneFinished() {\n");
		if (Globals.useBlock) {
			out.print("		float minTime = Float.MAX_VALUE;\n" +
					"		int bestIndex = 0;\n" +
					"		for (int j = 0; j < blockSizes.length; j++) {\n" +
					"			if (normalizedTimes[j] < minTime) {\n" +
					"				minTime = normalizedTimes[j];\n" +
					"				bestIndex = j;\n" +
					"			}\n" +
					"		}\n" +
					"		System.out.printf(\"autotune block size: %d\\n\", blockSizes[bestIndex]);\n" +
					"		_Block.maxBlockSize = blockSizes[bestIndex] == 1 ? 0 : blockSizes[bestIndex];\n" +
			"		Launcher.recordBlockSize(_Block.maxBlockSize);\n");
		}
		if (Globals.useSplice) {
			out.print("		_SpliceSet.init(numPoints, getAvgConvergence(), getAvgReach());\n");
		}
		out.print("	}\n" +
				"	\n" +
				"	public boolean isSampled(int i) {\n" +
				"		return sampled[i];\n" +
				"	}\n" +
				"	\n" +
				"	private void setSampled(int i) {\n" +
				"		sampled[i] = true;\n" +
				"	}\n" +
				"	\n" +
				"	private void setupSample(int [][] indexes, int idx, int size) {\n" +
				"		indexes[idx] = new int[size];\n" +
				"		samplePoint = rand.nextInt(numPoints);\n" +
				"		for (int i = 0; i < size; i++) {\n" +
				"			nextSample();\n" +
				"			indexes[idx][i] = samplePoint;\n" +
				"		}\n" +
				"	}\n" +
				"	\n" +
				"	private void nextSample() {\n" +
				"		samplePoint++;\n" +
				"		if (samplePoint == numPoints) {\n" +
				"			samplePoint = 0;\n" +
				"		}\n" +
				"		while (isSampled(samplePoint)) {\n" +
				"			samplePoint++;\n" +
				"			if (samplePoint == numPoints) {\n" +
				"				samplePoint = 0;\n" +
				"			}\n" +
				"		}\n" +
				"		setSampled(samplePoint);\n" +
				"	}\n" +
				"	\n" +
				"	public static void resetInstances() {\n");
		if (Globals.useBlock) {
				out.print("		_Block.resetInstance();\n" +
				"		_BlockStack.resetInstance();\n");
		}
				out.print("		_IntermediaryState.resetInstance();\n" +
				"	}\n" +
				"\n" +
				"	public void tuneSetup(int num) {\n" +
				"		numPoints = num;\n" +
				"		sampled = new boolean[numPoints];\n" +
				"	}\n");
		


		if (Globals.useBlock) gen_BlockSpecific(out);
		if (Globals.useSplice) gen_SpliceSpecific(out);
		out.print("}\n");
	}

	private static void gen_BlockSpecific(PrintStream out) {
		out.print("	// Block specific\n" +
				"	\n" +
				"	private int workDone = 0;\n" +
				"	private long start;\n" +
				"	private int maxBlockSize;\n" +
				"	private float [] normalizedTimes;\n" +
				"	private int [] blockSizes;\n" +
				"	\n" +
				"	public void tuneSetupBlock() {\n" +
				"		workDone = 0;\n" +
				"		int limit = numPoints / (tuneFraction * maxTuneCnt * 2);\n" +
				"		maxBlockSize = startBlockSize;\n" +
				"		int cnt = 1;\n" +
				"		while (maxBlockSize * 2 < limit) {\n" +
				"			maxBlockSize *= 2;\n" +
				"			cnt++;\n" +
				"		}\n" +
				"		normalizedTimes = new float[cnt + 1];\n" +
				"		blockSizes = new int[cnt + 1];\n" +
				"		for (int i = 0, size = startBlockSize; i < cnt; i++, size *= 2) {\n" +
				"			blockSizes[i] = size;\n" +
				"		}\n" +
				"		blockSizes[cnt] = 1;\n" +
				"		_Block.maxBlockSize = maxBlockSize;\n" +
				"	}\n" +
				"\n" +
				"	public void profileWorkDone(int work) {\n" +
				"		workDone += work;\n" +
				"	}\n" +
				"	\n" +
				"	public void tuneEntryBlock() {\n" +
				"		workDone = 0;\n" +
				"		start = System.nanoTime();\n" +
				"	}\n" +
				"\n" +
				"	public void tuneExitBlock(int i) {\n" +
				"		i = i % blockSizes.length;\n" +
				"		start = System.nanoTime() - start;\n" +
				"		float normalizedTime = (float) start / workDone;\n" +
				"		normalizedTimes[i] += normalizedTime;\n" +
				"	}\n");
	}

	private static void gen_SpliceSpecific(PrintStream out) {
		String nodeClass = RecursiveField.recursiveClass.getID();
		String pointClass = MethodInfo.getRecursiveMethodAllLoopVariantParams().get(0).getTypeAccess().toString();
		out.print("	// Splice specific\n" +
				"	\n" +
		"	private int profileDepthSum, profileDepthCnt;\n");
		out.printf("	private HashSet<%s> convergenceSet1, convergenceSet2;\n", nodeClass);
		out.printf("	private %s convergencePoint1, convergencePoint2;\n", pointClass);
		out.print("	private float convergenceSum;\n" +
				"	private int convergenceCnt;\n" +
				"	\n" +
				"	public float getAvgConvergence() {\n" +
				"		return convergenceSum / convergenceCnt;\n" +
				"	}\n" +
				"\n" +
				"	public float getAvgReach() {\n" +
				"		return (float) profileDepthSum / profileDepthCnt;	\n" +
				"	}\n" +
				"\n" +
				"	public void profileDepth(int depth) {\n" +
				"		profileDepthSum += depth;\n" +
				"		profileDepthCnt++;\n" +
				"	}\n" +
				"\n" +
				"	public void tuneSetupSplice() {\n" +
				"		profileDepthSum = 0;\n" +
		"		profileDepthCnt = 0;\n");
		out.printf("		convergenceSet1 = new HashSet<%s>();\n", nodeClass);
		out.printf("		convergenceSet2 = new HashSet<%s>();\n", nodeClass);
		out.print("		convergenceSum = 0;\n" +
				"		convergenceCnt = 0;\n" +
				"	}\n" +
				"\n" +
				"	public void tuneEntrySplice() {\n" +
				"		convergenceSet1.clear();\n" +
				"		convergenceSet2.clear();\n" +
				"		convergencePoint1 = null;\n" +
				"		convergencePoint2 = null;\n" +
				"	}\n" +
				"\n" +
				"	public void tuneExitSplice() {\n" +
				"		if (convergenceSet2.size() == 0) return;\n" +
		"		int same = 0;\n");
		out.printf("		for (%s o : convergenceSet1) {\n", nodeClass);
		out.print("			if (convergenceSet2.contains(o)) {\n" +
				"				same++;\n" +
				"			}\n" +
				"		}\n" +
				"		float avgSize = (float) (convergenceSet1.size() + convergenceSet2.size()) / 2;\n" +
				"\n" +
				"		convergenceSum += ((float) same / avgSize);\n" +
				"		convergenceCnt++;\n" +
				"	}\n" +
		"\n");
		out.printf("	public void profileConvergence(%s point, %s node) {\n", pointClass, nodeClass);
		out.print("		if (convergencePoint1 == null) {\n" +
				"			convergencePoint1 = point;\n" +
				"		} else if (convergencePoint2 == null && point != convergencePoint1) {\n" +
				"			convergencePoint2 = point;\n" +
				"		}\n" +
				"		if (convergencePoint1 == point) {\n" +
				"			convergenceSet1.add(node);\n" +
				"		} else if (convergencePoint2 == point) {\n" +
				"			convergenceSet2.add(node);\n" +
				"		}\n" +
				"	}\n");
	}
}
