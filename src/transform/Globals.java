/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import AST.ClassDecl;
import AST.FieldDecl;
import AST.FieldDeclaration;
import AST.ForStmt;
import AST.IfStmt;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.ParameterDeclaration;
import AST.Stmt;

public class Globals {
	// path of methods to recursive call
	public static ArrayList<MethodAccess> mainPath = new ArrayList<MethodAccess>();
		
	public static int numMethods;
		
	// the node parameter to the transformed (block) recursive method declaration
	// not used if isImplicitRecursiveStructure == true
	public static ParameterDeclaration nodeParam = null;
	public static int nodeParamIdx;
	
	// the outer for loop with entry to path to recursive method
	public static ForStmt outerFor = null;
	
	public static String blockPackageName = "";
	public static String blockPathName = "";
	public static HashSet<String> packageNames = new HashSet<String>();
	
	/*
	 * set in RecursiveStructureAnalysis.findRecursiveStructure
	 */
	public static boolean isImplicitRecursiveStructure;
	
	/*
	 * set in BlockTransfrom.transformMapRootRecursive
	 */
	public static boolean isStaticRecursiveMethod;
	
	public enum OptimizationLevel {
		GENERAL_ORDER, NODE_BASED_ORDER, NO_ORDER 
	}
	public static OptimizationLevel optimizationLevel;	
	
	// spliceOptimizationLevelAnalysis() will set this if
	// 1) optimizationLevel is GENERAL_ORDER or
	// 2) hasSplicePersistents is set to true
	public static boolean usePointIndex = false;
	
	// spliceOptimizationLevelAnalysis() will set this if
	// there are intermediary local vars declared in prologue and used in epilogue
	// e.g. blockDatas has entities of BlockDataType LOCAL_VAR
	public static boolean hasSplicePersistents = false;
	
	// set to true if MethodInfo.getRecursiveMethodloopVariantParams().size() > 1
	public static boolean usePointClass;

	
	// flags set with command line args; these are default values
	public static boolean useAutotune = true;
	public static boolean useBlock = true;
	public static boolean useSplice = true;
	public static boolean useElision = true;
	public static int maxOptLevel = 2;
	public static int blockSizeParam = 0;
	public static int spliceDepthParam = 0;
	public static String outputDir = "output";
	public static boolean useUtilStats = false;
	public static boolean useUtilProfiler = false;
	
	// force params while using autotuning, for debugging
	public static final boolean forceBlockSizeParam = false;
	public static final boolean skipSpliceIfSorted = false;
}
