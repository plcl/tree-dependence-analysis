/*
TreeSplicer, a framework to enhance temporal locality in repeated tree traversals.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2012, School of Electrical and Computer Engineering, Purdue University.
 */

package transform;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import AST.ASTNode;
import AST.AbstractDot;
import AST.Access;
import AST.ArrayAccess;
import AST.AssignExpr;
import AST.AssignSimpleExpr;
import AST.Block;
import AST.CastExpr;
import AST.CompilationUnit;
import AST.Dot;
import AST.EQExpr;
import AST.EnhancedForStmt;
import AST.Expr;
import AST.ExprStmt;
import AST.ForStmt;
import AST.IfStmt;
import AST.ImportDecl;
import AST.IntegerLiteral;
import AST.LTExpr;
import AST.List;
import AST.MethodAccess;
import AST.MethodDecl;
import AST.Opt;
import AST.PackageAccess;
import AST.ParExpr;
import AST.ParameterDeclaration;
import AST.PostIncExpr;
import AST.PrimitiveTypeAccess;
import AST.SingleTypeImportDecl;
import AST.Stmt;
import AST.TypeAccess;
import AST.VarAccess;
import AST.VariableDeclaration;

public class Utils {

	public final static String varName_Block = "_block";
	public final static String varName_NextBlock = "_nextBlock";
	public final static String varName_Blocks = "_blocks";
	public final static String varName_Block_Size = "size";
	public final static String varName_Block_MaxSize = "maxBlockSize";
	public final static String varName_Block_SampleSize = "sampleBlockSize";
	public final static String varName_Block_SamplePoint = "samplePoint";
	public final static String varName_Block_Data = "data_";
	public final static String varName_Block_Tuning = "tuning";
	public final static String varName_Block_WorkDone = "workDone";
	public final static String varName_Block_Valid = "valid";
	public final static String varName_Block_Points = "_points";
	public final static String varName_Block_PointIndex = "pointIndex";
	public final static String varName_BlockIndex = "_blockIndex";
	public final static String varName_BlockStack = "_stack";
	public final static String varName_BlockSet = "_set";
	public final static String varName_BlockSet_Block = "block";
	public final static String varName_BlockSet_NextBlock = "nextBlock";

	public final static String varName_Depth = "_depth";
	public final static String varName_Index = "_index";  // e.g. nodeIndex
	public final static String varName_Point = "_point";
	public final static String varName_PointStopped = "_pointStopped";
	public final static String varName_SpliceSet = "_spliceSet";
	public final static String varName_SpliceNode = "_spliceNode";
	public final static String varName_PointIndex = "_pointIndex";
	public final static String varName_CallIndex = "_callIndex";
	public final static String varName_InterState = "_interState";
	public final static String varName_Autotuner = "_autotuner";
	public final static String varName_TuneBlock = "_tuneBlock";
	public final static String varName_TuneInterState = "_tuneInterState";
	
	public final static String varName_InterState_SpliceSet = "spliceSet";

	public final static String typeName_Block = "_Block";
	public final static String typeName_BlockStack = "_BlockStack";
	public final static String typeName_BlockSet = "_BlockSet";
	public final static String typeName_Point = "_Point";
	public final static String typeName_SpliceSet = "_SpliceSet";
	public final static String typeName_SpliceNode = "_SpliceNode";
	public final static String typeName_SplicePoint = "_SplicePoint";
	public final static String typeName_IntermediaryState = "_IntermediaryState";
	public final static String typeName_Autotuner = "_Autotuner";

	public final static String methName_processBlock = "_processBlock";
	public final static String methName_Block_add = "add";
	public final static String methName_Block_useBlock = "useBlock";
	public final static String methName_Block_flushBlock = "flushBlock";
	public final static String methName_Block_flushBlockAutotune = "flushBlockAutotune";
	public final static String methName_Block_mapRoot = "mapRoot";
	//public final static String methName_Block_copy = "copy";
	public final static String methName_Block_isEmpty = "isEmpty";
	public final static String methName_Block_recycle = "recycle";
	public final static String methName_Block_compress = "compress";
	public final static String methName_Block_nextSample = "nextSample";
	public final static String methName_Block_isSampled = "isSampled";
	public final static String methName_BlockStack_get = "get";
	public final static String methName_BlockStack_getInstance = "getInstance";
	public final static String methName_BlockStack_setInstance = "setInstance";
	public final static String methName_Block_tuneSetup = "tuneSetup";
	public final static String methName_Block_tuneEntry = "tuneEntry";
	public final static String methName_Block_tuneExit = "tuneExit";
	public final static String methName_Block_tuneNext = "tuneNext";
	public final static String methName_Block_profileWorkDone = "profileWorkDone";

	public final static String methName_SpliceSet_initSpliceSetAndTree = "initSpliceSetAndTree";
	public final static String methName_SpliceSet_setTreeClass = "setTreeClass";
	public final static String methName_SpliceSet_getDepthParam = "getDepthParam";
	public final static String methName_SpliceSet_useSplice = "useSplice";
	public final static String methName_SpliceSet_getSpliceNode = "getSpliceNode";
	public final static String methName_SpliceSet_add = "add";
	public final static String methName_SpliceSet_init = "init";
	public final static String methName_SpliceSet_doSplice = "doSplice";
	public final static String methName_SpliceSet_workSize = "getWorkSize";
	public final static String methName_SpliceSet_markSpliceState = "markSpliceState";
	public final static String methName_SpliceSet_saveSpliceLocals = "saveSpliceLocals";
	public final static String methName_SpliceSet_profileDepth = "profileDepth";
	public final static String methName_SpliceSet_profileConvergence = "profileConvergence";

	public final static String methName_IntermediaryState_resetIndex = "resetIndex";
	public final static String methName_IntermediaryState_incIndex = "incIndex";
	public final static String methName_IntermediaryState_getIndex = "getIndex";

	public final static String methName_Autotuner_isSampled = "isSampled";
	public final static String methName_Autotuner_tuneFinished = "tuneFinished";

	public final static String methName_mapTreeSuffix = "_MapTree";
	public final static String methName_mapRootSuffix = "_MapRoot";
	public final static String methName_blockSuffix = "_Block";
	public final static String methName_autotuneSuffix = "_Autotune";
	public final static String methName_spliceSuffix = "_Splice";

	public final static String methName_prologueSuffix = "_Prologue";
	public final static String methName_prologueSpliceSuffix = "_PrologueSplice";
	public final static String methName_epilogueSuffix = "_Epilogue";

	public final static String annotationName_parallel = "ParallelFor";

	public final static String typeName_Launcher = "Launcher";
	public final static String methodName_Launcher_getProfiler = "getProfiler";
	public final static String methodName_Profiler_oneBlockDone = "oneBlockDone";
	public final static String methodName_Profiler_effectiveBlock = "effectiveBlock";
	public final static String methodName_Profiler_touch = "touch";

	public static CompilationUnit getEnclosingCompilationUnit(ASTNode node) {
		while (!(node instanceof CompilationUnit)) {
			node = node.getParent();
		}
		return (CompilationUnit) node;
	}

	public static Stmt getEnclosingStmt(ASTNode node) {
		while (!(node instanceof Stmt)) {
			node = node.getParent();
		}
		return (Stmt) node;		
	}
	
	public static Expr getDotLeft(ASTNode node) {
		if (node.getParent() instanceof AbstractDot) {
			AbstractDot dot = (AbstractDot) node.getParent();
			return dot.getLeft();
		}
		return null;
	}

	public static Block getEnclosingBlock(ASTNode node) {
		while (!(node instanceof Block)) {
			node = node.getParent();
		}
		return (Block) node;		
	}

	public static AbstractDot getEnclosingAbstractDot(ASTNode node) {
		while (!(node instanceof AbstractDot)) {
			node = node.getParent();
		}
		return (AbstractDot) node;		
	}

	public static Expr expandToAbstractDot(Expr expr) {
		while (expr.getParent() instanceof Dot) {
			expr = (Expr) expr.getParent();
		}
		return expr;
	}	

	public static MethodDecl getEnclosingMethodDecl(ASTNode node) {
		while (!(node instanceof MethodDecl) && node != null) {
			node = node.getParent();
		}
		return (MethodDecl) node;		
	}

	public static String getPrivatizedName(MethodDecl mdec, VariableDeclaration vdec) {
		return mdec.getID() + "_" +  vdec.getID();
	}

	public static String getPointName(VarAccess vacc) {
		return getPointName(vacc.getID());
	}

	public static String getPointName(String varName) {
		return varName_Block_Data + varName;
	}

	public static Expr getMainExpr(Expr expr) {
		while (expr instanceof CastExpr || expr instanceof ParExpr) {
			if (expr instanceof CastExpr) {
				CastExpr cexpr = (CastExpr) expr;
				expr = cexpr.getExpr();
			} else if (expr instanceof ParExpr) {
				ParExpr pexpr = (ParExpr) expr;
				expr = pexpr.getExpr();
			}
		}
		return expr;
	}

	public static ArrayList<MethodAccess> getRecursionPoints(MethodDecl mdec) {
		ArrayList<MethodAccess> ret = new ArrayList<MethodAccess>();
		ArrayList<MethodAccess> maccs = new ArrayList<MethodAccess>();
		mdec.gatherMethodAccesses(maccs);
		for (MethodAccess macc : maccs) {
			if (macc.getID().equals(mdec.getID())) {
				ret.add(macc);
			}
		}
		return ret;
	}

	public static void removeLoopVariantStmts(Set<VarAccess> loopVariantArgsPropagation, Stmt stmt,
			ArrayList<Stmt> recursionPointStmts, ArrayList<Stmt> toRemoveStmts) {
		if (stmt instanceof Block) {
			Block block = (Block) stmt;
			for (Stmt child : block.getStmts()) {
				removeLoopVariantStmts(loopVariantArgsPropagation, child, recursionPointStmts, toRemoveStmts);
			}
		} else if (stmt instanceof ExprStmt) {
			ExprStmt estmt = (ExprStmt) stmt;
			Expr expr = estmt.getExpr();
			if (expr instanceof AssignExpr) {
				AssignExpr aexpr = (AssignExpr) expr;
				propagateLoopVariantArgs(loopVariantArgsPropagation, aexpr.getSource(), aexpr.getDest());
			}
			if (stmt.find(loopVariantArgsPropagation)  && !stmt.find(recursionPointStmts)) {
				toRemoveStmts.add(stmt);
				return;
			}
		}else if (stmt instanceof VariableDeclaration) {
			VariableDeclaration vdec = (VariableDeclaration) stmt;
			if (vdec.hasInit()) {
				propagateLoopVariantArgs(loopVariantArgsPropagation, vdec.getInit(), new VarAccess(vdec.name()));
			}
			if (stmt.find(loopVariantArgsPropagation)  && !stmt.find(recursionPointStmts)) {
				toRemoveStmts.add(stmt);
				return;
			}
		} else if (stmt instanceof IfStmt) {
			IfStmt ifstmt = (IfStmt) stmt;
			Expr ifcond = ifstmt.getCondition();
			if (ifcond.find(loopVariantArgsPropagation)) {
				if(ifstmt.getThen().find(recursionPointStmts)) {
					if (!(ifstmt.hasElse() && ifstmt.getElse().find(recursionPointStmts))) {
						removeLoopVariantStmts(loopVariantArgsPropagation, ifstmt.getThen(), recursionPointStmts, toRemoveStmts);
						ifstmt.swapStmtWith(ifstmt.getThen());
					}
				} else if (ifstmt.hasElse() && ifstmt.getElse().find(recursionPointStmts)) {
					removeLoopVariantStmts(loopVariantArgsPropagation, ifstmt.getElse(), recursionPointStmts, toRemoveStmts);
					ifstmt.swapStmtWith(ifstmt.getElse());
				} else if (ifstmt.getCondition().find(recursionPointStmts)) {
					// recursionPoint in if cond should have been handled before calling removeLoopVariantStmts
					assert false;
				} else {
					toRemoveStmts.add(stmt);
					return;
				}
			} else {
				removeLoopVariantStmts(loopVariantArgsPropagation, ifstmt.getThen(), recursionPointStmts, toRemoveStmts);
				if (ifstmt.hasElse()) {
					removeLoopVariantStmts(loopVariantArgsPropagation, ifstmt.getElse(), recursionPointStmts, toRemoveStmts);
				}
			}
		} else if (stmt instanceof ForStmt) {
			ForStmt forstmt = (ForStmt) stmt;				
			removeLoopVariantStmts(loopVariantArgsPropagation, forstmt.getStmt(), recursionPointStmts, toRemoveStmts);
		} else if (stmt instanceof EnhancedForStmt) {
			EnhancedForStmt forstmt = (EnhancedForStmt) stmt;
			removeLoopVariantStmts(loopVariantArgsPropagation, forstmt.getStmt(), recursionPointStmts, toRemoveStmts);
		} else {
			if (stmt.find(loopVariantArgsPropagation)  && !stmt.find(recursionPointStmts)) {
				toRemoveStmts.add(stmt);
				return;
			}
		}
	}

	public static void propagateLoopVariantArgs(Set<VarAccess> set, Expr src, Expr dest) {
		if (src.find(set)) {
			// for nd.accx, treat as nd (conservative)
			if (dest instanceof Dot) {
				dest = ((Dot) dest).getLeft();
			}				
			VarAccess vdest = (VarAccess) dest;
			//System.out.println(dest);
			set.add(vdest);
		}		
	}

	// for control flow, if either path has loop variant stmts, the parent is also considered loop variant
	public static boolean gatherLoopVariantStmts(Set<Stmt> loopVariantControlStmts, Set<VarAccess> loopVariantArgsPropagation, Stmt stmt,
			ArrayList<Stmt> recursionPointStmts) {
		boolean ret = false;
		//System.out.println(stmt);
		if (stmt instanceof Block) {
			Block block = (Block) stmt;
			for (Stmt child : block.getStmts()) {
				ret = gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, child, recursionPointStmts) || ret;
			}
		} else if (stmt instanceof ExprStmt) {
			ExprStmt estmt = (ExprStmt) stmt;
			Expr expr = estmt.getExpr();
			if (expr instanceof AssignExpr) {
				AssignExpr aexpr = (AssignExpr) expr;
				propagateLoopVariantArgs(loopVariantArgsPropagation, aexpr.getSource(), aexpr.getDest());
			} 
			if (expr.find(loopVariantArgsPropagation) && !recursionPointStmts.contains(stmt)) {
				ret = true;
			}
		} else if (stmt instanceof VariableDeclaration) {
			VariableDeclaration vdec = (VariableDeclaration) stmt;
			if (vdec.hasInit()) {
				propagateLoopVariantArgs(loopVariantArgsPropagation, vdec.getInit(), new VarAccess(vdec.name()));
			}
		} else if (stmt instanceof IfStmt) {
			IfStmt ifstmt = (IfStmt) stmt;
			Expr ifcond = ifstmt.getCondition();
			if (ifcond.find(loopVariantArgsPropagation)) {
				ret = true;
			}
			ret = gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, ifstmt.getThen(), recursionPointStmts) || ret;
			if (ifstmt.hasElse()) {
				ret = gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, ifstmt.getElse(), recursionPointStmts) || ret;
			}
		} else if (stmt instanceof ForStmt) {
			ForStmt forstmt = (ForStmt) stmt;
			Expr forcond = forstmt.getCondition();
			if (forcond.find(loopVariantArgsPropagation)) {
				assert false;	// this shouldn't occur?
				ret = true;
			}				
			ret = gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, forstmt.getStmt(), recursionPointStmts) || ret;
		} else if (stmt instanceof EnhancedForStmt) {
			EnhancedForStmt forstmt = (EnhancedForStmt) stmt;
			ret = gatherLoopVariantStmts(loopVariantControlStmts, loopVariantArgsPropagation, forstmt.getStmt(), recursionPointStmts) || ret;
		}
		if (ret) {
			loopVariantControlStmts.add(stmt);
		}
		return ret;
	}


	// modified version of gatherLoopVariantStmts, to gather loop variant variable declarations
	public static ArrayList<VariableDeclaration> gatherDependentDecsEntry(Set<VarAccess> dependents, Stmt stmt) {
		ArrayList<VariableDeclaration> varDecs = new ArrayList<VariableDeclaration>();
		ArrayList<VariableDeclaration> loopVariantDecs = new ArrayList<VariableDeclaration>();
		gatherDependentDecs(varDecs, dependents, stmt);
		for (VariableDeclaration varDec : varDecs) {
			boolean isLoopVariant = false;
			for (VarAccess loopVariantVar : dependents) {
				if (loopVariantVar.getID().equals(varDec.getID())) {
					isLoopVariant = true;
					break;
				}
			}
			if (isLoopVariant) {
				//System.out.println(varDec);
				loopVariantDecs.add(varDec);
			}
		}
		return loopVariantDecs;
	}

	// recursive function - to be called from gatherDependentDecs
	private static void gatherDependentDecs(ArrayList<VariableDeclaration> varDecs, Set<VarAccess> dependents, Stmt stmt) {
		if (stmt instanceof Block) {
			Block block = (Block) stmt;
			for (Stmt child : block.getStmts()) {
				gatherDependentDecs(varDecs, dependents, child);
			}
		} else if (stmt instanceof ExprStmt) {
			ExprStmt estmt = (ExprStmt) stmt;
			Expr expr = estmt.getExpr();
			if (expr instanceof AssignExpr) {
				AssignExpr aexpr = (AssignExpr) expr;
				propagateLoopVariantArgs(dependents, aexpr.getSource(), aexpr.getDest());
			}
		} else if (stmt instanceof VariableDeclaration) {
			VariableDeclaration vdec = (VariableDeclaration) stmt;
			varDecs.add(vdec);
			if (vdec.hasInit()) {
				propagateLoopVariantArgs(dependents, vdec.getInit(), new VarAccess(vdec.name()));
			}
		} else if (stmt instanceof IfStmt) {
			IfStmt ifstmt = (IfStmt) stmt;
			gatherDependentDecs(varDecs, dependents, ifstmt.getThen());
			if (ifstmt.hasElse()) {
				gatherDependentDecs(varDecs, dependents, ifstmt.getElse());
			}
		} else if (stmt instanceof ForStmt) {
			ForStmt forstmt = (ForStmt) stmt;				
			gatherDependentDecs(varDecs, dependents, forstmt.getStmt());
		}
	}	

	/**
	 * makes point dec stmt
	 * ex) Ray ray = _block.data_ray[_blockIndex];
	 * for now assume that each point data will be of different type, and use type as the name
	 * @param vacc
	 * @return
	 */
	public static Stmt makePointDec(ParameterDeclaration pdec) {
		VariableDeclaration pointDec = new VariableDeclaration(
				(Access) pdec.getTypeAccess().fullCopy(),
				pdec.getID(),
				getBlockData(pdec.getID()));
		return pointDec;
	}

	public static Expr getBlockData(String fieldName) {
		Expr blockData;
		if (Globals.usePointClass) {
			blockData = new AbstractDot(new VarAccess(Utils.varName_Point), new VarAccess(fieldName));
		} else {
			blockData = new AbstractDot(new VarAccess(Utils.varName_Block), 
					new AbstractDot(new VarAccess(fieldName), new ArrayAccess(new VarAccess(Utils.varName_BlockIndex))));
		}
		return blockData;
	}

	public static void generateImports(PrintStream out, String packageName) {
		for (String pName : Globals.packageNames) {
			if (!pName.equals(packageName)) {
				out.print("import " + pName + ".*;\n");
			}
		}
	}

	public static void addImport(CompilationUnit cu, String packageName) {
		ImportDecl newIdec = new SingleTypeImportDecl(new PackageAccess(packageName));
		for (ImportDecl idec : cu.getImportDecls()) {
			if (idec.toString().equals(newIdec.toString())) {
				return;
			}
		}
		cu.addImportDecl(newIdec);
	}

	/**
	 * @deprecated
	 * @return
	 */
	public static ForStmt makeBlockForStmt() {
		// add for(int _blockIndex = 0; _blockIndex < _block.size; _blockIndex++) loop
		VariableDeclaration init1 = new VariableDeclaration(new PrimitiveTypeAccess("int"),
				Utils.varName_BlockIndex, new IntegerLiteral("0"));
		List<Stmt> initList = new List<Stmt>();
		initList.add(init1);
		LTExpr cond = new LTExpr(new VarAccess(Utils.varName_BlockIndex),
				new AbstractDot(new VarAccess(Utils.varName_Block), new VarAccess(Utils.varName_Block_Size)));
		Opt<Expr> condOpt = new Opt<Expr>(cond);
		PostIncExpr step = new PostIncExpr(new VarAccess(Utils.varName_BlockIndex));
		List<Stmt> stepList = new List<Stmt>();
		stepList.add(new ExprStmt(step));
		Block block = new Block();
		ForStmt forstmt = new ForStmt(initList, condOpt, stepList, block);
		return forstmt;
	}

	// returns last statement
	public static Stmt addPointDecs(Block block, ArrayList<ParameterDeclaration> pdecs) {
		Stmt ret = null;
		if (Globals.usePointClass) {
			VariableDeclaration pointDec = new VariableDeclaration(new TypeAccess(Utils.typeName_Point), 
					Utils.varName_Point, new AbstractDot(new VarAccess(Utils.varName_Block), 
							new AbstractDot(new VarAccess(Utils.varName_Block_Points), new ArrayAccess(new VarAccess(Utils.varName_BlockIndex)))));
			block.addStmt(pointDec);
		}
		for (ParameterDeclaration pdec : pdecs) {
			ret = Utils.makePointDec(pdec);
			block.addStmt(ret);
		}
		return ret;
	}

	public static void addPointDec(Stmt ref, ParameterDeclaration pdec, int numCallSets) {
		assert numCallSets == 2; // TODO: handle general case
		EQExpr cond = new EQExpr(new VarAccess(Utils.varName_CallIndex), new IntegerLiteral(0));
		Stmt then = new ExprStmt(new AssignSimpleExpr(
				new VarAccess(pdec.getID()), getBlockData(pdec.getID() + "0")));
		Stmt elsee = new ExprStmt(new AssignSimpleExpr(
				new VarAccess(pdec.getID()), getBlockData(pdec.getID() + "1")));
		IfStmt ifstmt = new IfStmt(cond, then, elsee);
		ref.insertStmtAfter(ifstmt);
		VariableDeclaration pointDec = new VariableDeclaration((Access) pdec.getTypeAccess().fullCopy(), pdec.getID());
		ref.insertStmtAfter(pointDec);
	}
}
